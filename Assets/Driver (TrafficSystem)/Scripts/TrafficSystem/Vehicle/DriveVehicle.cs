﻿using UnityEngine;
using System.Collections;
using Opsive.ThirdPersonController.Input.Wrappers;
using Opsive.ThirdPersonController.Wrappers;


public enum PlayerCarStatus { Idle = 0, OpenDoor = 1, inCar = 2, RollDoor = 3, Sit = 4, OutCar = 5, CloseDoor = 6 };
public enum PlayerBikeStatus { Idle = 0, GettingOn = 1, Sit = 2, GettingOff = 3};

public class DriveVehicle : MonoBehaviour
{

    public CharacterComponents characterComponents;


    private PlayerCarStatus playerCarStatus = PlayerCarStatus.Idle;
    private PlayerBikeStatus playerBikeStatus = PlayerBikeStatus.Idle;

    private bool gettingOnCar = false;
    private bool gettingOnBike = false;

    private Animator m_Animator;

    private CarComponents carComponents;
    private BikeComponents bikeComponents;


    private AIVehicle m_AIVehicle;


    private VehicleControl m_VehicleControl;
    private BikeControl m_BikeControl;

    private Transform handleTrigger;
    private Transform door;
    private Transform sitPoint;

	private VehicleGun gun;

    
    [System.Serializable]
    public class CharacterComponents
    {
        public Rigidbody myRigidbody;
        public Collider myCollider;
        public UnityEngine.AI.NavMeshObstacle myNavMeshObstacle;

        //public ThirdPersonCharacter myThirdPersonCharacter;
       // public ThirdPersonUserControl myThirdPersonUserControl;
		public UnityInput uInput;
		public AnimatorMonitor monitor;
		public RigidbodyCharacterController controller;
		public ControllerHandler controllerHandler;
		public Inventory inventory;
		public InventoryHandler inventoryHandler;
		public CharacterFootsteps footsteps;
		public ItemHandler itemHandler;
		public CharacterIK characterIK;
		public CharacterHealth health;
    }


    void OnTriggerStay(Collider other)
    {

        if (other.CompareTag("HandleTrigger"))
        {
            GameControl.manager.getInVehicle.SetActive(true);

            if ((GameControl.manager.controlMode == ControlMode.simple&&Input.GetKey(KeyCode.F))
                || (GameControl.manager.controlMode == ControlMode.touch && GameControl.driving == true))
            {


                if (other.transform.root.GetComponent<CarComponents>())
                {
                    carComponents = other.transform.root.GetComponent<CarComponents>();
                    m_AIVehicle = other.transform.root.GetComponent<AIVehicle>();
                    m_VehicleControl = other.transform.root.GetComponent<VehicleControl>();

                    door = carComponents.door;
                    handleTrigger = carComponents.handleTrigger;
                    sitPoint = carComponents.sitPoint;
                    gettingOnCar = true;
                    gettingOnBike = false;

                    GameControl.manager.getInVehicle.SetActive(false);

					SetDriverInVehicle (true, false);
                }


                if (other.transform.root.GetComponent<BikeComponents>())
                {
                    bikeComponents = other.transform.root.GetComponent<BikeComponents>();
                    m_AIVehicle = other.transform.root.GetComponent<AIVehicle>();
                    m_BikeControl = other.transform.root.GetComponent<BikeControl>();

                    handleTrigger = bikeComponents.handleTrigger;
                    sitPoint = bikeComponents.sitPoint;

                    gettingOnBike = true;
                    gettingOnCar = false;

                    GameControl.manager.getInVehicle.SetActive(false);

					SetDriverInVehicle (true, true);

                }


			} 

        }

    }


    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("HandleTrigger"))
            GameControl.manager.getInVehicle.SetActive(false);
        
    }
    public void ComponentsStatus(bool active)
    {
		if (!active) {
			GameController.Instance.DisableLockTarget ();
		}
		characterComponents.uInput.enabled = active;
		characterComponents.monitor.enabled = active;
		characterComponents.controller.enabled = active;
		characterComponents.controllerHandler.enabled = active;
			
		characterComponents.inventory.enabled = active;
		characterComponents.inventoryHandler.enabled = active;
		characterComponents.footsteps.enabled = active;
		characterComponents.itemHandler.enabled = active;
		characterComponents.characterIK.enabled = active;
		characterComponents.health.enabled = active;

        characterComponents.myNavMeshObstacle.enabled = active;
        characterComponents.myRigidbody.isKinematic = !active;
        characterComponents.myCollider.enabled = active;
    }


    void Start()
    {
        m_Animator = GetComponent<Animator>();
		//SetStickKinematic (false);
    }

    public void GetinCar()
    {


        switch (playerCarStatus)
        {

            case PlayerCarStatus.Idle:

                transform.parent = carComponents.sitPoint.transform;
                ComponentsStatus(false);

                transform.position = Vector3.MoveTowards(transform.position, handleTrigger.position, Time.deltaTime * 3.0f);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, handleTrigger.rotation, Time.deltaTime * 250.0f);

                if (transform.position == handleTrigger.position && transform.rotation == handleTrigger.rotation)
                {
                    m_Animator.ForceStateNormalizedTime(0.0f);

                    m_Animator.SetFloat("CarStatus", 1);
                    m_Animator.SetBool("DriveCar", true);
                    m_Animator.Play("Drive Car", 0);
                    m_VehicleControl.carSounds.openDoor.Play();
                    //playerCarStatus++;
					playerCarStatus = PlayerCarStatus.Sit;
                }

                break;
            case PlayerCarStatus.OpenDoor:

                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    m_Animator.SetFloat("CarStatus", 2);
                    m_Animator.ForceStateNormalizedTime(0.0f);
                    playerCarStatus++;
                }
                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.6f)
                {
                    door.localRotation = Quaternion.RotateTowards(door.localRotation, Quaternion.Euler(0, 45, 0), Time.deltaTime * 300.0f);
                }


                break;
            case PlayerCarStatus.inCar:


                carComponents.driving = false;

                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    m_Animator.SetFloat("CarStatus", 3);
                    m_Animator.ForceStateNormalizedTime(0.0f);
                    playerCarStatus++;
                }
                else
                {

                    if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.3f)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, sitPoint.position, Time.deltaTime * 3.0f);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, sitPoint.rotation, Time.deltaTime * 250.0f);
                    }
                    else
                    {
                        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 500.0f);
                    }
                }


                break;
            case PlayerCarStatus.RollDoor:



                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    m_Animator.SetFloat("CarStatus", 4);
                    m_Animator.ForceStateNormalizedTime(0.0f);
                    m_VehicleControl.carSounds.closeDoor.Play();
                    playerCarStatus++;
                }
                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f)
                {
                    door.localRotation = Quaternion.RotateTowards(door.localRotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 250.0f);
                }

                break;
            case PlayerCarStatus.Sit:


                AIContoller.manager.playerCamera.enabled = false;
				AIContoller.manager.playerCamera.GetComponent<Camera>().enabled = false;

                AIContoller.manager.vehicleCamera.cameraSwitchView = carComponents.cameraViewSetting.cameraViews;
                AIContoller.manager.vehicleCamera.distance = carComponents.cameraViewSetting.distance;
                AIContoller.manager.vehicleCamera.height = carComponents.cameraViewSetting.height;
                AIContoller.manager.vehicleCamera.Angle = carComponents.cameraViewSetting.Angle;


                AIContoller.manager.vehicleCamera.target = handleTrigger.root.transform;
                AIContoller.manager.vehicleCamera.enabled = true;
				AIContoller.manager.vehicleCamera.GetComponent<Camera>().enabled = true;


                m_AIVehicle.vehicleStatus = VehicleStatus.Player;

                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    if ((GameControl.manager.controlMode == ControlMode.simple && Input.GetKey(KeyCode.F))
                        || (GameControl.manager.controlMode == ControlMode.touch && GameControl.driving == false))
                    {
                        m_Animator.SetFloat("CarStatus", 5);
                        m_Animator.ForceStateNormalizedTime(0.0f);
                        m_AIVehicle.vehicleStatus = VehicleStatus.EmptyOn;
                        m_VehicleControl.carSounds.openDoor.Play();
                        playerCarStatus++;
                    }

                }


                break;
            case PlayerCarStatus.OutCar:

                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    m_Animator.SetFloat("CarStatus", 6);
                    m_Animator.ForceStateNormalizedTime(0.0f);
                    playerCarStatus++;
                }
                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.3f)
                {
                    transform.position = Vector3.MoveTowards(transform.position, handleTrigger.position, Time.deltaTime * 3.0f);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, handleTrigger.rotation, Time.deltaTime * 250.0f);
                }

                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.1f)
                {

                    door.localRotation = Quaternion.RotateTowards(door.localRotation, Quaternion.Euler(0, 45, 0), Time.deltaTime * 250.0f);
                }




                break;
            case PlayerCarStatus.CloseDoor:

                AIContoller.manager.playerCamera.enabled = true;
                AIContoller.manager.vehicleCamera.enabled = false;


                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f)
                {
                    gettingOnCar = false;
                    transform.parent = null;
                    ComponentsStatus(true);
                    m_VehicleControl.carSounds.closeDoor.Play();

                    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

                    playerCarStatus = PlayerCarStatus.Idle;

                }
                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.2f)
                {
                    handleTrigger.root.GetComponent<CarComponents>().door.localRotation = Quaternion.RotateTowards(
                        handleTrigger.root.GetComponent<CarComponents>().door.localRotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 250.0f);
                    m_Animator.SetBool("DriveCar", false);
                }


                break;
        }

    }



    public void GetinBike()
    {

        switch (playerBikeStatus)
        {

            case PlayerBikeStatus.Idle:

                transform.parent = bikeComponents.sitPoint.transform;
                ComponentsStatus(false);

                transform.position = Vector3.MoveTowards(transform.position, handleTrigger.position, Time.deltaTime * 3.0f);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, handleTrigger.rotation, Time.deltaTime * 250.0f);

                if (transform.position == handleTrigger.position && transform.rotation == handleTrigger.rotation)
                {
                    m_Animator.ForceStateNormalizedTime(0.0f);

                    m_Animator.SetFloat("BikeStatus", 1);
                    m_Animator.SetBool("DriveBike", true);
                    m_Animator.Play("Drive Bike", 0);
                    playerBikeStatus++;
                }

                break;
            case PlayerBikeStatus.GettingOn:


                bikeComponents.driving = false;


                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    m_Animator.SetFloat("BikeStatus", 2);
                    m_Animator.ForceStateNormalizedTime(0.0f);
                    playerBikeStatus++;
                }
                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.4f)
                {
                    transform.position = Vector3.MoveTowards(transform.position, sitPoint.position, Time.deltaTime * 3.0f);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, sitPoint.rotation, Time.deltaTime * 250.0f);
                }


                break;
            case PlayerBikeStatus.Sit:

                AIContoller.manager.playerCamera.enabled = false;


                AIContoller.manager.vehicleCamera.cameraSwitchView = bikeComponents.cameraViewSetting.cameraViews;
                AIContoller.manager.vehicleCamera.distance = bikeComponents.cameraViewSetting.distance;
                AIContoller.manager.vehicleCamera.height = bikeComponents.cameraViewSetting.height;
                AIContoller.manager.vehicleCamera.Angle = bikeComponents.cameraViewSetting.Angle;


                AIContoller.manager.vehicleCamera.target = handleTrigger.root.transform;
                AIContoller.manager.vehicleCamera.enabled = true;


                m_AIVehicle.vehicleStatus = VehicleStatus.Player;

                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
                {
                    if ((GameControl.manager.controlMode == ControlMode.simple && Input.GetKey(KeyCode.F)) 
                        || (GameControl.manager.controlMode == ControlMode.touch && GameControl.driving == false))
                    {
                        m_Animator.SetFloat("BikeStatus", 3);
                        m_Animator.ForceStateNormalizedTime(0.0f);

                        m_AIVehicle.vehicleStatus = VehicleStatus.EmptyOn;
                        playerBikeStatus++;
                    }

                }


                break;

            case PlayerBikeStatus.GettingOff:

                AIContoller.manager.playerCamera.enabled = true;
                AIContoller.manager.vehicleCamera.enabled = false;

                if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f)
                {
                    gettingOnBike = false;
                    transform.parent = null;
                    ComponentsStatus(true);
                    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                    playerBikeStatus = PlayerBikeStatus.Idle;
                }
                else if (m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.4f)
                {
                    transform.position = Vector3.MoveTowards(transform.position, handleTrigger.position, Time.deltaTime * 3.0f);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, handleTrigger.rotation, Time.deltaTime * 250.0f);

                    m_Animator.SetBool("DriveBike", false);
                }


                break;
        }



    }


    void Update()
    {
		if (gettingOnCar) { //GetinCar(); else if (gettingOnBike) GetinBike();
			if ((GameControl.manager.controlMode == ControlMode.simple && Input.GetKey (KeyCode.F))
			    || (GameControl.manager.controlMode == ControlMode.touch && GameControl.driving == false)) {
				SetDriverInVehicle (false, false);
			}
		} else if (gettingOnBike) { //GetinCar(); else if (gettingOnBike) GetinBike();
			if ((GameControl.manager.controlMode == ControlMode.simple && Input.GetKey (KeyCode.F))
				|| (GameControl.manager.controlMode == ControlMode.touch && GameControl.driving == false)) {
				SetDriverInVehicle (false, true);
			}
		}
    }

	void SetDriverInVehicle(bool inCar, bool isBike) {
		if (inCar) {
			if (isBike) {
				bikeComponents.GetOffDriver ();
				transform.parent = bikeComponents.sitPoint.transform;
			} else {
				carComponents.GetOffDriver ();
				transform.parent = carComponents.sitPoint.transform;
			}

			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.Euler (0, 0, 0);
			ComponentsStatus(false);

			//TODO
//			m_Animator.SetFloat("CarStatus", 1);
//			m_Animator.ForceStateNormalizedTime(0.0f);
//			m_Animator.SetBool("DriveCar", true);
			if (!isBike) {
				foreach (Renderer r in GetComponentsInChildren<Renderer>()) {
					if (r.gameObject.activeInHierarchy)
						r.enabled = false;
				}
			} else {
				m_Animator.SetBool("DriveCar", true);
			}
			GameController.Instance.SetStickKinematic (true);

			//m_Animator.Play("Drive Car", 0);
			AIContoller.manager.playerCamera.enabled = false;
			if (isBike) {
				AIContoller.manager.vehicleCamera.cameraSwitchView = bikeComponents.cameraViewSetting.cameraViews;
				AIContoller.manager.vehicleCamera.distance = bikeComponents.cameraViewSetting.distance;
				AIContoller.manager.vehicleCamera.height = bikeComponents.cameraViewSetting.height;
				AIContoller.manager.vehicleCamera.Angle = bikeComponents.cameraViewSetting.Angle;
				AIContoller.manager.vehicleCamera.target = handleTrigger.root.transform;
				AIContoller.manager.vehicleCamera.enabled = true;
				if (bikeComponents.cameraViewSetting.cameraViews.Count > 0)
					AIContoller.manager.vehicleCamera.SetHeight (bikeComponents.cameraViewSetting.cameraViews [0].transform.position.y);
			} else {
				AIContoller.manager.vehicleCamera.cameraSwitchView = carComponents.cameraViewSetting.cameraViews;
				AIContoller.manager.vehicleCamera.distance = carComponents.cameraViewSetting.distance;
				AIContoller.manager.vehicleCamera.height = carComponents.cameraViewSetting.height;
				AIContoller.manager.vehicleCamera.Angle = carComponents.cameraViewSetting.Angle;
				AIContoller.manager.vehicleCamera.target = handleTrigger.root.transform;
				AIContoller.manager.vehicleCamera.enabled = true;
				if (carComponents.cameraViewSetting.cameraViews.Count > 0)
					AIContoller.manager.vehicleCamera.SetHeight (carComponents.cameraViewSetting.cameraViews [0].transform.position.y);
				if (carComponents.GetComponent<VehicleGun> ())
					gun = carComponents.GetComponent<VehicleGun> ();
			}
			if (GameController.Instance.shieldEnabled) {
				SetShieldEnabled (true);
			}
			SetGunEnabled (isBike, true);
			m_AIVehicle.vehicleStatus = VehicleStatus.Player;
			GameUI.Instance.ShowAdRandom ();

			//SetupUpgrades upgrades = GetComponent<SetupUpgrades> ();
			//m_VehicleControl.UpgradePower (upgrades.getDriveSpeedPrecentage ());
			if ((!isBike && m_VehicleControl.isPoliceCar) || (isBike && m_BikeControl.isPoliceBike))
				GameController.Instance.playerState = GameController.PlayerState.DrivePoiceCar;
			else
				GameController.Instance.playerState = GameController.PlayerState.DriveStolenCar;
			AchievementController.Instance.SetMeasureWalking (false);
			Health carHealth = isBike ? m_BikeControl.GetComponent<Health> () : m_VehicleControl.GetComponent<Health> () ;
			GameUI.Instance.InitCarHealth (carHealth.gameObject, carHealth);
		} else {
			m_Animator.ForceStateNormalizedTime(0.0f);
			m_AIVehicle.vehicleStatus = VehicleStatus.EmptyOn;
			if (isBike)
				gettingOnBike = false;
			else
				gettingOnCar = false;
			transform.parent = null;

			UnityEngine.AI.NavMeshHit hit;
			UnityEngine.AI.NavMesh.SamplePosition(handleTrigger.position, out hit, 50f, 1);
			transform.position = hit.position;
			transform.rotation = Quaternion.Euler(0, 0, 0);
			ComponentsStatus(true);
			//TODO
//			m_Animator.SetBool("DriveCar", false);
			if (!isBike) {
				foreach (Renderer r in GetComponentsInChildren<Renderer>()) {
					if (r.gameObject.activeInHierarchy)
						r.enabled = true;
				}
				carComponents.GetComponent<Health> ().Invincible = false;
			} else {
				m_Animator.SetBool("DriveCar", false);
				bikeComponents.GetComponent<Health> ().Invincible = false;
			}
			GameController.Instance.SetStickKinematic (false);

			AIContoller.manager.playerCamera.enabled = true;
			AIContoller.manager.vehicleCamera.enabled = false;
			GameController.Instance.playerState = GameController.PlayerState.Walk;
			AchievementController.Instance.SetMeasureWalking (true);
			GameUI.Instance.HideCarHealth ();
			SetGunEnabled (isBike, false);
		}
		GameController.Instance.SetPlayerInCar (inCar);
	}

	void SetGunEnabled(bool isBike, bool enabled) {
		if (enabled) {
			if (isBike) {
				if (bikeComponents.GetComponent<VehicleGun> ()) {
					gun = bikeComponents.GetComponent<VehicleGun> ();
					GameUI.Instance.SetVehicleGunEnabled (true, gun.crosshairSprite);
					gun.SetGunEnabled (true);
				} else {
					GameUI.Instance.SetVehicleGunEnabled (false, null);
				}
			} else {
				if (carComponents.GetComponent<VehicleGun> ()) {
					gun = carComponents.GetComponent<VehicleGun> ();
					GameUI.Instance.SetVehicleGunEnabled (true, gun.crosshairSprite);
					gun.SetGunEnabled (true);
				} else {
					GameUI.Instance.SetVehicleGunEnabled (false, null);
				}
			}
		} else {
			if (gun != null) {
				gun.SetGunEnabled (false);
				gun = null;
			}
			GameUI.Instance.SetVehicleGunEnabled (false, null);
		}
	}

	public void OnCrash() {
		if (m_VehicleControl)
			SetDriverInVehicle (false, false);
		else
			SetDriverInVehicle (false, true);
		GameControl.driving = false;
		GetComponent<CharacterHealth> ().InstantDeath ();
	}

	public void RestoreCarHealth() {
		if (m_VehicleControl != null) {
			Health carHealth = m_VehicleControl.GetComponent<Health> ();
			carHealth.SetHealthAmount (carHealth.MaxHealth);
			m_VehicleControl.GetComponent<ExplodeCar> ().steamFX.SetActive (false);
		} else if (m_BikeControl != null) {
			Health bikeHealth = m_BikeControl.GetComponent<Health> ();
			bikeHealth.SetHealthAmount (bikeHealth.MaxHealth);
			m_BikeControl.GetComponent<ExplodeCar> ().steamFX.SetActive (false);
		}
	} 

	public VehicleGun getVehicleGun() {
		return gun;
	}

	public void SetShieldEnabled(bool enabled) {
		if (carComponents != null) {
			carComponents.GetComponent<Health> ().Invincible = enabled;
		} else if (bikeComponents != null) {
			bikeComponents.GetComponent<Health> ().Invincible = enabled;
		}
	}
}


















