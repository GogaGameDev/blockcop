﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Opsive.ThirdPersonController;

public class ExplodeCar : MonoBehaviour {

	public GameObject explosionPrefab;
	public float bodyForce = 100f, wheelForce = 100f;
	public Material burnedMaterial;
	public GameObject steamFX;
	public MeshRenderer bodyRenderer;
	[System.NonSerialized] private GameObject m_GameObject;
	bool exploded = false;

	// Use this for initialization
	void Start () {
		m_GameObject = gameObject;
		EventHandler.RegisterEvent(m_GameObject, "OnDeath", Explode);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.E)) {
			Health hitHealth = GetComponent<Health>();
			hitHealth.Damage(1000f, hitHealth.transform.position, Vector3.zero, null, hitHealth.transform.gameObject);
		}
	}

	IEnumerator DeathDelay(float delay) {
		yield return new WaitForSeconds (delay);
		Health hitHealth = GameObject.FindWithTag ("Player").GetComponent<Health>();
		hitHealth.InstantDeath ();
	}

	public void Explode() {
		if (exploded)
			return;
		exploded = true;
		Destroy (GetComponent<VehicleOptimizer> ());
		AchievementController.Instance.explosionCounter.Inc ();
		if (GetComponent<AIVehicle> ().vehicleStatus == VehicleStatus.Player) {
			GameObject.FindWithTag ("Player").GetComponent<DriveVehicle> ().OnCrash ();
			//GameControl.manager.GetOutVehicle ();
			//StartCoroutine (DeathDelay (1f));
		} else if (GetComponent<AIVehicle> ().vehicleStatus == VehicleStatus.AI) {
			GetComponent<CarComponents> ().GetOffDriver ();
		}
		steamFX.SetActive (false);
		if (GetComponent<VehicleControl> ()) {
			GameObject wfr = GetComponent<VehicleControl> ().carWheels.wheels.frontRight.gameObject;
			GameObject wfl = GetComponent<VehicleControl> ().carWheels.wheels.frontLeft.gameObject;
			GameObject wbr = GetComponent<VehicleControl> ().carWheels.wheels.backRight.gameObject;
			GameObject wbl = GetComponent<VehicleControl> ().carWheels.wheels.backLeft.gameObject;
			GetComponent<CarComponents> ().handleTrigger.gameObject.SetActive (false);
			GetComponent<CarComponents> ().enabled = false;
			GetComponent<AIVehicle> ().enabled = false;
			GetComponent<VehicleControl> ().enabled = false;
			bodyRenderer.material = burnedMaterial;
			AddWheelForce (wfr, transform.right * wheelForce);
			AddWheelForce (wfl, transform.right * wheelForce * -1f);
			AddWheelForce (wbr, transform.right * wheelForce);
			AddWheelForce (wbl, transform.right * wheelForce * -1f);
			GetComponent<BoxCollider> ().material = null;
		} else {
			GameObject wheelFront = GetComponent<BikeControl> ().bikeWheels.wheels.wheelFront.gameObject;
			GameObject wheelBack = GetComponent<BikeControl> ().bikeWheels.wheels.wheelBack.gameObject;
			GetComponent<BikeComponents> ().handleTrigger.gameObject.SetActive (false);
			GetComponent<BikeComponents> ().enabled = false;
			GetComponent<AIVehicle> ().enabled = false;
			GetComponent<BikeControl> ().enabled = false;
			bodyRenderer.material = burnedMaterial;
			AddWheelForce (wheelFront, transform.forward * wheelForce);
			AddWheelForce (wheelBack, transform.forward * wheelForce * -1f);
		}
		//GetComponent<Rigidbody> ().drag = 0;
		//GetComponent<Rigidbody> ().angularDrag = 0;
		GetComponent<Rigidbody> ().mass = 100;
		GetComponent<Rigidbody> ().AddForce (Vector3.up * bodyForce);
		Destroy (gameObject, 5f);
		RemoveDecals ();
		foreach (Light light in GetComponentsInChildren<Light>())
			light.enabled = false;
		foreach (AudioSource audio in GetComponentsInChildren<AudioSource>())
			audio.mute = true;

		AIContoller.manager.currentVehicles--;
		CreateAI.manager.CreateVehicle ();
	}

	void AddWheelForce(GameObject wheel, Vector3 direction) {
		wheel.transform.parent = null;
		wheel.AddComponent<BoxCollider> ();
		if (!wheel.GetComponent<Rigidbody>())
			wheel.AddComponent<Rigidbody> ().AddForce (direction * wheelForce);
		Destroy (wheel, 5f);
	}

	public void LowHealth() {
		steamFX.SetActive (true);
	}

	void RemoveDecals() {
		foreach (DecalRemover decal in GetComponentsInChildren<DecalRemover>())
			decal.RemoveDecal ();
	}
		
}
