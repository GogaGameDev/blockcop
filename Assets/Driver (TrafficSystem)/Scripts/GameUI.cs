﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BehaviorDesigner.Runtime;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Opsive.ThirdPersonController;
using UnityEngine.Advertisements;
using EventHandler = Opsive.ThirdPersonController.EventHandler;
using Random = UnityEngine.Random;
using OTO;

public class GameUI : MonoBehaviour
{

	public static GameUI Instance;

    public static GameObject instPlayerIcon;

    public Panels panels;
	public Button pauseButton;
    public VehicleUI vehicleUI;
	public Text missionText, sessionCoinsText, totalScoreText, sessionKillsText;
	public Text endScoreText, endCoinsText, endKillsText, noPrizesText, halosNeededText;
	public BaseMenu misteryBoxPanel, prizeWonPanel, boostersMenu, confirmExitMenu;
	public PrizeLabel haloPrize, xPrize, magnetPrize, healthPrize, gearPrize, shieldPrize; 
	public Button doublePrizeButton;
	public Slider shieldEnergySlider, magnetEnergySlider;
	public GameObject vehicleShootButton;
	public Image vehicleCrosshairImage;
	public EventSystem eventSystem;

    private Transform player;
    private Transform camera;

   
    private int gearst = 0;
    private float thisAngle = -150;
    private AIVehicle AIVehicleComponent;
    private Vector3 curPosBigMap;// current position of big map
	public Slider carHealthSlider, carShieldSlider;

	private int referenceDPI = 100;
	private float referencePixelDrag = 8f;

    [System.Serializable]
    public class Panels
    {
        public GameObject tachometer;
        public GameObject vehicleControl;
        public GameObject playerControl;
		public GameObject pausePanel, loadingPanel;
		public GameObject carHealthRoot;
		public CanvasGroup loosePanel, winPanel, rewardContinuePanel, dailyRewardPanel;
		public AchievementsMenu achievementsMenu;
		public AchievementCompletedMenu achievementCompletedMenu;
		public BaseMenu coinsNotificationPanel, restoreCopHealthConfirmPanel, restoreCarHealthConfirmPanel;
		public Text continueCounterText, restoreHaloCounterText, restoreGearCounterText;
    }

    [System.Serializable]
    public class VehicleUI
    {
        public Image tachometerNeedle;
        public Image barShiftGUI;

        public Text speedText;
        public Text gearText;
    }

	[System.Serializable]
	public class PrizeLabel
	{
		public Image icon;
		public Text counter;
	}
	
	private BannerView bannerView;
	private InterstitialAd interstitialAd;
	private RewardedAd admobRewardedAd;

	private const string ADMOB_BANNER_ID_ANDROID = "ca-app-pub-3940256099942544/6300978111";
	private const string ADMOB_INTERSTITIAL_ID_ANDROID = "ca-app-pub-3940256099942544/1033173712";
	
	private const string ADMOB_BANNER_ID_IOS = "ca-app-pub-8596057543629926/4762610623";
	private const string ADMOB_INTERSTITIAL_ID_IOS = "ca-app-pub-8596057543629926/3449528952";
	private const string ADMOB_REWARDEDVIDEO_ID_IOS = "ca-app-pub-8596057543629926/7324505769";


	private const string UNITYAD_BANNER_ID_IOS = "banner";
	private const string UNITY_AD_INTERSTITIAL_ID_IOS = "video";

	private bool canContinue = true;
	private bool isRewardedVideoLoading = false;
	private bool isRewardedVideoCancelled = false;
	private bool isRewardedVideoShown = false;
	private bool adsEnabled = true;
	private int misteryBoxes = 0;
    ////////////////////////////////////////////////////////////

    public void ShowVehicleUI()
    {

        AIVehicleComponent = AIContoller.manager.vehicleCamera.target.GetComponent<AIVehicle>();
        //if (!panels.tachometer.activeSelf) panels.tachometer.SetActive(true);


       /* gearst = AIVehicleComponent.currentGear;
        vehicleUI.speedText.text = ((int)AIVehicleComponent.vehicleSpeed).ToString();

        if (AIVehicleComponent.automaticGear)
        {

            if (gearst > 0 && AIVehicleComponent.vehicleSpeed > 1)
            {
                vehicleUI.gearText.color = Color.green;
                vehicleUI.gearText.text = gearst.ToString();
            }
            else if (AIVehicleComponent.vehicleSpeed > 1)
            {
                vehicleUI.gearText.color = Color.red;
                vehicleUI.gearText.text = "R";
            }
            else
            {
                vehicleUI.gearText.color = Color.white;
                vehicleUI.gearText.text = "N";
            }
        }
        else
        {
            if (AIVehicleComponent.neutralGear)
            {
                vehicleUI.gearText.color = Color.white;
                vehicleUI.gearText.text = "N";
            }
            else
            {
                if (AIVehicleComponent.currentGear != 0)
                {
                    vehicleUI.gearText.color = Color.green;
                    vehicleUI.gearText.text = gearst.ToString();
                }
                else
                {

                    vehicleUI.gearText.color = Color.red;
                    vehicleUI.gearText.text = "R";
                }
            }
        }*/

        thisAngle = (AIVehicleComponent.motorRPM / 20) - 175;
        thisAngle = Mathf.Clamp(thisAngle, -180, 90);

       // vehicleUI.tachometerNeedle.rectTransform.rotation = Quaternion.Euler(0, 0, -thisAngle);
       // vehicleUI.barShiftGUI.rectTransform.localScale = new Vector3(AIVehicleComponent.powerShift / 100.0f, 1, 1);
    }

	void Awake() {
		Instance = this;
	}

    void Start()
    {
		player = AIContoller.manager.playerCamera.Character.transform;
        camera = AIContoller.manager.playerCamera.transform;
		//carHealthSlider = panels.carHealthRoot.GetComponentInChildren<Slider> ();
		UpdateTotalCoinsLabel ();
		if (PlayerPrefs.GetInt ("sound", 1) == 1) {
			AudioListener.volume = 1;
		} else {
			AudioListener.volume = 0;
		}
		if (Application.internetReachability == NetworkReachability.NotReachable)
			canContinue = false;

//		if (GoogleMobileAd.GetBanner(PlayerPrefs.GetInt("banner_id", 0)) == null) {
//			banner = GoogleMobileAd.CreateAdBanner(TextAnchor.UpperCenter, GADBannerSize.BANNER);
//			banner.Show();
//			PlayerPrefs.SetInt("banner_id", banner.id);
//		} else {
//			banner = GoogleMobileAd.GetBanner(PlayerPrefs.GetInt("banner_id"));
//			if (Random.Range(0, 3) != 0)
//				banner.Show();
//			else
//				banner.Hide();
//		}
		adsEnabled = PlayerPrefs.GetInt("ads", 1) == 1;
//		if (adsEnabled && Random.Range (0, 2) == 1)
//			Chartboost.cacheInterstitial (CBLocation.Default);
		/*if (PlayerPrefs.HasKey ("continue")) {
			PlayerPrefs.DeleteKey ("continue");
			canContinue = false;
		}*/
		ResetSessionPrizes ();
		eventSystem.pixelDragThreshold = Mathf.RoundToInt(Screen.dpi/ referenceDPI*referencePixelDrag);
		//admob
		string interstitialID = "", rewardedID = "", bannerID = "";
#if UNITY_ANDROID
		bannerID = ADMOB_BANNER_ID_ANDROID;
		interstitialID = ADMOB_INTERSTITIAL_ID_ANDROID;
#elif UNITY_IOS
				bannerID = ADMOB_BANNER_ID_IOS;
				interstitialID = ADMOB_INTERSTITIAL_ID_IOS;
		rewardedID = ADMOB_REWARDEDVIDEO_ID_IOS;
#endif
		MobileAds.Initialize(initStatus => { });

		interstitialAd = new InterstitialAd(interstitialID);

		// Called when an ad request has successfully loaded.
		this.interstitialAd.OnAdLoaded += HandleOnInterLoaded;
		// Called when an ad request failed to load.
		this.interstitialAd.OnAdFailedToLoad += HandleOnInterFailedToLoad;
		// Called when an ad is shown.
		this.interstitialAd.OnAdOpening += HandleOnInterOpened;
		// Called when the ad is closed.
		this.interstitialAd.OnAdClosed += HandleOnInterClosed;
		// Called when the ad click caused the user to leave the application.
		this.interstitialAd.OnAdLeavingApplication += HandleOnInterLeavingApplication;
		if (Random.Range(0, 2) == 1 && adsEnabled)
		{
			AdRequest request2 = new AdRequest.Builder().Build();
			interstitialAd.LoadAd(request2);
		}
		bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Top);
		bannerView.OnAdLoaded += OnBannerLoaded;
		// Called when an ad request has successfully loaded.
		// Called when an ad request failed to load.
		this.bannerView.OnAdFailedToLoad += this.HandleOnBannerFailedToLoad;
		// Called when an ad is clicked.
		this.bannerView.OnAdOpening += this.HandleOnBannerOpened;
		// Called when the user returned from the app after an ad click.
		this.bannerView.OnAdClosed += this.HandleRewardedAdClosed;
		// Called when the ad click caused the user to leave the application.
		this.bannerView.OnAdLeavingApplication += this.HandleOnBannerLeavingApplication;
		if (adsEnabled)
		{
			AdRequest request1 = new AdRequest.Builder().Build();
			bannerView.LoadAd(request1);
		}
		if (adsEnabled)
		{
			admobRewardedAd = new RewardedAd(rewardedID);
			AdRequest requestVideo = new AdRequest.Builder().Build();
			admobRewardedAd.LoadAd(requestVideo);

			// Called when an ad request has successfully loaded.
			admobRewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
			// Called when an ad request failed to load.
			admobRewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
			// Called when an ad is shown.
			admobRewardedAd.OnAdOpening += HandleRewardedAdOpening;
			// Called when an ad request failed to show.
			admobRewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
			// Called when the user should be rewarded for interacting with the ad.
			admobRewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
			// Called when the ad is closed.
			admobRewardedAd.OnAdClosed += HandleRewardedAdClosed;
		}

		// UnityAds
		if (!Advertisement.isInitialized)
		{
			Advertisement.Initialize("4068586");
		}
		StartCoroutine(ShowBannerWhenReady_UnityAd());
		
	}
    
    void OnBannerLoaded(object sender, EventArgs args) {
		if (app_ads_flag.is_admob_enabled)
		{
			bannerView.Show();

			

		}
    }

    #region admob events
    public void HandleOnBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
							+ args.Message);
	}

	public void HandleOnBannerOpened(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdOpened event received");
		if (FireBaseAnalitycs.instance != null)
		{
			FireBaseAnalitycs.instance.OnBannerAdShown_Send();
		}
	}

	public void HandleOnBannerClosed(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdClosed event received");
	}

	public void HandleOnBannerLeavingApplication(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdLeavingApplication event received");
	}
	public void HandleOnInterLoaded(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdLoaded event received");
	}

	public void HandleOnInterFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
							+ args.Message);
	}

	public void HandleOnInterOpened(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdOpened event received");

		if (FireBaseAnalitycs.instance != null) {
			FireBaseAnalitycs.instance.OnIntersistialAdShown_Send();
		}
	}

	public void HandleOnInterClosed(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdClosed event received");
	}

	public void HandleOnInterLeavingApplication(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdLeavingApplication event received");
	}
    #endregion
    IEnumerator ShowBannerWhenReady_UnityAd()
	{
		if (app_ads_flag.is_unity_enabled)
		{
			
			while (!Advertisement.IsReady(UNITYAD_BANNER_ID_IOS))
			{
				yield return new WaitForSeconds(0.5f);
			}
			Advertisement.Banner.SetPosition(UnityEngine.Advertisements.BannerPosition.TOP_CENTER);
			Advertisement.Banner.Show(UNITYAD_BANNER_ID_IOS);

			if (FireBaseAnalitycs.instance != null) {
				FireBaseAnalitycs.instance.OnBannerAdShown_Send();
			}
		}
	}
	void Update()
    {
        /*if (panels.bigMap.activeSelf == false)
        {
            ShowMiniMapUI();*/
            if (AIContoller.manager.vehicleCamera.enabled)
            {
                ShowVehicleUI();
                if (panels.vehicleControl) panels.vehicleControl.SetActive(true); // only for touch mode
				if (panels.playerControl) SetPlayerControlsEnabled(false); // only for touch mode
            }
            else
            {
               // panels.tachometer.SetActive(false);
                if (panels.vehicleControl) panels.vehicleControl.SetActive(false); // only for touch mode
				if (panels.playerControl) SetPlayerControlsEnabled(true); // only for touch mode
            }
		/*}
        else
        {
            mapUI.bigMapView.orthographicSize = Mathf.Clamp(mapUI.bigMapView.orthographicSize, -1200, -200);
        }*/
//		if (Input.GetKeyDown (KeyCode.C))
//			OnRewarded (CBLocation.Default, 9);
//		if (Input.GetKeyDown (KeyCode.I))
//			player.GetComponent<Opsive.ThirdPersonController.CharacterHealth> ().InstantDeath ();
    }

	void SetPlayerControlsEnabled(bool enabled) {
		panels.playerControl.GetComponent<CanvasGroup> ().alpha = enabled ? 1f : 0f;
		panels.playerControl.GetComponent<CanvasGroup> ().blocksRaycasts = enabled ? true : false;
	}

	public void SetPaused(bool paused) {
		if (paused) {
			pauseButton.interactable = false;
			panels.pausePanel.SetActive (true);
			AudioListener.volume = 0f;
			Time.timeScale = 0;
		} else {
			pauseButton.interactable = true;
			panels.pausePanel.SetActive (false);
			if (PlayerPrefs.GetInt ("sound", 1) == 1) {
				AudioListener.volume = 1f;
			} 
			Time.timeScale = 1;
		}
	}

	bool isGameOver = false;

	public void OnGameOver(bool win, float delay) {
		if (isGameOver)
			return;
		isGameOver = true;
		GameController.Instance.canAddScore = false;
		if (win) {
			panels.winPanel.blocksRaycasts = true;
			LeanTween.alphaCanvas (panels.winPanel, 1f, 1f).setDelay (delay);
		} else {
			if (GameController.Instance.hasEnoughHalos()) {
				ShowContinueMenu ();
			} else {
				//GameController.Instance.SaveScore ();
				ShowAd();
				//AchievementController.Instance.strokeKillsCounter.value = 50;
				misteryBoxes = (int)(AchievementController.Instance.strokeKillsCounter.value / 10);
				if (misteryBoxes > 0) {
					misteryBoxPanel.Show ();
				} else {
					ShowGameOverMenu (delay);
				}
			}
		}
	}

	void ShowGameOverMenu(float delay) {
		int haloWon = PlayerPrefs.GetInt ("session_halo", 0);
		int multWon = PlayerPrefs.GetInt ("session_multiplier", 0);
		int magnetWon = PlayerPrefs.GetInt ("session_magnet", 0);
		int aidkitWon = PlayerPrefs.GetInt ("session_aidkit", 0);
		int gearWon = PlayerPrefs.GetInt ("session_gear", 0);
		int shieldWon = PlayerPrefs.GetInt ("session_shield", 0);
		int coinsWon = PlayerPrefs.GetInt ("session_coins", 0);
		if (haloWon == 0 && multWon == 0 && magnetWon == 0 &&
		    aidkitWon == 0 && gearWon == 0 && shieldWon == 0) {
			noPrizesText.gameObject.SetActive (true);
		} else {
			SetPrizeWonValue (haloPrize, haloWon);
			SetPrizeWonValue (xPrize, multWon);
			SetPrizeWonValue (magnetPrize, magnetWon);
			SetPrizeWonValue (healthPrize, aidkitWon);
			SetPrizeWonValue (gearPrize, gearWon);
			SetPrizeWonValue (shieldPrize, shieldWon);
		}
		endScoreText.text = "TOTAL SCORE: " + GameController.Instance.score.ToString();
		endCoinsText.text = "COINS EARNED: " + (GameController.Instance.sessionCoins + coinsWon).ToString();
		endKillsText.text = "KILLS: " + GameController.Instance.kills.ToString();
		panels.loosePanel.blocksRaycasts = true;
		LeanTween.alphaCanvas (panels.loosePanel, 1f, 1f).setDelay (delay).setIgnoreTimeScale(true);
		if (PlayerPrefs.GetInt ("highscore", 0) < GameController.Instance.score) {
			PlayerPrefs.SetInt ("highscore", GameController.Instance.score);
		}
		if (PlayerPrefs.GetInt ("killstreak", 0) < GameController.Instance.kills) {
			PlayerPrefs.SetInt ("killstreak", GameController.Instance.kills);
		}
	}

	void SetPrizeWonValue(PrizeLabel label, int value) {
		if (value > 0) {
			label.icon.gameObject.SetActive (true);
			label.counter.gameObject.SetActive (true);
			label.counter.text = "x" + value.ToString ();
		}
	}

	public void RestartGame() {
		Time.timeScale = 1;
		panels.pausePanel.SetActive (false);
		panels.loosePanel.gameObject.SetActive (false);
		panels.loadingPanel.SetActive (true);
		CleanScene ();
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void PrepareExitOnPause() {
		panels.pausePanel.SetActive (false);
		confirmExitMenu.Show ();
	}

	public void CancelExitOnPause() {
		panels.pausePanel.SetActive (true);
		confirmExitMenu.Hide ();
	}

	bool prepareExitOnPause = false;
	public void ConfirmExitOnPause() {
		misteryBoxes = (int)(AchievementController.Instance.strokeKillsCounter.value / 10);
		if (misteryBoxes > 0) {
			confirmExitMenu.Hide ();
			misteryBoxPanel.Show ();
			//prepareExitOnPause = true;
		} else {
			confirmExitMenu.Hide ();
			//ExitGame ();
			ShowGameOverMenu (0f);
		}
	}

	public void ExitGame() {
		Time.timeScale = 1;
		panels.pausePanel.SetActive (false);
		panels.loosePanel.gameObject.SetActive (false);
		panels.loadingPanel.SetActive (true);
		if (PlayerPrefs.HasKey("continue"))
			PlayerPrefs.DeleteKey ("continue");
		CleanScene ();
		AudioListener.volume = 1f;
		SceneManager.LoadScene (1);
	}

	public void LoadNextLevel() {
		Time.timeScale = 1;
		panels.pausePanel.SetActive (false);
		panels.loosePanel.gameObject.SetActive (false);
		panels.loadingPanel.SetActive (true);
		CleanScene ();
		if (SceneManager.GetActiveScene ().buildIndex >= SceneManager.sceneCountInBuildSettings - 1) {
			SceneManager.LoadScene (1);
		} else {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex+1);
		}
	}

	public void FreeWalk() {
		panels.winPanel.blocksRaycasts = false;
		panels.winPanel.alpha = 0f;
	}

	void CleanScene() {
		foreach (GameObject human in GameObject.FindGameObjectsWithTag("Human")) {
			DestroyImmediate (human);
		}
		foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
			DestroyImmediate (enemy);
		}
	}

	void OnDestroy() {
//		GoogleMobileAd.OnRewardedVideoAdFailedToLoad -= OnRewardedVideoFailedToLoad;
//		GoogleMobileAd.OnRewardedVideoLoaded -= OnRewardedVideoLoaded;
//		GoogleMobileAd.OnRewarded -= OnRewarded;
	}

	public void UseHaloButtonEvent() {
		isGameOver = false;
		GameController.Instance.canAddScore = true;
		GameController.Instance.Revive ();
		LeanTween.cancel (gameObject);
		panels.rewardContinuePanel.blocksRaycasts = false;
		panels.rewardContinuePanel.GetComponent<BaseMenu>().Hide();
	}

	public void UseHaloCancelButtonEvent() {
		panels.rewardContinuePanel.blocksRaycasts = false;
		panels.rewardContinuePanel.GetComponent<BaseMenu>().Hide();
		ShowAd();
		misteryBoxes = (int)(AchievementController.Instance.strokeKillsCounter.value / 10);
		if (misteryBoxes > 0) {
			misteryBoxPanel.Show ();
		} else {
			ShowGameOverMenu (0f);
		}
		GameController.Instance.SaveScore ();
//		if (Chartboost.hasInterstitial (CBLocation.Default)) {
//			Chartboost.showInterstitial (CBLocation.Default);
//		}
	}

	int counter = 5;

	public void ShowContinueMenu() {
		//StartCoroutine ("ContinueTimer");
		halosNeededText.text = "for " + GameController.Instance.halosNeeded.ToString();
		Time.timeScale = 1f;
		panels.rewardContinuePanel.gameObject.SetActive (true);
		panels.rewardContinuePanel.blocksRaycasts = true;
		panels.rewardContinuePanel.GetComponent<BaseMenu>().Show();
		panels.continueCounterText.text = "5";
		counter = 5;
		LeanTween.delayedCall (gameObject, 1f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
		LeanTween.delayedCall (gameObject, 2f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
		LeanTween.delayedCall (gameObject, 3f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
		LeanTween.delayedCall (gameObject, 4f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
//		LeanTween.delayedCall (gameObject, 5f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
//		LeanTween.delayedCall (gameObject, 6f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
//		LeanTween.delayedCall (gameObject, 7f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
//		LeanTween.delayedCall (gameObject, 8f, () => DecreaseTimer ()).setIgnoreTimeScale(true);
//		LeanTween.delayedCall (gameObject, 9f, () => DecreaseTimer ()).setIgnoreTimeScale (true);
		LeanTween.delayedCall (gameObject, 5, () => UseHaloCancelButtonEvent ()).setIgnoreTimeScale (true);
	}

	void DecreaseTimer() {
		counter--;
		panels.continueCounterText.text = counter.ToString();
//		Debug.Log ("kbg");
	}



	public void ShowAdRandom() {
		if (app_ads_flag.is_admob_enabled)
		{
			if (adsEnabled && Random.Range(0, 10) == 5 && interstitialAd.IsLoaded())
			{
				interstitialAd.Show();
				StartCoroutine(LoadNextAd(1f));
			}
		}

		if (app_ads_flag.is_unity_enabled)
		{
			// show unity intersistial
			if (adsEnabled && Random.Range(0, 10) == 5 && interstitialAd.IsLoaded())
			{
				interstitialAd.Show();
				StartCoroutine(LoadNextAd(1f));
			}
		}
	}

	public void ShowAd() {
		if (app_ads_flag.is_admob_enabled)
		{
			if (adsEnabled && interstitialAd.IsLoaded())
			{
				interstitialAd.Show();
				StartCoroutine(LoadNextAd(1f));
			}
		}

		if (app_ads_flag.is_unity_enabled)
		{
			if (adsEnabled && Advertisement.IsReady(UNITY_AD_INTERSTITIAL_ID_IOS))
			{
				Advertisement.Show(UNITY_AD_INTERSTITIAL_ID_IOS);
			}
		}
	}
	
	IEnumerator LoadNextAd(float delay)
	{
		yield return new WaitForSeconds(delay);
		AdRequest request = new AdRequest.Builder().Build();
		interstitialAd.LoadAd(request);
	}

	public void SetDailyRewardMenuVisible(bool visible) {
		panels.dailyRewardPanel.blocksRaycasts = visible;
		panels.dailyRewardPanel.alpha = visible ? 1f : 0f;
	}

	public void SetAchievementsPanelVisible(bool visible) {
		if (visible) {
			AchievementController.Instance.SaveAll ();
		}
		SetPanelVisible (panels.achievementsMenu, visible);
	}

	public void OnAchievementCompleted(string name, int reward, string rewardName) {
		panels.achievementCompletedMenu.nameText.text = name;
		panels.achievementCompletedMenu.rewardText.text = "REWARD: " + reward.ToString ();
		panels.achievementCompletedMenu.rewardIconImage.sprite = panels.achievementsMenu.getRewardSprite (rewardName);
		SetAchievementCompletedPanelVisible (true);
	}


	public void SetAchievementCompletedPanelVisible(bool visible) {
		SetPanelVisible (panels.achievementCompletedMenu, visible);
	}

	private int haloCounter = 1, gearCounter = 1;
	public void RestoreHealthButtonEvent(bool forPlayer) {
//		int coins = PlayerPrefs.GetInt ("coins", 0);
//		if (coins >= 1000) {
//			SetPanelVisible (panels.restoreCopHealthConfirmPanel, true);
//		} else {
//			SetPanelVisible (panels.coinsNotificationPanel, true);
//		}
		if (forPlayer) {
			if (PlayerPrefs.GetInt ("halo", 0) >= haloCounter) {
				panels.restoreHaloCounterText.text = haloCounter.ToString ();
				SetPanelVisible (panels.restoreCopHealthConfirmPanel, true);
			}
		} else {
			if (PlayerPrefs.GetInt ("gear", 0) >= gearCounter) {
				panels.restoreGearCounterText.text = gearCounter.ToString ();
				SetPanelVisible (panels.restoreCarHealthConfirmPanel, true);
			}
		}
	}

	public void ConfirmRestoreHealth(bool forPlayer) {
		//DecreaseTotalCoins (1000);
		if (forPlayer) {
			CharacterHealth health = player.GetComponent<CharacterHealth> ();
			health.SetHealthAmount (health.MaxHealth);
			SetPanelVisible (panels.restoreCopHealthConfirmPanel, false);
			PlayerPrefs.SetInt ("halo", PlayerPrefs.GetInt ("halo", 0) - haloCounter);
			haloCounter = haloCounter * 2;
		} else {
			player.GetComponent<DriveVehicle> ().RestoreCarHealth ();
			SetPanelVisible (panels.restoreCarHealthConfirmPanel, false);
			PlayerPrefs.SetInt ("gear", PlayerPrefs.GetInt ("gear", 0) - gearCounter);
			gearCounter = gearCounter * 2;
		}

	}

	public void CancelRestoreHealth(bool forPlayer) {
		if (forPlayer) {
			SetPanelVisible (panels.restoreCopHealthConfirmPanel, false);
		} else {
			SetPanelVisible (panels.restoreCarHealthConfirmPanel, false);
		}
	}

	void SetPanelVisible(BaseMenu panel, bool visible) {
		Time.timeScale = visible ?  0 : 1;
		if (visible) {
			panel.Show ();
		} else {
			panel.Hide ();
		}
	}

	public void InitCarHealth(GameObject car, Health health) {
		Debug.Log (health.CurrentShield);
		panels.carHealthRoot.SetActive (true);
		carHealthSlider.maxValue = health.MaxHealth;
		carHealthSlider.value = health.CurrentHealth;
		carShieldSlider.maxValue = health.MaxShield;
		carShieldSlider.value = health.CurrentShield;
		EventHandler.RegisterEvent<float>(car, "OnHealthAmountChange", UpdateCarHealth); 
		EventHandler.RegisterEvent<float>(car, "OnHealthShieldAmountChange", UpdateCarShield);
	}

	void UpdateCarHealth(float health) {
		carHealthSlider.value = health;
	}

	void UpdateCarShield(float shield) {
		carShieldSlider.value = shield;
	}

	public void HideCarHealth() {
		EventHandler.UnregisterEvent<float>("OnHealthAmountChange", UpdateCarHealth);
		EventHandler.UnregisterEvent<float>("OnHealthShieldAmountChange", UpdateCarShield);
		panels.carHealthRoot.SetActive (false);
	}

	public void HideNoCoinsNotification() {
		SetPanelVisible (panels.coinsNotificationPanel, false);
	}

	public void IncreaseTotalCoins(int value) {
		int coins = PlayerPrefs.GetInt ("coins", 0);
		coins += value;
		PlayerPrefs.SetInt ("coins", coins);
		UpdateTotalCoinsLabel ();
	}

	public void DecreaseTotalCoins(int value) {
		int coins = PlayerPrefs.GetInt ("coins", 0);
		coins -= value;
		PlayerPrefs.SetInt ("coins", coins);
		UpdateTotalCoinsLabel ();
	}

	public void UpdateTotalCoinsLabel() {
	//	totalScoreText.text = "Total Coins: " + PlayerPrefs.GetInt ("coins", 0).ToString("N0");
	}

	public void UpdateScoreLabel(int score) {
		totalScoreText.text = score.ToString("N0");
	}

	public void OpenMisteryBox() {
		misteryBoxes--;
		misteryBoxPanel.Hide ();
		prizeWonPanel.Show ();
	}

	public void ContinueToNextMisteryBox() {
		isRewardedVideoCancelled = true;
		doublePrizeButton.interactable = true;
		prizeWonPanel.Hide ();
		if (misteryBoxes > 0) {
			misteryBoxPanel.Show ();
		} else {
			if (prepareExitOnPause) {
				ExitGame ();
			} else {
				ShowGameOverMenu (0f);
			}
		}

	}

	public void DoublePrizeButtonEvent() {
		if (!isRewardedVideoLoading) {
			StartCoroutine(ShowAdWhenReady());
			doublePrizeButton.interactable = false;
			isRewardedVideoLoading = true;
		}
	}
	
	IEnumerator ShowAdWhenReady()
	{
		if (app_ads_flag.is_unity_enabled)
		{
			Debug.Log("I CALLED!! UnityAd");
			while (!Advertisement.IsReady("rewardedVideo"))
				yield return null;
			isRewardedVideoShown = true;
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhandler;

			Advertisement.Show("rewardedVideo", options);
		}

		if (app_ads_flag.is_admob_enabled)
		{
			Debug.Log("I CALLED!! Admob");
			while (!admobRewardedAd.IsLoaded())
				yield return null;
			isRewardedVideoShown = true;
			admobRewardedAd.Show();
		}

	}
	
	void AdCallbackhandler(ShowResult result)
	{
		if (result == ShowResult.Finished)
		{
			OnRewarded();

			if (FireBaseAnalitycs.instance != null) {
				FireBaseAnalitycs.instance.OnRewardAdShown_Send();
				FireBaseAnalitycs.instance.OnRewardAdCompleteInside_Send();
			}
		}
		else
		{
			OnRewardedVideoFailedToLoad();
		}
	}

	public void HandleRewardedAdLoaded(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdLoaded event received");
	}

	public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
	{
		OnRewardedVideoFailedToLoad();
	}

	public void HandleRewardedAdOpening(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdOpening event received");

		if (FireBaseAnalitycs.instance != null) {
			FireBaseAnalitycs.instance.OnRewardAdShown_Send();
		}
	}

	public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
	{
		MonoBehaviour.print(
			"HandleRewardedAdFailedToShow event received with message: "
							 + args.Message);
	}

	public void HandleRewardedAdClosed(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdClosed event received");
	}

	public void HandleUserEarnedReward(object sender, Reward args)
	{
		OnRewarded();

		if (FireBaseAnalitycs.instance != null) {
			FireBaseAnalitycs.instance.OnRewardAdCompleteInside_Send();
		}
	}

	//	public void RewardContinueButtonEvent() {
	//		if (isRewardedVideoLoading)
	//			return;
	//		isRewardedVideoLoading = true;
	//		//GoogleMobileAd.LoadRewardedVideo();
	//		Chartboost.cacheRewardedVideo(CBLocation.Default);
	//		LeanTween.cancel (gameObject);
	//		panels.continueCounterText.text = "LOADING";
	//	}


	private void OnRewardedVideoFailedToLoad() {
		if (isRewardedVideoCancelled)
			return;
		doublePrizeButton.interactable = false;
		//AndroidJNI//Androi AndroidMessage.Create("Can't load video", "Error " + id.ToString());
//		panels.rewardContinuePanel.blocksRaycasts = false;
//		panels.rewardContinuePanel.GetComponent<BaseMenu>().Hide();
//		panels.loosePanel.blocksRaycasts = true;
//		LeanTween.alphaCanvas (panels.loosePanel, 1f, 1f);
//		GameController.Instance.SaveScore ();
//		if (Chartboost.hasInterstitial (CBLocation.Default)) {
//			Chartboost.showInterstitial (CBLocation.Default);
//		}
	}

	private void OnRewarded() {
		prizeWonPanel.GetComponent<WonPrizeMenu> ().DoublePrize ();
	}

	void ResetSessionPrizes() {
		PlayerPrefs.SetInt ("session_halo", 0);
		PlayerPrefs.SetInt ("session_multiplier", 0);
		PlayerPrefs.SetInt ("session_magnet", 0);
		PlayerPrefs.SetInt ("session_aidkit", 0);
		PlayerPrefs.SetInt ("session_gear", 0);
		PlayerPrefs.SetInt ("session_shield", 0);
		PlayerPrefs.SetInt ("session_coins", 0);
	}

	public void SetBoostersPanelVisible(bool visible) {
		SetPanelVisible (boostersMenu, visible);
	}

	public void EnableShieldBoosterUI() {
		shieldEnergySlider.transform.parent.gameObject.SetActive (true);
		LeanTween.value(gameObject, (value) =>
			{
				shieldEnergySlider.value = value;
			}, 1f, 0f, 60f).setOnComplete(() => DisableShieldBoosterUI());
	}

	private void DisableShieldBoosterUI() {
		shieldEnergySlider.transform.parent.gameObject.SetActive (false);
		GameController.Instance.SetShieldBoosterEnabled (false);
	}

	public void EnableMagnetBoosterUI() {
		magnetEnergySlider.transform.parent.gameObject.SetActive (true);
		LeanTween.value(gameObject, (value) =>
			{
				magnetEnergySlider.value = value;
			}, 1f, 0f, 60f).setOnComplete(() => DisableMagnetBoosterUI());
	}

	private void DisableMagnetBoosterUI() {
		magnetEnergySlider.transform.parent.gameObject.SetActive (false);
		GameController.Instance.SetMagnetBoosterEnabled (false);
	}

	public void UseBooster(int id) {
		SetBoostersPanelVisible (false);
		switch (id) {
		case 0: //mult
			GameController.Instance.UpdateXBooster ();
			break;
		case 1: //shield
			EnableShieldBoosterUI ();
			GameController.Instance.SetShieldBoosterEnabled (true);
			break;
		case 2: //health
			CharacterHealth health = player.GetComponent<CharacterHealth> ();
			health.SetHealthAmount (health.MaxHealth);
			break;
		case 3: //gear
			player.GetComponent<DriveVehicle> ().RestoreCarHealth ();
			break;
		case 4: //magnet
			EnableMagnetBoosterUI();
			GameController.Instance.SetMagnetBoosterEnabled (true);
			break;
		}
	}

	public void SetVehicleShootEnabled(bool enabled) {
		GameController.Instance.SetVehicleFireEnabled (enabled);
	}

	public void SetVehicleGunEnabled(bool enabled, Sprite crosshairSprite) {
		if (enabled && !canUseGun ())
			return;
		vehicleShootButton.SetActive (enabled);
		vehicleCrosshairImage.gameObject.SetActive (enabled);
		if (enabled)
			vehicleCrosshairImage.sprite = crosshairSprite;

	}

	bool canUseGun() {
		if (PlayerPrefs.GetInt ("vehicle", 0) == 3) {
			return PlayerPrefs.GetInt ("vehicle3upgrade", 0) >= 4;
		} else if (PlayerPrefs.GetInt ("vehicle", 0) == 4) {
			return PlayerPrefs.GetInt ("vehicle4upgrade", 0) >= 4;
		}
		return false;
	}
}






