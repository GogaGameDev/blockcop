﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

namespace OTO
{
	public class IAP : MonoBehaviour
	{
		public static IAP _instance;

		[HideInInspector] public UnityEvent OnPurchase;
		[HideInInspector] public UnityEvent OnInitFail;
		[HideInInspector] public UnityEvent OnInitSucess;
		[HideInInspector] public UnityEvent OnPurchaseChecked;
		[HideInInspector] public UnityEvent OnPurchaseUnchecked;
		bool _check;
		[SerializeField] bool doNotDestroy;

		//Regular
		[HideInInspector] public ShopProductNames regular;
		[HideInInspector] public string regularID;
		[HideInInspector] public EventString OnRegularBuy = new EventString();

		private void Awake()
		{
			_instance = this;

			if (doNotDestroy)
			{
				DontDestroyOnLoad(this.gameObject);
			}
		}

		public void BuyIAP(ShopProductNames product, bool check)
		{
			_check = check;
			IAPManager.Instance.BuyProduct(product, ProductBoughtCallback);
		}
		public void BuyIAPRegular()
		{

			IAPManager.Instance.BuyProduct(regular, ProductBoughtCallback);
		}



		public void Init()
		{
			IAPManager.Instance.InitializeIAPManager(InitializeResultCallback);
		}

		private void InitializeResultCallback(IAPOperationStatus status, string message, List<StoreProduct>
	shopProducts)
		{
			Debug.Log("IAP Status" + status);
			if (status == IAPOperationStatus.Success)
			{
				OnInitSucess.Invoke();
				//IAP was successfully initialized
				//loop through all products
				Debug.Log("Product Count: " + shopProducts.Count);
				for (int i = 0; i < shopProducts.Count; i++)
				{

					Debug.Log("Product ID: " + shopProducts[i].GetStoreID());
				}

			}
			else
			{
				Debug.Log("Error occurred " + message);
				OnInitFail.Invoke();
			}
		}


		// automatically called after one product is bought
		// this is an example of product bought callback
		private void ProductBoughtCallback(IAPOperationStatus status, string message, StoreProduct
		product)
		{
			if (status == IAPOperationStatus.Success)
			{

				Debug.Log("SuccessFull Purchase: " + product.GetStoreID());
				if (product.GetStoreID() == regularID)
				{
					OnRegularBuy.Invoke(regularID);
				}
				OnPurchase.Invoke();

				if (_check)
				{
					OnPurchaseChecked.Invoke();
				}
				else
				{
					OnPurchaseUnchecked.Invoke();
				}
			}
			else
			{
				//an error occurred in the buy process, log the message for more details
				Debug.Log("Buy product failed: " + message);
			}
		}

		public bool IsInit()
		{
			return IAPManager.Instance.IsInitialized();
		}


	}
}
