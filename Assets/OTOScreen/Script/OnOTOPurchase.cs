﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OTO
{

    public class OnOTOPurchase : MonoBehaviour
    {
        public bool test;
        private void Awake()
        {
            if (test)
            {
                PlayerPrefs.DeleteAll();
            }
            IAP iap = GetComponent<IAP>();
            iap.OnPurchaseChecked.AddListener(OnChecked);
            iap.OnPurchaseUnchecked.AddListener(OnUncheacked);
        }

        void OnChecked()
        {
            FireBaseAnalitycs.instance.OnOTOBuyCheckMark_Send(GameProps.ottoOffer);
            Debug.Log("OnChecked Purchase");
            PlayerPrefs.SetInt("ads", 0);
            PlayerPrefs.SetInt("level" + 1 + "", 1);
            PlayerPrefs.SetInt("level" + 2 + "", 1);
            //value pack
            PlayerPrefs.SetInt("aidkit", PlayerPrefs.GetInt("aidkit", 0) + 17);
            PlayerPrefs.SetInt("gear", PlayerPrefs.GetInt("gear", 0) + 17);
            PlayerPrefs.SetInt("halo", PlayerPrefs.GetInt("halo", 0) + 6);
            PlayerPrefs.SetInt("multiplier", PlayerPrefs.GetInt("multiplier", 0) + 12);
            PlayerPrefs.SetInt("shield", PlayerPrefs.GetInt("shield", 0) + 12);
            PlayerPrefs.SetInt("magnet", PlayerPrefs.GetInt("magnet", 0) + 17);
            PlayerPrefs.SetInt("value_pack", 1);
        }
        void OnUncheacked()
        {
            Debug.Log("OnUncheacked Purchase");
            FireBaseAnalitycs.instance.OnOTOBuyWithoutCheckMark_Send(GameProps.ottoOffer);
            PlayerPrefs.SetInt("level" + 1 + "", 1);
            PlayerPrefs.SetInt("level" + 2 + "", 1);

            // value pack 
            PlayerPrefs.SetInt("aidkit", PlayerPrefs.GetInt("aidkit", 0) + 17);
            PlayerPrefs.SetInt("gear", PlayerPrefs.GetInt("gear", 0) + 17);
            PlayerPrefs.SetInt("halo", PlayerPrefs.GetInt("halo", 0) + 6);
            PlayerPrefs.SetInt("multiplier", PlayerPrefs.GetInt("multiplier", 0) + 12);
            PlayerPrefs.SetInt("shield", PlayerPrefs.GetInt("shield", 0) + 12);
            PlayerPrefs.SetInt("magnet", PlayerPrefs.GetInt("magnet", 0) + 17);
            PlayerPrefs.SetInt("value_pack", 1);

        }
    }
}
