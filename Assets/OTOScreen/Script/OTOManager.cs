﻿using Firebase;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using Firebase.Extensions;
using System;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.Events;

namespace OTO
{
    public class OTOManager : MonoBehaviour
    {
        FirebaseFirestore db;
        DocumentSnapshot _OTO1;
        [HideInInspector] public OTOSettings _oto_settings;
        [HideInInspector] public OTOScreen otoScreen;
        OTOScreenVisual screenVisual;
        [HideInInspector] public UnityEvent OnDataLoaded;
        [HideInInspector] public UnityEvent OnDataLoadFailed;
        [HideInInspector] public OTOEvent OnDataSend = new OTOEvent();


        private void Awake()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {

                db = FirebaseFirestore.DefaultInstance;
                Debug.Log("App Name: " + db.App.Name);
                GetAllData();
            });
        }
        // Start is called before the first frame update
        void Start()
        {
            screenVisual = GetComponent<OTOScreenVisual>();
        }



        void SetSettings(Dictionary<string, object> docDictonary)
        {

            _oto_settings = new OTOSettings(docDictonary);
            GameProps.ottoOffer = _oto_settings.offer_type;
            if (_oto_settings.hide_oto)
            {
                screenVisual.NextScene();
            }

        }
        void SetOtoScreen(Dictionary<string, object> docDictonary)
        {
            otoScreen = new OTOScreen(docDictonary);
            oto_inapIds.checked_iap_product_id = otoScreen.checked_iap_product_id;
            oto_inapIds.unchecked_iap_product_id = otoScreen.unchecked_iap_product_id;
            OnDataSend.Invoke(otoScreen);


            StartCoroutine(DownloadImage(otoScreen.promotion_imageLink));
        }
        IEnumerator DownloadImage(string url)
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                OnDataLoadFailed.Invoke();
            }
            else
            {
                Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                screenVisual.image.sprite = Sprite.Create(myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
                OnDataLoaded.Invoke();
            }


        }
        void GetAllData()
        {


            CollectionReference usersRef = db.Collection("digitmarket-config");
            usersRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
            {

                QuerySnapshot snapshot = task.Result;
                DocumentSnapshot doc = snapshot.Documents.FirstOrDefault(x => x.Id == "oto-settings");
                Dictionary<string, object> docDictonary = doc.ToDictionary();
                SetSettings(docDictonary);

                doc = snapshot.Documents.FirstOrDefault(x => x.Id == _oto_settings.offer_type);
                docDictonary = doc.ToDictionary();
                SetOtoScreen(docDictonary);

                DocumentSnapshot app_adsSetting = snapshot.Documents.FirstOrDefault(x => x.Id == "app-ads");
                if (app_adsSetting != null)
                {
                    SetAppSettings(app_adsSetting.ToDictionary());
                }
                Debug.Log("Read all data from  FireBase.");
            });
        }

        void SetAppSettings(Dictionary<string, object> appAdSettings)
        {

            app_ads_flag.is_admob_enabled = (bool)appAdSettings["is-admob-enabled"];
            app_ads_flag.is_fb_adnetwork_enabled = (bool)appAdSettings["is-fb-adnetwork-enabled"];
            app_ads_flag.is_unity_enabled = (bool)appAdSettings["is-unity-enabled"];
            app_ads_flag.is_unity_enabled = (bool)appAdSettings["is-ironsource-enabled"];
            app_ads_flag.number_of_clicks_for_instertitial = (int)((Int64)appAdSettings["number-of-clicks-for-instertitial"]);
            Debug.Log("app_ads_flag  Initialized");
        }

    }
}
