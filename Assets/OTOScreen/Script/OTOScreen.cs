﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OTO
{
    public class OTOScreen
    {
        public bool all_oto_iap_unlock_ads;
        public string checked_iap_product_id;
        public int discount_percent;
        public string discount_plus_ob_prefix;
        public string discount_plus_ob_prefix_ipad;
        public string discount_price_text_prefix;
        public string discount_price_text_prefix_ipad;
        public float discounted_plus_ob_price;
        public float discounted_price;
        public string ob_price_text_prefix;
        public string promotion_imageLink;
        public string regular_iap_product_id;
        public float regular_price;
        public string regular_price_prefix;
        public string regular_pricePrefix_ipad;
        public string regular_price_text_prefix;
        public string unchecked_iap_product_id;
        public string unlock_button_prefix;

        public OTOScreen(Dictionary<string, object> _params)
        {

            all_oto_iap_unlock_ads = (bool)_params["all-oto-iap-unlock-ads"];
            checked_iap_product_id = (string)_params["checked-iap-product-id"];
            discount_percent = (int)((Int64)_params["discount-percent"]);
            discount_plus_ob_prefix = (string)_params["discount-plus-ob-prefix"];
            discount_plus_ob_prefix_ipad = (string)_params["discount-plus-ob-prefix-ipad"];
            discount_price_text_prefix = (string)_params["discount-price-prefix"];
            discount_price_text_prefix_ipad = (string)_params["discount-price-prefix-ipad"];
            discounted_plus_ob_price = (float)((Double)_params["discounted-plus-ob-price"]);
            discounted_price = (float)((Double)_params["discounted-price"]);
            ob_price_text_prefix = (string)_params["ob-price-text-prefix"];
            promotion_imageLink = (string)_params["promotion-image"];
            regular_iap_product_id = (string)_params["regular-iap-product-id"];
            regular_price = (float)((Double)_params["regular-price"]);
            regular_price_prefix = (string)_params["regular-price-prefix"];
            regular_pricePrefix_ipad = (string)_params["regular-price-prefix-ipad"];
            regular_price_text_prefix = (string)_params["regular-price-text-prefix"];
            unchecked_iap_product_id = (string)_params["unchecked-iap-product-id"];
            unlock_button_prefix = (string)_params["unlock-button-prefix"];
        }
    }
}
