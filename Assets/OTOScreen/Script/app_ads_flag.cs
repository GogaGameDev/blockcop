﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace OTO
{
    public static class app_ads_flag
    {
        public static bool is_admob_enabled = true;
        public static bool is_fb_adnetwork_enabled = false;
        public static bool is_unity_enabled = true;
        public static bool is_ironsource_enabled = false;
        public static int number_of_clicks_for_instertitial = 1;
    }
}
