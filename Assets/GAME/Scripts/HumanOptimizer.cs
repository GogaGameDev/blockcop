﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanOptimizer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (!GetComponent<Renderer>().isVisible)
			SetHumanActive (false);
	}
	
	void OnBecameVisible() {
		SetHumanActive (true);
	}

	void OnBecameInvisible() {
		SetHumanActive (false);
	}

	void SetHumanActive(bool active) {
		GameObject root = transform.root.gameObject;
		if (!root.GetComponent<Opsive.ThirdPersonController.Health> ().IsAlive ())
			return;
		if (root.GetComponent<Rigidbody> ())
			root.GetComponent<Rigidbody> ().isKinematic = !active;
		if (root.GetComponent<AICharacterControl> ())
			root.GetComponent<AICharacterControl> ().enabled = active;
		if (root.GetComponent<ThirdPersonCharacter> ())
			root.GetComponent<ThirdPersonCharacter> ().enabled = active;
		if (root.GetComponent<NavMeshAgent> ())
			root.GetComponent<NavMeshAgent> ().enabled = active;
		if (root.GetComponent<Animator> ())
			root.GetComponent<Animator> ().enabled = active;
	}
}
