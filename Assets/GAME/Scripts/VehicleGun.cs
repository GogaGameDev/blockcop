﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Opsive.ThirdPersonController;

public class VehicleGun : MonoBehaviour {

	public enum GunType
	{
		AssaultRifle, Projectile
	}

	public GameObject weaponRoot;
	public GunType type = GunType.AssaultRifle;
	public Sprite crosshairSprite;
	public float fireRate = 2f;
	public int ammo = 100;

	public LayerMask m_HitscanImpactLayers;
	public float m_HitscanDamageAmount = 7f;
	[SerializeField] protected float m_HitscanImpactForce = 5;
	[SerializeField] protected GameObject m_Projectile;
	[SerializeField] protected Transform m_FirePoint;
	[SerializeField] protected Transform projectileRotationPoint;
	[SerializeField] protected GameObject m_MuzzleFlash;
	[Tooltip("If Muzzle Flash is specified, the location is the position and rotation that the muzzle flash spawns at")]
	[SerializeField] protected Transform m_MuzzleFlashLocation;
	[Tooltip("Optionally specify any smoke that should appear when the weapon is fired")]
	[SerializeField] protected GameObject m_Smoke;
	[Tooltip("If Smoke is specified, the location is the position and rotation that the smoke spawns at")]
	[SerializeField] protected Transform m_SmokeLocation;
	[Tooltip("Optionally specify a sound that should play when the weapon is fired")]
	[SerializeField] protected AudioClip m_FireSound;
	[Tooltip("If Fire Sound is specified, play the sound after the specified delay")]
	[SerializeField] protected float m_FireSoundDelay;


	private AudioSource m_AudioSource;

	private float m_LastFireTime;

	private bool isFiring = false, isGunActive = false;

	private RectTransform crossHair;

	// Use this for initialization
	void Start () {
		m_AudioSource = weaponRoot.GetComponent<AudioSource> ();
	}
	
	void Update() {
		if (isFiring) {
			Fire();
		}
		if (isGunActive) {
			Vector3 directedPos = m_FirePoint.transform.position + (m_FirePoint.transform.forward );

			Vector3 vpPosition = Camera.main.WorldToViewportPoint(directedPos);
			crossHair.transform.position = Camera.main.WorldToViewportPoint(directedPos);
			Vector3 pos;
			pos.x = Screen.width  * vpPosition.x - Screen.width / 2f;
			pos.y = vpPosition.y;
			pos.z = 0f;

			crossHair.transform.localPosition = pos;

		}
	//	Debug.DrawRay (m_FirePoint.position, transform.forward, Color.blue);
	}

	public void SetFireEnabled(bool enabled) {
		isFiring = enabled;
	}

	void Fire() {
		if (ammo <= 0)
			return;
		if (type == GunType.AssaultRifle) {
			FireHitscan ();
		} else {
			FireProjectile ();
		}
	}

	void FireHitscan() {
		if (Time.time - m_LastFireTime < fireRate)
			return;
		m_LastFireTime = Time.time;
		RaycastHit m_RaycastHit;
		if (Physics.Raycast(m_FirePoint.position, transform.forward, out m_RaycastHit, 10000f, m_HitscanImpactLayers.value)) {
			// If the Health component exists it will apply a force to the rigidbody in addition to deducting the health. Otherwise just apply the force to the rigidbody. 
			Health hitHealth;
			if ((hitHealth = m_RaycastHit.transform.GetComponentInParent<Health> ()) != null) {
				hitHealth.Damage (m_HitscanDamageAmount, m_RaycastHit.point, m_FirePoint.forward * m_HitscanImpactForce, null, m_RaycastHit.transform.gameObject);
				if (hitHealth.CurrentHealth <= 0f) {
					AchievementController.Instance.copCarKillsCounter.Inc ();
				}
			} else if (m_HitscanImpactForce > 0 && m_RaycastHit.rigidbody != null && !m_RaycastHit.rigidbody.isKinematic) {
				m_RaycastHit.rigidbody.AddForceAtPosition (m_FirePoint.forward * m_HitscanImpactForce, m_RaycastHit.point);
			}
		}
		if (m_FireSound != null) {
			m_AudioSource.clip = m_FireSound;
			m_AudioSource.Play();
		}
		if (m_MuzzleFlash) {
			// Choose a random z rotation angle.
			var eulerAngles = m_MuzzleFlashLocation.eulerAngles;
			eulerAngles.z = Random.Range(0, 360);
			var muzzleFlashObject = ObjectPool.Instantiate(m_MuzzleFlash, m_MuzzleFlashLocation.position, Quaternion.Euler(eulerAngles), weaponRoot.transform);
			MuzzleFlash muzzleFlash;
			if ((muzzleFlash = Utility.GetComponentForType<MuzzleFlash>(muzzleFlashObject)) != null) {
				muzzleFlash.Show();
			}
		}
		ammo--;
	}

	void FireProjectile() {
		if (Time.time - m_LastFireTime < fireRate)
			return;
		m_LastFireTime = Time.time;

		// Spawn a projectile which will move in the direction that the turret is facing
		//Quaternion dir = Quaternion.LookRotation(-m_FirePoint.forward);


		var projectile = ObjectPool.Instantiate(m_Projectile, m_FirePoint.position, projectileRotationPoint.rotation).GetComponent<Projectile>();
		projectile.Initialize(transform.forward, Vector3.zero, gameObject);
		var projectileCollider = projectile.GetComponent<Collider>();

		// Ignore all of the turret's colliders to prevent the projectile from detonating as a result of the turret. 
		Collider[] m_Colliders = GetComponentsInChildren<Collider>();
		if (projectileCollider != null) {
			for (int i = 0; i < m_Colliders.Length; ++i) {
				LayerManager.IgnoreCollision(projectileCollider, m_Colliders[i]);
			}
		}

		// Spawn a muzzle flash.
		if (m_MuzzleFlash) {
			ObjectPool.Instantiate(m_MuzzleFlash, m_MuzzleFlashLocation.position, m_MuzzleFlashLocation.rotation, weaponRoot.transform);
		}

		// Spawn any smoke.
		if (m_Smoke) {
			ObjectPool.Instantiate(m_Smoke, m_SmokeLocation.position, m_SmokeLocation.rotation);
		}

		// Play a firing sound.
		if (m_FireSound != null && !m_AudioSource.isPlaying) {
			m_AudioSource.clip = m_FireSound;
			m_AudioSource.Play();
		}
		ammo--;
	}

	bool ammoUpgraded = false;
	public void SetAmmo(int value) {
		if (!ammoUpgraded) {
			ammoUpgraded = true;
			ammo = value;
			crossHair = GameUI.Instance.vehicleCrosshairImage.GetComponent<RectTransform>();
			crossHair.transform.localPosition = Vector3.zero;
		}
	}

	public void SetGunEnabled(bool enabled) {
		isGunActive = enabled;
	}
}
