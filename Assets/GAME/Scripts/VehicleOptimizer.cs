﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleOptimizer : MonoBehaviour {

	/*private float visibleArea = 200f;
	Transform player;*/

	// Use this for initialization
	void Start () {
		if (!GetComponent<Renderer>().isVisible)
			SetVehicleActive (false);
	}
	
	void OnBecameVisible() {
		SetVehicleActive (true);
	}

	void OnBecameInvisible() {
		SetVehicleActive (false);
	}

	void SetVehicleActive(bool active) {
		Transform root = transform.root;
		if (root.GetComponent<AIVehicle> ().vehicleStatus != VehicleStatus.Player) {
			root.GetComponent<Rigidbody> ().isKinematic = !active;
			root.GetComponent<CarComponents> ().enabled = active;
			root.GetComponent<AIVehicle> ().enabled = active;
			root.GetComponent<VehicleControl> ().enabled = active;
			root.GetComponent<ExplodeCar> ().enabled = active;
		}
	}
}
