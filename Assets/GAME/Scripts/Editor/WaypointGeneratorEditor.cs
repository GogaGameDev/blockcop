﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaypointGenerator))]
public class WaypointGeneratorEditor : Editor {

	public override void OnInspectorGUI() {
		base.OnInspectorGUI ();
		WaypointGenerator generator = target as WaypointGenerator;
		if (GUILayout.Button ("Generate Waypoints")) {
			generator.Generate ();
		}

		if (GUILayout.Button ("Clear Waypoints")) {
			generator.Clear ();
		}
	}
}
