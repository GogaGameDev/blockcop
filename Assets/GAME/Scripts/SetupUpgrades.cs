﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.UI;

public class SetupUpgrades : MonoBehaviour {

	public List<UpgradeParams> upgradeValues;

	[System.Serializable]
	public class UpgradeParams {
		public float runIncreasePercent;
		public float healthIncreasePercent;
		public float damageIncreasePercent;
		public float driveIncreasePercent;

		public UpgradeParams() {
			runIncreasePercent = 0f;
			healthIncreasePercent = 0f;
			damageIncreasePercent = 0f;
			driveIncreasePercent = 0f;
		}
	}

	private UpgradeParams upgradeParams;

	// Use this for initialization
	void Awake () {
		int skinID = PlayerPrefs.GetInt ("skin", 0);
		int copSpeed = PlayerPrefs.GetInt ("skin" + skinID + "speed", 0);
		int copHealth = PlayerPrefs.GetInt ("skin" + skinID + "health", 0);
		int copArmor = PlayerPrefs.GetInt ("skin" + skinID + "armor", 0);
		Debug.Log (copSpeed + " " + copHealth + " " + copArmor);
		float health = GetComponent<CharacterHealth> ().MaxHealth;
		GetComponent<CharacterHealth> ().MaxHealth = health + (25f * copHealth / 100f) * health;
		//GetComponent<CharacterHealth> ().SetHealthAmount (GetComponent<CharacterHealth> ().MaxHealth);
		GetComponent<CharacterHealth> ().MaxShield = (25f * copArmor / 100f) * 100f;
		//GetComponent<CharacterHealth> ().SetShieldAmount (GetComponent<CharacterHealth> ().MaxShield);
		//Invoke ("UpdateUI", 0.5f);
		float moveSpeedMult = GetComponent<RigidbodyCharacterController> ().RootMotionSpeedMultiplier;
		GetComponent<RigidbodyCharacterController> ().RootMotionSpeedMultiplier = moveSpeedMult + (10f * copSpeed / 100f) * moveSpeedMult;
		Debug.Log ("OK");
		//SetPlayerAbilities ()
//		int upgradeID = PlayerPrefs.GetInt ("skin" + skinID + "upgrade", 0);
//		if (upgradeID > 0) {
//			upgradeParams = upgradeValues [upgradeID-1];
//			SetPlayerAbilities ();
//		} else {
//			upgradeParams = new UpgradeParams ();
//		}
	}

	void Start() {

	}

	void SetPlayerAbilities() {
//		foreach (ShootableWeapon weapon in GetComponentsInChildren<ShootableWeapon>()) {
//			weapon.m_HitscanDamageAmount = weapon.m_HitscanDamageAmount + (upgradeParams.damageIncreasePercent / 100f) * weapon.m_HitscanDamageAmount;
//		}
//		foreach (MeleeWeapon weapon in GetComponentsInChildren<MeleeWeapon>()) {
//			weapon.m_DamageAmount = weapon.m_DamageAmount + (upgradeParams.damageIncreasePercent / 100f) * weapon.m_DamageAmount;
//		}
		float health = GetComponent<CharacterHealth> ().MaxHealth;
		GetComponent<CharacterHealth> ().MaxHealth = health + (upgradeParams.healthIncreasePercent / 100f) * health;
		Invoke ("UpdateUI", 0.2f);

		float moveSpeedMult = GetComponent<RigidbodyCharacterController> ().RootMotionSpeedMultiplier;
		GetComponent<RigidbodyCharacterController> ().RootMotionSpeedMultiplier = moveSpeedMult + (upgradeParams.runIncreasePercent / 100f) * moveSpeedMult;
	}

	void UpdateUI() {
		EventHandler.ExecuteEvent<float>(gameObject, "OnHealthAmountChange", GetComponent<CharacterHealth> ().MaxHealth);
	}

	public float getDriveSpeedPrecentage() {
		return upgradeParams.driveIncreasePercent;
	}
}
