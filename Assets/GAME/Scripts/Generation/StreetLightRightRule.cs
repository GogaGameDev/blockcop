﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonArchitect;

public class StreetLightRightRule : SelectorRule {

	public override bool CanSelect(PropSocket socket, Matrix4x4 propTransform, DungeonModel model, System.Random random) {
		if ((socket.gridPosition.x + socket.gridPosition.z) % 2 == 0) {
			return true;
		}
		return false;
	}
		
}

