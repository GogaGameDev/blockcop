﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonArchitect;
using DungeonArchitect.Builders.SimpleCity;
using DAShooter;

public class WaypointGenerator : DungeonEventListener {

	public SimpleCityDungeonModel model;
	public GameObject nodeTemplate, wayControlTemplate;
	public GameObject waypointParent;
	public Vector3 waypointOffset = Vector3.up;
	public float roadWidth = 4f;

	// Use this for initialization
	void Start () {
		
	}

	public override void OnDungeonMarkersEmitted(Dungeon dungeon, DungeonModel model, List<PropSocket> markers) 
	{
		BuildCityWaypoints((SimpleCityDungeonModel)model);
	}
	
	public void Generate() {
		//SimpleCityDungeonModel model = FindObjectOfType<SimpleCityDungeonModel> ();
		//BuildCityWaypoints(model);
	}

	int nameInc = 1;

	void BuildCityWaypoints(SimpleCityDungeonModel model)
	{
		var cells = model.Cells;
		var width = cells.GetLength(0);
		var height = cells.GetLength(1);
		var cellSize = new Vector3(model.Config.CellSize.x, 0, model.Config.CellSize.y);
		int idCounter = 1;
		var cellToWaypoint = new Dictionary<SimpleCityCell, Waypoint>();
		var adjacentWaypoints = new Dictionary<Waypoint, List<Waypoint>>();
		for (int x = 0; x < width; x++)
		{
			for (int z = 0; z < height; z++)
			{
				var cell = cells[x, z];
				if (cell.CellType == SimpleCityCellType.Road)
				{
					// Create a waypoint here
					var worldPos = Vector3.Scale(cellSize, new Vector3(x, 0, z));
					worldPos += waypointOffset;
					GameObject waypointObject;
					if (isStraightRoad (x, z, cells)) {
						waypointObject = Instantiate (nodeTemplate, worldPos, Quaternion.identity) as GameObject;
						nodeTemplate.GetComponent<Node> ().widthDistance = roadWidth;
						waypointObject.name = nameInc.ToString ();
						nameInc++;
					} else {
						waypointObject = Instantiate (wayControlTemplate, worldPos, Quaternion.identity) as GameObject;
						nameInc = 1;
					}

					waypointObject.transform.parent = waypointParent.transform;

					var waypoint = waypointObject.GetComponent<Waypoint>();
					adjacentWaypoints.Add(waypoint, new List<Waypoint>());
					waypoint.id = idCounter++;
					cellToWaypoint.Add(cell, waypoint);
				}
			}
		}

		for (int x = 0; x < width; x++)
		{
			for (int z = 0; z < height; z++)
			{
				var cell = cells[x, z];
				// connect to adjacent road tiles
				ConnectAdjacentRoadTiles(model, cell, 0, -1, cellToWaypoint, adjacentWaypoints);
				ConnectAdjacentRoadTiles(model, cell, -1, 0, cellToWaypoint, adjacentWaypoints);
			}
		}

		foreach (var waypoint in cellToWaypoint.Values)
		{
			waypoint.AdjacentWaypoints = adjacentWaypoints[waypoint].ToArray();
		}
		foreach (Waypoint wp in FindObjectsOfType<Waypoint>()) {
			if (wp.GetComponent<Node> ()) {
				Node node = wp.GetComponent<Node> ();
				node.previousNode = wp.AdjacentWaypoints [0].transform;
				node.nextNode = wp.AdjacentWaypoints [1].transform;
			} else if (wp.GetComponent<WaysControl> () && wp.AdjacentWaypoints.Length > 0) {
				WaysControl waysControl = wp.GetComponent<WaysControl> ();
				waysControl.ways = wp.AdjacentWaypoints.Length;
				waysControl.way1 = wp.AdjacentWaypoints [0].transform;
				if (wp.AdjacentWaypoints.Length > 1) 
					waysControl.way2 = wp.AdjacentWaypoints [1].transform;
				if (wp.AdjacentWaypoints.Length > 2) 
					waysControl.way3 = wp.AdjacentWaypoints [2].transform;
				if (wp.AdjacentWaypoints.Length > 3) 
					waysControl.way4 = wp.AdjacentWaypoints [3].transform;
			}
		}
		Debug.Log ("Complete");
	}

	void ConnectAdjacentRoadTiles(SimpleCityDungeonModel model, SimpleCityCell cell, int dx, int dz,
		Dictionary<SimpleCityCell, Waypoint> cellToWaypoint, Dictionary<Waypoint, List<Waypoint>> adjacentWaypoints)
	{
		int adjacentX = cell.Position.x + dx;
		int adjacentZ = cell.Position.z + dz;
		if (adjacentX < 0 || adjacentZ < 0) return;
		var adjacentCell = model.Cells[adjacentX, adjacentZ];
		if (cell.CellType == SimpleCityCellType.Road && adjacentCell.CellType == SimpleCityCellType.Road)
		{
			// Connect the two cells
			var waypoint1 = cellToWaypoint[cell];
			var waypoint2 = cellToWaypoint[adjacentCell];

			adjacentWaypoints[waypoint1].Add(waypoint2);
			adjacentWaypoints[waypoint2].Add(waypoint1);
		}
	}

	public void Clear() {
		var oldWaypoints = GameObject.FindObjectsOfType<Waypoint>();
		foreach (var waypoint in oldWaypoints) {
			if (Application.isPlaying) {
				Destroy(waypoint.gameObject);
			} else {
				DestroyImmediate(waypoint.gameObject);
			}
		}
	}

	bool isStraightRoad(int x, int z, SimpleCityCell[,] cells) {
		float angle = 0f; 
		return RoadBeautifier.GetRoadMarkerName (x, z, cells, out angle) == SimpleCityDungeonConstants.Road_S;
	}
}
