﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonArchitect;
using DungeonArchitect.Builders.SimpleCity;
using DungeonArchitect.Utils;

public class ZebraCrossingTransformRule : TransformationRule {

	public override void GetTransform(PropSocket socket, DungeonModel model, Matrix4x4 propTransform, System.Random random, out Vector3 outPosition, out Quaternion outRotation, out Vector3 outScale) {
		base.GetTransform(socket, model, propTransform, random, out outPosition, out outRotation, out outScale);
		SimpleCityCell[,] cells = ((SimpleCityDungeonModel)model).Cells;
		if (isCrossRoad(socket.gridPosition.x, socket.gridPosition.z+1, cells) || 
			isCrossRoad(socket.gridPosition.x-1, socket.gridPosition.z, cells)) {
			outRotation = Quaternion.Euler (0, 180, 0);
		} 
	}

	bool isCrossRoad(int x, int z, SimpleCityCell[,] cells) {
		float angle = 0f; 
		return RoadBeautifier.GetRoadMarkerName (x, z, cells, out angle) == SimpleCityDungeonConstants.Road_X;
	}
}
