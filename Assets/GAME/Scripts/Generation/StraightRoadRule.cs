﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonArchitect;
using DungeonArchitect.Builders.SimpleCity;
using DungeonArchitect.Utils;

public class StraightRoadRule : SelectorRule {

	public override bool CanSelect(PropSocket socket, Matrix4x4 propTransform, DungeonModel model, System.Random random) {
		SimpleCityCell[,] cells = ((SimpleCityDungeonModel)model).Cells;
		if (isCrossRoad(socket.gridPosition.x, socket.gridPosition.z-1, cells) || 
			isCrossRoad(socket.gridPosition.x, socket.gridPosition.z+1, cells) || 
			isCrossRoad(socket.gridPosition.x-1, socket.gridPosition.z, cells) ||
			isCrossRoad(socket.gridPosition.x+1, socket.gridPosition.z, cells)) {
			return false;
		} 
		return true;
	}

	bool isCrossRoad(int x, int z, SimpleCityCell[,] cells) {
		float angle = 0f; 
		return RoadBeautifier.GetRoadMarkerName (x, z, cells, out angle) == SimpleCityDungeonConstants.Road_X;
	}
}
