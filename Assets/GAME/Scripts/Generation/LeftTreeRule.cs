﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonArchitect;
using DungeonArchitect.Builders.SimpleCity;
using DungeonArchitect.Utils;


public class LeftTreeRule : SelectorRule {

	public override bool CanSelect(PropSocket socket, Matrix4x4 propTransform, DungeonModel model, System.Random random) {
		SimpleCityCell[,] cells = ((SimpleCityDungeonModel)model).Cells;
		if (isCrossRoad(socket.gridPosition.x, socket.gridPosition.z-1, cells) || 
			isCrossRoad(socket.gridPosition.x, socket.gridPosition.z+1, cells) || 
			isCrossRoad(socket.gridPosition.x-1, socket.gridPosition.z, cells) ||
			isCrossRoad(socket.gridPosition.x+1, socket.gridPosition.z, cells)) {
			return false;
		} 
		if (!cellExists(socket.gridPosition.x-1, socket.gridPosition.z, cells))
			return false;
		if (!cellExists(socket.gridPosition.x, socket.gridPosition.z-1, cells))
			return false;
		return true;
	}

	bool isCrossRoad(int x, int z, SimpleCityCell[,] cells) {
		float angle = 0f; 
		return RoadBeautifier.GetRoadMarkerName (x, z, cells, out angle) == SimpleCityDungeonConstants.Road_X ||
				RoadBeautifier.GetRoadMarkerName (x, z, cells, out angle) == SimpleCityDungeonConstants.Road_T ||
				RoadBeautifier.GetRoadMarkerName (x, z, cells, out angle) == SimpleCityDungeonConstants.Road_Corner;
	}

	bool cellExists(int x, int z, SimpleCityCell[,] cells) {
		var lx = cells.GetLength(0);
		var lz = cells.GetLength(1);
		if (x < 0 || z < 0 || x >= lx || z >= lz)
		{
			return false;
		}
		return true;
	}
}
