﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.ThirdPersonController;

public class DropBonusOnDeath : MonoBehaviour {

	public GameObject bonus;
	private bool dropped = false;

	// Use this for initialization
	void Start () {
		EventHandler.RegisterEvent<float>(gameObject, "OnHealthAmountChange", DropObject);
	}
	
	public void DropObject(float health) {
		if (health > float.Epsilon || dropped)
			return;
		dropped = true;
		Vector3 pos = transform.position;
		pos.y = bonus.transform.position.y;
		Instantiate (bonus, pos, Quaternion.identity);
		//EventHandler.UnregisterEvent("OnDeath", DropObject);
		GetComponent<bl_MiniMapItem> ().DestroyItem (true);
		GameController.Instance.DecreaseEnemies ();
		Destroy (this.gameObject, 3f);
	}

}
