﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.ThirdPersonController;

public class OnPlayerDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		EventHandler.RegisterEvent<float>(gameObject, "OnHealthAmountChange", GameOver);
	}

	public void GameOver(float health) {
		if (health > float.Epsilon)
			return;
		GameController.Instance.SetStickKinematic (true);
		AchievementController.Instance.deathCounter.Inc ();
		GameUI.Instance.OnGameOver (false, 2f);
	}
}
