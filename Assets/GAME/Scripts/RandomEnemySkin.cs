﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEnemySkin : MonoBehaviour {

	public List<Skin> skins;

	[System.Serializable]
	public class Skin
	{
		public SkinnedMeshRenderer mesh;
		public Material material;
	}

	// Use this for initialization
	void Start () {
		//GetComponent<SkinnedMeshRenderer> ().material.mainTexture = skins [Random.Range (0, skins.Length)];
		int skinID = Random.Range (0, skins.Count);
		GetComponent<SkinnedMeshRenderer> ().material = skins [skinID].material;
//		Debug.Log (skinID);
		skins [skinID].mesh.gameObject.SetActive (true);
		skins [skinID].mesh.material = skins [skinID].material;
	}
	
}
