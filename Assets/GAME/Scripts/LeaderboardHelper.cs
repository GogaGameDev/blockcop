﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*using SA.Android.GMS.Common;
using SA.Android.GMS.Auth;
using SA.Android.GMS.Games;
using SA.Android.Utilities;
using SA.Android.App;
using SA.iOS.GameKit;
using SA.Foundation.Templates;*/
using UnityEngine.SocialPlatforms;

public class LeaderboardHelper : MonoBehaviour
{
    public static LeaderboardHelper Instance;
    
    private string DISTANCE_LEADERBOARD_ID = "CgkI8rKN4YgJEAIQAQ";
    private string COINS_LEADERBOARD_ID = "CgkI8rKN4YgJEAIQAg";

    private string KILLS_LEADERBOARD_ID_IOS = "bcc_total_kills";
    private string COINS_LEADERBOARD_ID_IOS = "bcc_total_coins";
    private string COLLECTED_LEADERBOARD_ID_IOS = "bcc_guns_collected";
    private string TIME_LEADERBOARD_ID_IOS = "bcc_time_played";
    private string HIGHSCORE_LEADERBOARD_ID_IOS = "bcc_highscore";
    private string KILLSTREAK_LEADERBOARD_ID_IOS = "bcc_killstreak";
    private string MILESRUN_LEADERBOARD_ID_IOS = "bcc_milesrun";
    private string MILESDRIVEN_LEADERBOARD_ID_IOS = "bcc_milesdriven";
   
    private ILeaderboard leaderboardKills, leaderboardCoins, leaderboardCollected, leaderboardTime, 
                         leaderboardHighscore, leaderboardKillstreak, leaderboardMilesRun, leaderboardMilesDriven;
    
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    void ReportScoreAndShow()
    {
        Social.ReportScore(PlayerPrefs.GetInt("distance", 0), DISTANCE_LEADERBOARD_ID, (bool success) => {
            if (success)
            {
                Social.ReportScore(PlayerPrefs.GetInt("total_earned", 0), COINS_LEADERBOARD_ID, (bool success2) =>
                {
                    if (success2)
                    {
                        Social.ShowLeaderboardUI();  
                    }
                });
            }
        });
    }
    
    public void GameCenterButtonEvent()
    {
        if (Social.localUser.authenticated)
        {
            ShowGameCenter();
        }
        else
        {
            Social.localUser.Authenticate(success => {
                if (success)
                {
                    leaderboardKills = Social.CreateLeaderboard();
                    leaderboardKills.id = KILLS_LEADERBOARD_ID_IOS;
                    leaderboardKills.LoadScores(result => {});
                    leaderboardCoins = Social.CreateLeaderboard();
                    leaderboardCoins.id = COINS_LEADERBOARD_ID_IOS;
                    leaderboardCoins.LoadScores(result => {});
                    leaderboardCollected = Social.CreateLeaderboard();
                    leaderboardCollected.id = COLLECTED_LEADERBOARD_ID_IOS;
                    leaderboardCollected.LoadScores(result => {});
                    leaderboardTime = Social.CreateLeaderboard();
                    leaderboardTime.id = TIME_LEADERBOARD_ID_IOS;
                    leaderboardTime.LoadScores(result => {});
                    leaderboardHighscore = Social.CreateLeaderboard();
                    leaderboardHighscore.id = HIGHSCORE_LEADERBOARD_ID_IOS;
                    leaderboardHighscore.LoadScores(result => {});
                    leaderboardKillstreak = Social.CreateLeaderboard();
                    leaderboardKillstreak.id = KILLSTREAK_LEADERBOARD_ID_IOS;
                    leaderboardKillstreak.LoadScores(result => {});
                    leaderboardMilesRun = Social.CreateLeaderboard();
                    leaderboardMilesRun.id = MILESRUN_LEADERBOARD_ID_IOS;
                    leaderboardMilesRun.LoadScores(result => {});
                    leaderboardMilesDriven = Social.CreateLeaderboard();
                    leaderboardMilesDriven.id = MILESDRIVEN_LEADERBOARD_ID_IOS;
                    leaderboardMilesDriven.LoadScores(result =>
                    {
                        if (result)
                            ShowGameCenter();
                    });
                }
            });
        }
    }
    const float metersToMiles = 0.000621f;
    void ShowGameCenter()
    {
        int seconds = Mathf.RoundToInt (PlayerPrefs.GetFloat ("total_time_seconds", 0));
        int milesRun = Mathf.RoundToInt(metersToMiles * (PlayerPrefs.GetFloat ("distance_walked1", 0) + 
                                                         PlayerPrefs.GetFloat ("distance_walked2", 0) + PlayerPrefs.GetFloat ("distance_walked3", 0)));
        int milesDriven = Mathf.RoundToInt(metersToMiles * (PlayerPrefs.GetFloat ("distance_drived1", 0) + 
                                                            PlayerPrefs.GetFloat ("distance_drived2", 0) + PlayerPrefs.GetFloat ("distance_drived3", 0)));
        
        Social.ReportScore(PlayerPrefs.GetInt("total_kills", 0), KILLS_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(PlayerPrefs.GetInt("earned_coins", 0), COINS_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(PlayerPrefs.GetInt("pickup_guns", 0), COLLECTED_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(seconds, TIME_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(PlayerPrefs.GetInt("highscore", 0), HIGHSCORE_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(PlayerPrefs.GetInt("killstreak", 0), KILLSTREAK_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(milesRun, MILESRUN_LEADERBOARD_ID_IOS, success => { });
        Social.ReportScore(milesDriven, MILESDRIVEN_LEADERBOARD_ID_IOS, success =>
        {
            Social.ShowLeaderboardUI();
        });
    }
    
    public void Show()
    {
        #if UNITY_ANDROID
        if (Social.localUser.authenticated)
        {
           ReportScoreAndShow();
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                ReportScoreAndShow();
            });
        }
        #elif UNITY_IOS
        GameCenterButtonEvent();
        #endif
    
        /*#if UNITY_ANDROID
        int responce =  AN_GoogleApiAvailability.IsGooglePlayServicesAvailable();
        if(responce == AN_ConnectionResult.SUCCESS) {
            if (AN_GoogleSignIn.GetLastSignedInAccount() != null) //signed
            {
                ShowLeaderboardsUI();    
            }
            else
            {
                SignIn();	    
            }
        } else {
            Debug.Log("Google Api not avaliable on current device, trying to resolve");
            AN_GoogleApiAvailability.MakeGooglePlayServicesAvailable((result) => {
                if(result.IsSucceeded) {
                    if (AN_GoogleSignIn.GetLastSignedInAccount() != null) //signed
                    {
                        ShowLeaderboardsUI();  
                    }
                    else
                    {
                        SignIn();	
                    }
					    
                } else {
                    // Failed to resolve, all attempts to use GMS API on this device will fail
                }
            });
        }
        #elif UNITY_IOS
        ISN_GKLocalPlayer.Authenticate((SA_Result res) => {
            if (res.IsSucceeded) {
                ISN_GKScore scoreReporter1 = new ISN_GKScore(DISTANCE_LEADERBOARD_ID_IOS);
                scoreReporter1.Value = PlayerPrefs.GetInt("distance", 0);
                scoreReporter1.Context = 1;

                ISN_GKScore scoreReporter2 = new ISN_GKScore(COINS_LEADERBOARD_ID_IOS);
                scoreReporter2.Value = PlayerPrefs.GetInt("total_earned", 0);
                scoreReporter2.Context = 1;

                var scores = new List<ISN_GKScore>() {scoreReporter1, scoreReporter2};

                ISN_GKScore.ReportScores(scores, (result) => {
                    if (result.IsSucceeded) {
                        ISN_GKGameCenterViewController viewController = new ISN_GKGameCenterViewController();
                        viewController.ViewState = ISN_GKGameCenterViewControllerState.Leaderboards;
                        viewController.Show();
                        Debug.Log("Score Report Success");
                    } else {
                        Debug.Log("Score Report failed! Code: " + result.Error.Code + " Message: " + result.Error.Message);
                    }
                });
            }
            else {
                Debug.Log("Authenticate is failed! Error with code: " + res.Error.Code + " and description: " + res.Error.Message);
            }
        });
        #endif*/
    }
    
    void SignIn()
    {
        /*AN_GoogleSignInOptions.Builder builder = new AN_GoogleSignInOptions.Builder(AN_GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        builder.RequestId();
        builder.RequestEmail();
        builder.RequestProfile();

        AN_GoogleSignInOptions gso = builder.Build();
        AN_GoogleSignInClient client = AN_GoogleSignIn.GetClient(gso);
        AN_Logger.Log("SignInNoSilent Start ");

        client.SignIn((signInResult) => {
            AN_Logger.Log("Sign In StatusCode: " + signInResult.StatusCode);
            if (signInResult.IsSucceeded) {
                AN_Logger.Log("SignIn Succeeded");
                ShowLeaderboardsUI();
            } else {
                AN_Logger.Log("SignIn filed: " + signInResult.Error.FullMessage);
            }
        });*/
    }

    void ShowLeaderboardsUI()
    {
/*        var leaderboards = AN_Games.GetLeaderboardsClient();
        leaderboards.SubmitScore(DISTANCE_LEADERBOARD_ID, PlayerPrefs.GetInt("distance", 0));
        leaderboards.SubmitScore(COINS_LEADERBOARD_ID, PlayerPrefs.GetInt("total_earned", 0));
        leaderboards.GetAllLeaderboardsIntent((result) => {
            if (result.IsSucceeded) {
                var intent = result.Intent;
                AN_ProxyActivity proxy = new AN_ProxyActivity();
                proxy.StartActivityForResult(intent, (intentResult) => {
                    proxy.Finish();
                    //Note: you might want to check is user had sigend out with that UI
                });

            } else {
                Debug.Log("Failed to Get leaderboards Intent " + result.Error.FullMessage);
            }
        });*/
    }

}
