﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateMenu : BaseMenu {

	public string appURL = "https://itunes.apple.com/us/app/block-city-cop-vice-town/id1359340311";

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
	}

	public void RateButtonEvent() {
		Application.OpenURL (appURL);
	}
	
	public void CancelButtonEvent() {
		FindObjectOfType<MainMenu> ().character.SetActive (true);
		this.Hide ();
	}
}
