﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour {

	public Text nameLabel;
	public Text targetLabel;
	public Text rewardLabel;
	public Slider progressSlider;
	public Text progressLabel;
	public GameObject completedLabel;

	// Use this for initialization
	void Start () {
		
	}
	

}
