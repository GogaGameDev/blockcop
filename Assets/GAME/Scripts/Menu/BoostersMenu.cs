﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostersMenu : BaseMenu {

	public BoosterTab[] boosterTabs;
	public string[] playerPrefsNames;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
		PlayerPrefs.SetInt ("cur_mult", 1);
		PlayerPrefs.SetInt ("next_mult", 2);
		PlayerPrefs.SetInt ("next_mult_cost", 1);
	}
	
	public override void Show() {
		base.Show ();
		InitBoostersUI ();
	}

	void InitBoostersUI() {
		for (int i = 0; i < boosterTabs.Length; i++) {
			int value = PlayerPrefs.GetInt (playerPrefsNames [i], 0);
			if (i == 0) {
				int cost = PlayerPrefs.GetInt ("next_mult_cost", 1);
				int curMult = PlayerPrefs.GetInt ("cur_mult", 1);
				int nextMult = PlayerPrefs.GetInt ("next_mult", 2);
				boosterTabs [i].InitMultiplierBooster (value, cost, curMult, nextMult);  
			} else {
				boosterTabs [i].Init(value, 1); 
			}
		}
	}

	public void UseBooster(int id) {
		if (id == 0) {
			int cost = PlayerPrefs.GetInt ("next_mult_cost", 1);
			PlayerPrefs.SetInt (playerPrefsNames [id], PlayerPrefs.GetInt (playerPrefsNames [id], 0) - cost);
		} else {
			PlayerPrefs.SetInt (playerPrefsNames [id], PlayerPrefs.GetInt (playerPrefsNames [id], 0) - 1);
		}
		GameUI.Instance.UseBooster (id);
	}
}
