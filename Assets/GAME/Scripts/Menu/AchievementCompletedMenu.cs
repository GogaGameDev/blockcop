﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementCompletedMenu : BaseMenu {

	public Text nameText; 
	public Text rewardText; 
	public Image rewardIconImage;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
	}
	

}
