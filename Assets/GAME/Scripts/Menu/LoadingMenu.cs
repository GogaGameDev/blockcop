﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingMenu : BaseMenu {

	private int targetLevel = 1;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
	}
	
	protected override void ShowActions() {
		SceneManager.LoadScene (targetLevel);
	}

	public void SetLevel(int level) {
		targetLevel = level;
	}
}
