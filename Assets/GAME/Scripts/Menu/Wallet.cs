﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wallet : MonoBehaviour {

	public static Wallet Instance;
	private Text moneyLabel;
	private int money = 0;

	// Use this for initialization
	void Start () {
		Instance = this;
		//PlayerPrefs.SetInt("coins", 1111000);
		money = PlayerPrefs.GetInt ("coins", 0);
		//PlayerPrefs.DeleteAll ();

	}

	public void Init(Text coinsLabel) {
		moneyLabel = coinsLabel;
		money = PlayerPrefs.GetInt ("coins", 0);
		moneyLabel.text = money.ToString("N0");
	}
	
	public void Pay(int cost) {
		if (money - cost >= 0) {
			money -= cost;
			moneyLabel.text = money.ToString("N0");
			PlayerPrefs.SetInt ("coins", money);
		}
	}

	public bool HasValue(int value) {
		if (money - value >= 0)
			return true;
		else
			return false;
	}

	public int getBalance() {
		return money;
	}

	public void Receive(int value) {
		money += value;
		moneyLabel.text = money.ToString("N0");
		PlayerPrefs.SetInt ("coins", money);
	}

	public void UpdateValue() {
		money = PlayerPrefs.GetInt ("coins", 0);
		moneyLabel.text = money.ToString("N0");
	}
}
