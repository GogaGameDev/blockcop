﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopMenu : BaseMenu {

	public enum Submenu {
		Cops, Vehicles, Weapons
	}

	public BaseMenu mainMenu, notificationMenu, notificationVehiclesMenu;
	//public List<Skin> skins;
	public GameObject character;
	public int[] skinPrices;
	public int[] copMaxUpgradeLevels;
	public CopUpgradeLevel[] copUpgradeLevelSettings;
	public CopUpgradeUI copUpgradeUI;
	public VehiclesUpgradeUI vehicleUpgradeUI;
	public List<VehicleUpgradeList> vehicleUpgradeLevelSettings;
	public WeaponUpgradeUI weaponUpgradeUI;
	public List<WeaponUpgradeList> weaponUpgradeLevelSettings;
	public GameObject priceTab, purchaseButton, selectButton, upgradeButton, selectedImage;
	public Text currentLevelText, coinsLabel, notificationVehiclesText, chooseText;
	private Vector2 rotationDelta = Vector2.zero;
	private Quaternion defaultRotation = Quaternion.Euler (0, 180, 0);

	private int currentItemID = 0;

	private Submenu currentMenu = Submenu.Cops;


	private Wallet wallet;

	[System.Serializable]
	public class CopUpgradeUI
	{
		public GameObject root;
		public Image speedIndicator;
		public Image healthIndicator;
		public Image armorIndicator;
		public Image speedIndicatorBG;
		public Image healthIndicatorBG;
		public Image armorIndicatorBG;
	}

	[System.Serializable]
	public class VehiclesUpgradeUI
	{
		public GameObject root;
		public Image speedIndicator;
		public Image healthIndicator;
		public Image armorIndicator;
		public Image damageIndicator;
		public Image ammoIndicator;
		public GameObject[] vehicles;
		public int[] vehiclePrices;
	}

	[System.Serializable]
	public class WeaponUpgradeUI
	{
		public GameObject root;
		public Image damageIndicator;
		public Image ammoIndicator;
		public Image damageIndicatorBG;
		public Image ammoIndicatorBG;
		public GameObject[] weapons;
		public int[] weaponPrices;
	}

	[System.Serializable]
	public class VehicleUpgradeList
	{
		public string name;
		public List<VehicleUpgradeLevel> list;
	}

	[System.Serializable]
	public class WeaponUpgradeList
	{
		public string name;
		public List<WeaponUpgradeLevel> list;
	}

	[System.Serializable]
	public class CopUpgradeLevel
	{
		public int price;
		public int speed;
		public int health;
		public int armor;
	}

	[System.Serializable]
	public class VehicleUpgradeLevel
	{
		public int price;
		public int speed;
		public int health;
		public int armor;
		public int damage;
		public int ammo = -1;
	}

	[System.Serializable]
	public class WeaponUpgradeLevel
	{
		public int price;
		public int damage;
		public int ammo;
	}

//	[System.Serializable]
//	public class Skin
//	{
//		public SkinnedMeshRenderer meshBase;
//		public Material baseMaterial;
//		public SkinnedMeshRenderer mesh;
//		public Material material;
//	}

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
		SetDefaultUpgrades ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.D)) {
			Debug.Log ("Current skin: " + currentItemID + "");
			Debug.Log ("Current upgrade: " + getCurrentUpgradeLevel(currentItemID) + "");
		}
		if (Input.GetKeyDown (KeyCode.C)) {
			wallet.Receive (1000000);
		}
	}

	public override void Show() {
		base.Show ();
		currentMenu = Submenu.Cops;
		wallet = Wallet.Instance;
		wallet.Init (coinsLabel);
		InitShopUI ();
	}

	void InitShopUI() {
		currentItemID = getCurrentItemID();
		SetItemEnabled (true);
		SetUpgradeUIEnabled (true);
		if (isItemUnlocked (currentItemID)) {
			UpdateShopUI (true);
		} else {
			UpdateShopUI (false);
		}

	}


	public override void Hide() {
		base.Hide ();
		SetItemEnabled (false);
		SetUpgradeUIEnabled (false);
	}

	bool isItemUnlocked(int itemID) {
		if (currentMenu == Submenu.Cops) {
			return skinPrices [itemID] == 0 || PlayerPrefs.GetInt ("skin" + itemID + "", 0) == 1;
		} else if (currentMenu == Submenu.Vehicles) {
			return vehicleUpgradeUI.vehiclePrices [itemID] == 0 || PlayerPrefs.GetInt ("vehicle" + itemID + "", 0) == 1;
		} else if (currentMenu == Submenu.Weapons) {
			return weaponUpgradeUI.weaponPrices [itemID] == 0 || PlayerPrefs.GetInt ("weapon" + itemID + "", 0) == 1;
		}
		return false;
	}

	int getCurrentUpgradeLevel(int itemID) {
		if (currentMenu == Submenu.Cops) {
			return PlayerPrefs.GetInt ("skin" + itemID + "upgrade", 0);
		} else if (currentMenu == Submenu.Vehicles) {
			return PlayerPrefs.GetInt ("vehicle" + itemID + "upgrade", 0);
		} else if (currentMenu == Submenu.Weapons) {
			return PlayerPrefs.GetInt ("weapon" + itemID + "upgrade", 0);
		}
		return 0;
	}

	void UpdateShopUI(bool itemPurchased) {
		priceTab.SetActive (!itemPurchased);
		if (currentMenu == Submenu.Cops) {
			chooseText.text = "CHOOSE COP";
			if (!itemPurchased) {
				priceTab.GetComponentInChildren<Text> ().text = skinPrices [currentItemID].ToString ("N0");
				upgradeButton.SetActive (false);
				selectedImage.SetActive (false);
			} else {
				int currentUpgrade = getCurrentUpgradeLevel (currentItemID);
				if (currentUpgrade < copMaxUpgradeLevels [currentItemID]) { //can upgrade more
					priceTab.SetActive (true);
					priceTab.GetComponentInChildren<Text> ().text = copUpgradeLevelSettings [currentUpgrade + 1].price.ToString ("N0");
					upgradeButton.SetActive (true);
				} else {
					priceTab.SetActive (false);
					upgradeButton.SetActive (false);
				}
				CheckItemSelected (currentItemID);
			}
		} else if (currentMenu == Submenu.Vehicles) {
			chooseText.text = "CHOOSE VEHICLE";
			if (!itemPurchased) {
				priceTab.GetComponentInChildren<Text> ().text = vehicleUpgradeUI.vehiclePrices [currentItemID].ToString ("N0");
				upgradeButton.SetActive (false);
				selectedImage.SetActive (false);
			} else {
				int currentUpgrade = getCurrentUpgradeLevel (currentItemID);
				if (currentUpgrade < vehicleUpgradeLevelSettings [currentItemID].list.Count - 1) { //can upgrade more
					priceTab.SetActive (true);
					priceTab.GetComponentInChildren<Text> ().text = vehicleUpgradeLevelSettings [currentItemID].list [currentUpgrade + 1].price.ToString ("N0");
					upgradeButton.SetActive (true);
				} else {
					priceTab.SetActive (false);
					upgradeButton.SetActive (false);
				}
				CheckItemSelected (currentItemID);
			}
		} else if (currentMenu == Submenu.Weapons) {
			chooseText.text = "CHOOSE WEAPON";
			if (!itemPurchased) {
				priceTab.GetComponentInChildren<Text> ().text = weaponUpgradeUI.weaponPrices [currentItemID].ToString ("N0");
				upgradeButton.SetActive (false);
				selectedImage.SetActive (false);
			} else {
				int currentUpgrade = getCurrentUpgradeLevel (currentItemID);
				if (currentUpgrade < weaponUpgradeLevelSettings [currentItemID].list.Count - 1) { //can upgrade more
					priceTab.SetActive (true);
					priceTab.GetComponentInChildren<Text> ().text = weaponUpgradeLevelSettings [currentItemID].list [currentUpgrade + 1].price.ToString ("N0");
					upgradeButton.SetActive (true);
				} else {
					priceTab.SetActive (false);
					upgradeButton.SetActive (false);
				}
				CheckItemSelected (currentItemID);
			}
		}
		purchaseButton.SetActive (!itemPurchased);
		UpdateUpgradeLevelUI ();
	}

	public void OnItemDrag(BaseEventData baseData) {
		if (currentMenu == Submenu.Cops) {
			rotationDelta = ((PointerEventData)baseData).delta;
			float curY = character.transform.eulerAngles.y;
			SetItemRotation (Quaternion.Euler (0, curY - rotationDelta.x / 2f, 0));
		} else if (currentMenu == Submenu.Vehicles) {
			rotationDelta = ((PointerEventData)baseData).delta;
			float curY =  vehicleUpgradeUI.vehicles[currentItemID].transform.eulerAngles.y;
			SetItemRotation (Quaternion.Euler (0, curY - rotationDelta.x / 2f, 0));
		} else if (currentMenu == Submenu.Weapons) {
			rotationDelta = ((PointerEventData)baseData).delta;
			float curY =  weaponUpgradeUI.weapons[currentItemID].transform.eulerAngles.y;
			SetItemRotation (Quaternion.Euler (0, curY - rotationDelta.x / 2f, 0));
		}
	}

	public void OnItemSelect() {
		if (isItemUnlocked (currentItemID)) {
			SaveCurrentItemID (currentItemID);
			selectedImage.SetActive (true);
		}
	}

	void CheckItemSelected(int itemID) {
		if (getCurrentItemID() == itemID) {
			selectedImage.SetActive (true);
		} else {
			selectedImage.SetActive (false);
		}
	}

	int getCurrentItemID() {
		if (currentMenu == Submenu.Cops) {
			return PlayerPrefs.GetInt ("skin", 0);
		} else if (currentMenu == Submenu.Vehicles) {
			return PlayerPrefs.GetInt ("vehicle", 0);
		} else if (currentMenu == Submenu.Weapons) {
			return PlayerPrefs.GetInt ("weapon", 0);
		} 
		return 0;
	}

	void SaveCurrentItemID(int id) {
		if (currentMenu == Submenu.Cops) {
			PlayerPrefs.SetInt ("skin", id);
		} else if (currentMenu == Submenu.Vehicles) {
			PlayerPrefs.SetInt ("vehicle", id);
		} else if (currentMenu == Submenu.Weapons) {
			PlayerPrefs.SetInt ("weapon", id);
		} 
	}

	public void NextSkinButtonEvent() {
		Quaternion curRot = GetItemRotation();
		//SetSkinEnabled (false);
		currentItemID++;
		if (currentItemID >= getItemsCount())
			currentItemID = 0;
		//SetSkinEnabled (true);
		SetItemRotation (curRot);
		if (isItemUnlocked(currentItemID)) {
			UpdateShopUI (true);
		} else {
			UpdateShopUI (false);
		}
		ChangeItemLogo ();
		//MenuSoundManager.Instance.PlaySound (MenuSoundManager.Instance.clickSound);
	}

	public void PrevSkinButtonEvent() {
		Quaternion curRot = GetItemRotation ();
		//SetSkinEnabled (false);
		currentItemID--;
		if (currentItemID < 0)
			currentItemID = getItemsCount()-1;
		//SetSkinEnabled (true);
		SetItemRotation (curRot);
		if (isItemUnlocked(currentItemID)) {
			UpdateShopUI (true);
		} else {
			UpdateShopUI (false);
		}
		ChangeItemLogo ();
	}

	int getItemsCount() {
		if (currentMenu == Submenu.Cops) {
			return skinPrices.Length;
		} else if (currentMenu == Submenu.Vehicles) {
			return vehicleUpgradeUI.vehicles.Length;
		} else  {
			return weaponUpgradeUI.weapons.Length;
		}
	}

	void ChangeItemLogo() {
		if (currentMenu == Submenu.Cops) {
			character.GetComponentInChildren<SetPlayerSkin> ().SetSkin (currentItemID);
		} else if (currentMenu == Submenu.Vehicles) {
			foreach (GameObject vehicle in vehicleUpgradeUI.vehicles) {
				vehicle.SetActive (false);
			}
			vehicleUpgradeUI.vehicles [currentItemID].SetActive (true);
		} else if (currentMenu == Submenu.Weapons) {
			foreach (GameObject weapon in weaponUpgradeUI.weapons) {
				weapon.SetActive (false);
			}
			weaponUpgradeUI.weapons [currentItemID].SetActive (true);
		}
	}

	public void BackButtonEvent() {
		SetItemRotation(defaultRotation);
		SetItemEnabled (false);
		this.Hide ();
		mainMenu.Show ();
		//MenuSoundManager.Instance.PlaySound (MenuSoundManager.Instance.clickSound);
	}

	public void SelectSkinButonEvent() {
		SetItemRotation(defaultRotation);
		SetItemEnabled (false);
		//PlayerPrefs.SetInt ("skin", currentSkinID);
		this.Hide ();
		//levelsMenu.Show ();
		//MenuSoundManager.Instance.PlaySound (MenuSoundManager.Instance.clickSound);
	}


	public void PurchaseSkinButonEvent() {
		if (currentMenu == Submenu.Cops) {
			if (wallet.HasValue (skinPrices [currentItemID])) {
				wallet.Pay (skinPrices [currentItemID]);
				PlayerPrefs.SetInt ("skin" + currentItemID + "", 1);
				UpdateShopUI (true);
			} else {
				SetNotificationVisible (true);
			}
		} else if (currentMenu == Submenu.Vehicles) {
			if (wallet.HasValue (vehicleUpgradeUI.vehiclePrices[currentItemID])) { 
				if (canPurchaseVehicle (currentItemID)) { 
					wallet.Pay (vehicleUpgradeUI.vehiclePrices[currentItemID]);
					PlayerPrefs.SetInt ("vehicle" + currentItemID + "", 1);
					UpdateShopUI (true);
				} else {
					notificationVehiclesText.text = getCantBuyVehicleMessage (currentItemID);
					SetVehicleNotificationVisible (true);
				}
			} else {
				SetNotificationVisible (true);
			}
		} else if (currentMenu == Submenu.Weapons) {
			if (wallet.HasValue (weaponUpgradeUI.weaponPrices[currentItemID])) { 
				wallet.Pay (weaponUpgradeUI.weaponPrices[currentItemID]);
				PlayerPrefs.SetInt ("weapon" + currentItemID + "", 1);
				UpdateShopUI (true);
			} else {
				SetNotificationVisible (true);
			}
		}
	}

	public void UpgradeSkinButonEvent() {
		int currentUpgradeID = getCurrentUpgradeLevel (currentItemID);
		if (currentMenu == Submenu.Cops) {
			int price = copUpgradeLevelSettings [currentUpgradeID + 1].price;
			if (wallet.HasValue (price)) {
				wallet.Pay (price);
				PlayerPrefs.SetInt ("skin" + currentItemID + "upgrade", currentUpgradeID + 1);
				PlayerPrefs.SetInt ("skin" + currentItemID + "health", copUpgradeLevelSettings [currentUpgradeID + 1].health);
				PlayerPrefs.SetInt ("skin" + currentItemID + "armor", copUpgradeLevelSettings [currentUpgradeID + 1].armor);
				PlayerPrefs.SetInt ("skin" + currentItemID + "speed", copUpgradeLevelSettings [currentUpgradeID + 1].speed);
				UpdateShopUI (true);
			} else {
				SetNotificationVisible (true);
			}
		} else if (currentMenu == Submenu.Vehicles) {
			int price = vehicleUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].price;
			if (wallet.HasValue (price)) {
				wallet.Pay (price);
				PlayerPrefs.SetInt ("vehicle" + currentItemID + "upgrade", currentUpgradeID + 1);
				PlayerPrefs.SetInt ("vehicle" + currentItemID + "armor", vehicleUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].armor);
				PlayerPrefs.SetInt ("vehicle" + currentItemID + "damage", vehicleUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].damage);
				PlayerPrefs.SetInt ("vehicle" + currentItemID + "health", vehicleUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].health);
				PlayerPrefs.SetInt ("vehicle" + currentItemID + "speed", vehicleUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].speed);
				PlayerPrefs.SetInt ("vehicle" + currentItemID + "ammo", vehicleUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].ammo);
				UpdateShopUI (true);
			} else {
				SetNotificationVisible (true);
			}
		} else if (currentMenu == Submenu.Weapons) {
			int price = weaponUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].price;
			if (wallet.HasValue (price)) {
				wallet.Pay (price);
				PlayerPrefs.SetInt ("weapon" + currentItemID + "upgrade", currentUpgradeID + 1);
				PlayerPrefs.SetInt ("weapon" + currentItemID + "ammo", weaponUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].ammo);
				PlayerPrefs.SetInt ("weapon" + currentItemID + "damage", weaponUpgradeLevelSettings[currentItemID].list[currentUpgradeID + 1].damage);
				UpdateShopUI (true);
			} else {
				SetNotificationVisible (true);
			}
		}
	}

	public void SetNotificationVisible(bool visible) {
		menuCG.blocksRaycasts = !visible;
		SetItemEnabled (!visible);
		if (visible) {
			notificationMenu.Show ();
			//MenuSoundManager.Instance.PlaySound (MenuSoundManager.Instance.lockedSound);
		} else {
			notificationMenu.Hide ();
			//MenuSoundManager.Instance.PlaySound (MenuSoundManager.Instance.clickSound);
		}
	}

	public void SetVehicleNotificationVisible(bool visible) {
		menuCG.blocksRaycasts = !visible;
		SetItemEnabled (!visible);
		if (visible) {
			notificationVehiclesMenu.Show ();
		} else {
			notificationVehiclesMenu.Hide ();
		}
	}

	void SetItemEnabled(bool enabled) {
		if (currentMenu == Submenu.Cops) {
			character.SetActive (enabled);
		} else if (currentMenu == Submenu.Vehicles) {
			vehicleUpgradeUI.vehicles [currentItemID].SetActive (enabled);
		} else if (currentMenu == Submenu.Weapons) {
			weaponUpgradeUI.weapons [currentItemID].SetActive (enabled);
		}
	}

	void SetUpgradeUIEnabled(bool enabled) {
		if (currentMenu == Submenu.Cops) {
			copUpgradeUI.root.SetActive (enabled);
			if (enabled) {
				character.GetComponentInChildren<SetPlayerSkin> ().SetSkin (currentItemID);
			}
		} else if (currentMenu == Submenu.Vehicles) {
			vehicleUpgradeUI.root.SetActive (enabled);
		} else if (currentMenu == Submenu.Weapons) {
			weaponUpgradeUI.root.SetActive (enabled);
		}
	}

	void SetItemRotation(Quaternion rot) {
		if (currentMenu == Submenu.Cops) {
			character.transform.rotation = rot;
		} else if (currentMenu == Submenu.Vehicles) {
			vehicleUpgradeUI.vehicles [currentItemID].transform.rotation = rot;
		} else if (currentMenu == Submenu.Weapons) {
			weaponUpgradeUI.weapons [currentItemID].transform.rotation = rot;
		}
	}

	Quaternion GetItemRotation() {
		if (currentMenu == Submenu.Cops) {
			return character.transform.rotation;
		} else if (currentMenu == Submenu.Vehicles) {
			return vehicleUpgradeUI.vehicles [currentItemID].transform.rotation;
		} else {
			return weaponUpgradeUI.weapons [currentItemID].transform.rotation;
		}
	}

	void UpdateUpgradeLevelUI() {
		int curLevel = (getCurrentUpgradeLevel (currentItemID) + 1);
		if (currentMenu == Submenu.Cops) {
			currentLevelText.text = "LVL " + curLevel.ToString () + "/" + (copMaxUpgradeLevels [currentItemID] + 1).ToString ();
			copUpgradeUI.armorIndicator.fillAmount = (float)copUpgradeLevelSettings [curLevel - 1].armor / 10f;
			copUpgradeUI.healthIndicator.fillAmount = (float)copUpgradeLevelSettings [curLevel - 1].health / 10f;
			copUpgradeUI.speedIndicator.fillAmount = (float)copUpgradeLevelSettings [curLevel - 1].speed / 10f;
			float fillValue = getMaxBarFillAmount ();
			copUpgradeUI.armorIndicatorBG.fillAmount = fillValue;
			copUpgradeUI.healthIndicatorBG.fillAmount = fillValue;
			copUpgradeUI.speedIndicatorBG.fillAmount = fillValue;
		} else if (currentMenu == Submenu.Vehicles) {
			currentLevelText.text = "LVL " + curLevel.ToString () + "/" + (vehicleUpgradeLevelSettings[currentItemID].list.Count).ToString ();
			vehicleUpgradeUI.armorIndicator.fillAmount = (float)vehicleUpgradeLevelSettings[currentItemID].list [curLevel - 1].armor / 10f;
			vehicleUpgradeUI.damageIndicator.fillAmount = (float)vehicleUpgradeLevelSettings[currentItemID].list [curLevel - 1].damage / 10f;
			vehicleUpgradeUI.healthIndicator.fillAmount = (float)vehicleUpgradeLevelSettings[currentItemID].list [curLevel - 1].health / 10f;
			vehicleUpgradeUI.speedIndicator.fillAmount = (float)vehicleUpgradeLevelSettings[currentItemID].list [curLevel - 1].speed / 10f;
			vehicleUpgradeUI.ammoIndicator.fillAmount = (float)vehicleUpgradeLevelSettings[currentItemID].list [curLevel - 1].ammo / 10f;
		} else if (currentMenu == Submenu.Weapons) {
			currentLevelText.text = "LVL " + curLevel.ToString () + "/" + (weaponUpgradeLevelSettings[currentItemID].list.Count).ToString ();
			weaponUpgradeUI.ammoIndicator.fillAmount = (float)weaponUpgradeLevelSettings[currentItemID].list [curLevel - 1].ammo / 10f;
			weaponUpgradeUI.damageIndicator.fillAmount = (float)weaponUpgradeLevelSettings[currentItemID].list [curLevel - 1].damage / 10f;
//			float fillValue = getMaxBarFillAmount ();
//			weaponUpgradeUI.ammoIndicatorBG.fillAmount = fillValue;
//			weaponUpgradeUI.damageIndicatorBG.fillAmount = fillValue;
		} 
	}

	float getMaxBarFillAmount() {
		if (currentMenu == Submenu.Cops) {
			int currentValue = copUpgradeLevelSettings [copMaxUpgradeLevels [currentItemID]].armor;
			if (copUpgradeLevelSettings [copMaxUpgradeLevels [currentItemID]].health > currentValue)
				currentValue = copUpgradeLevelSettings [copMaxUpgradeLevels [currentItemID]].health;
			if (copUpgradeLevelSettings [copMaxUpgradeLevels [currentItemID]].speed > currentValue)
				currentValue = copUpgradeLevelSettings [copMaxUpgradeLevels [currentItemID]].speed;
			return (float)currentValue / 10f;
		} else if (currentMenu == Submenu.Vehicles) {
			return 1f;
		} else {
			int maxUpgrade = weaponUpgradeLevelSettings [currentItemID].list.Count;
			int currentValue = weaponUpgradeLevelSettings [currentItemID].list [maxUpgrade - 1].ammo;
			if (weaponUpgradeLevelSettings [currentItemID].list [maxUpgrade - 1].damage > currentValue)
				currentValue = weaponUpgradeLevelSettings [currentItemID].list [maxUpgrade - 1].damage;
			return (float)currentValue / 10f;
		}
	}

	public void ChangeSubmenu(int id) {
		Submenu newMenu = (Submenu)id;
		if (currentMenu != newMenu) {
			selectedImage.SetActive (false);
			SetItemEnabled (false);
			SetUpgradeUIEnabled (false);
			currentMenu = newMenu;
			InitShopUI ();
		}
	}

	void SetDefaultUpgrades() {
		for (int i = 0; i < copMaxUpgradeLevels.Length; i++) {
			if (PlayerPrefs.GetInt ("skin" + i + "upgrade", 0) == 0) {
				PlayerPrefs.SetInt ("skin" + i + "health", copUpgradeLevelSettings [0].health);
				PlayerPrefs.SetInt ("skin" + i + "armor", copUpgradeLevelSettings [0].armor);
				PlayerPrefs.SetInt ("skin" + i + "speed", copUpgradeLevelSettings [0].speed);
			}
		}
		for (int i = 0; i < vehicleUpgradeUI.vehicles.Length; i++) {
			if (PlayerPrefs.GetInt ("vehicle" + i + "upgrade", 0) == 0) {
				PlayerPrefs.SetInt ("vehicle" + i + "armor", vehicleUpgradeLevelSettings[i].list[0].armor);
				PlayerPrefs.SetInt ("vehicle" + i + "damage", vehicleUpgradeLevelSettings[i].list[0].damage);
				PlayerPrefs.SetInt ("vehicle" + i + "health", vehicleUpgradeLevelSettings[i].list[0].health);
				PlayerPrefs.SetInt ("vehicle" + i + "speed", vehicleUpgradeLevelSettings[i].list[0].speed);
				PlayerPrefs.SetInt ("vehicle" + i + "ammo", vehicleUpgradeLevelSettings[i].list[0].ammo);
			}
		}
		for (int i = 0; i < weaponUpgradeUI.weapons.Length; i++) {
			if (PlayerPrefs.GetInt ("weapon" + i + "upgrade", 0) == 0) {
				PlayerPrefs.SetInt ("weapon" + i + "ammo", weaponUpgradeLevelSettings[i].list[0].ammo);
				PlayerPrefs.SetInt ("weapon" + i + "damage", weaponUpgradeLevelSettings[i].list[0].damage);
			}
		}
	}

	bool canPurchaseVehicle(int vehicleID) {
		switch (vehicleID) {
		case 1:
			for (int i = 0; i < copMaxUpgradeLevels.Length; i++) {
				if (PlayerPrefs.GetInt ("skin" + i + "upgrade", 0) >= 3) {
					return true;
				}
			}
			break;
		case 2:
			for (int i = 0; i < copMaxUpgradeLevels.Length; i++) {
				if (PlayerPrefs.GetInt ("skin" + i + "upgrade", 0) >= 4) {
					return true;
				}
			}
			break;
		case 3:
			bool copUpgraded = false;
			for (int i = 0; i < copMaxUpgradeLevels.Length; i++) {
				if (PlayerPrefs.GetInt ("skin" + i + "upgrade", 0) >= 5) {
					copUpgraded = true;
					break;
				}
			}
			if (copUpgraded && PlayerPrefs.GetInt ("weapon3upgrade", 0) >= 2)
				return true;
			break;
		case 4:
			copUpgraded = false;
			for (int i = 12; i < copMaxUpgradeLevels.Length; i++) {
				if (PlayerPrefs.GetInt ("skin" + i + "upgrade", 0) >= 2) {
					copUpgraded = true;
					break;
				}
			}
			Debug.Log (copUpgraded + " " + PlayerPrefs.GetInt ("weapon4upgrade", 0));
			if (copUpgraded && PlayerPrefs.GetInt ("weapon5upgrade", 0) >= 2)
				return true;
			break;
		}
		return false;
	}

	string getCantBuyVehicleMessage(int vehicleID) {
		switch (vehicleID) {
		case 1:
			return "Must have at least one cop upgraded to level 4 before unlocking this vehicle.";
			break;
		case 2:
			return "Must have at least one cop upgraded to level 5 before unlocking this vehicle.";
			break;
		case 3:
			return "Must have at least one cop upgraded to level 6 and the Assault Rifle upgraded to level 3 before unlocking this vehicle.";
			break;
		case 4:
			return "Must have a level 3 male or female soldier and the Grenade Launcher upgraded to level 3 before unlocking this vehicle.";
			break;
		}
		return "";
	}
}
