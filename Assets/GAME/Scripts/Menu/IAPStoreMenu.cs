using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GoogleMobileAds.Api;
using System;
using OTO;

public class IAPStoreMenu : BaseMenu {

	public BaseMenu mainMenu, mysteryBoxMenu, prizeWonMenu, notificationPanel;
	public Text coinsLabel;
	public List<Product> products;
	public EventSystem eventSystem;
	
	public GameObject noAdsButton, doubleCoinsButton, noAdsOKImage, doubleCoinsOkImage, 
					  starterPackButton, starterPackOkImage, valuePackButton, valuePackOkImage;
	public Button doublePrizeButton;
	public GameObject restorePurchasesButton;
	public int misteryBoxPrice = 1000;
	private int referenceDPI = 100;
	private float referencePixelDrag = 8f;
	private Wallet wallet;
	private bool isRewardedVideoLoading = false;
	private bool isRewardedVideoCancelled = false;
	private bool isRewardedVideoShown = false;
	private RewardedAd admobRewardedAd;
	private string rewardedID;

	[System.Serializable]
	public class Product
	{
		public int value;
		public float price;
		public IAPItem item;
	}

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
		wallet = GetComponent<Wallet> ();
		eventSystem.pixelDragThreshold = Mathf.RoundToInt(Screen.dpi/ referenceDPI*referencePixelDrag);
#if UNITY_ANDROID
		restorePurchasesButton.SetActive(false);
#endif
		if (!Advertisement.isInitialized)
		{
			Advertisement.Initialize("4068586");
		}
		rewardedID = "ca-app-pub-8596057543629926/7324505769";
		 admobRewardedAd = new RewardedAd(rewardedID);
			AdRequest requestVideo = new AdRequest.Builder().Build();
			admobRewardedAd.LoadAd(requestVideo);

			// Called when an ad request has successfully loaded.
			admobRewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
			// Called when an ad request failed to load.
			admobRewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
			// Called when an ad is shown.
			admobRewardedAd.OnAdOpening += HandleRewardedAdOpening;
			// Called when an ad request failed to show.
			admobRewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
			// Called when the user should be rewarded for interacting with the ad.
			admobRewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
			// Called when the ad is closed.
			admobRewardedAd.OnAdClosed += HandleRewardedAdClosed;
		
	}

	public override void Show() {
		base.Show ();
		int coins = PlayerPrefs.GetInt ("coins", 0);
		coinsLabel.text = coins.ToString("N0");
		wallet.Init (coinsLabel);
		//InitProductsUI ();
		if (PlayerPrefs.GetInt("ads", 1) == 0) 
			SetItemPurchased(noAdsButton, noAdsOKImage);
		if (PlayerPrefs.GetInt("double_coins", 0) == 1) 
			SetItemPurchased(doubleCoinsButton, doubleCoinsOkImage);
		if (PlayerPrefs.GetInt("starter_pack", 0) == 1) 
			SetItemPurchased(starterPackButton, starterPackOkImage);
		if (PlayerPrefs.GetInt("value_pack", 0) == 1) 
			SetItemPurchased(valuePackButton, valuePackOkImage);
		isRewardedVideoLoading = false;
		isRewardedVideoCancelled = false;
		isRewardedVideoShown = false;
	}

	void InitProductsUI() {
		for (int i = 0; i < products.Count; i++) {
			products [i].item.priceLabel.text = products [i].price.ToString("C");
			products [i].item.valueLabel.text = products [i].value.ToString("N0");
		}
	}

	/*public void PurchaseProduct(int productID) {
		#if UNITY_ANDROID
		switch (productID) {
		case 0: //no ads
			GameBillingManagerExample.purchase (GameBillingManagerExample.REMOVE_ADS);
			break;
		case 1: //double coins
			GameBillingManagerExample.purchase (GameBillingManagerExample.DOUBLE_COINS);
			break;
		case 2: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.STARTER_PACK);
			break;
		case 3: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.VALUE_PACK);
			break;
		case 4: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.AID15_PACK);
			break;
		case 5: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.AID100_PACK);
			break;
		case 6: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.GEAR15_PACK);
			break;
		case 7: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.GEAR100_PACK);
			break;
		case 8: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.HALO10_PACK);
			break;
		case 9: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.HALO75_PACK);
			break;
		case 10: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.X15_PACK);
			break;
		case 11: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.X100_PACK);
			break;
		case 12: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.MAGNET15_PACK);
			break;
		case 13: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.MAGNET100_PACK);
			break;
		case 14: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.SHIELD10_PACK);
			break;
		case 15: 
			GameBillingManagerExample.purchase (GameBillingManagerExample.SHIELD75_PACK);
			break;
		#elif UNITY_IOS
		switch (productID) {
		case 0: //no ads
			PaymentManagerExample.buyItem (PaymentManagerExample.REMOVE_ADS);
			break;
		case 1: //double coins
			PaymentManagerExample.buyItem (PaymentManagerExample.DOUBLE_COINS);
			break;
		case 2: 
			PaymentManagerExample.buyItem (PaymentManagerExample.STARTER_PACK);
			break;
		case 3: 
			PaymentManagerExample.buyItem (PaymentManagerExample.VALUE_PACK);
			break;
		case 4: 
			PaymentManagerExample.buyItem (PaymentManagerExample.AID15_PACK);
			break;
		case 5: 
			PaymentManagerExample.buyItem (PaymentManagerExample.AID100_PACK);
			break;
		case 6: 
			PaymentManagerExample.buyItem (PaymentManagerExample.GEAR15_PACK);
			break;
		case 7: 
			PaymentManagerExample.buyItem (PaymentManagerExample.GEAR100_PACK);
			break;
		case 8: 
			PaymentManagerExample.buyItem (PaymentManagerExample.HALO10_PACK);
			break;
		case 9: 
			PaymentManagerExample.buyItem (PaymentManagerExample.HALO75_PACK);
			break;
		case 10: 
			PaymentManagerExample.buyItem (PaymentManagerExample.X15_PACK);
			break;
		case 11: 
			PaymentManagerExample.buyItem (PaymentManagerExample.X100_PACK);
			break;
		case 12: 
			PaymentManagerExample.buyItem (PaymentManagerExample.MAGNET15_PACK);
			break;
		case 13: 
			PaymentManagerExample.buyItem (PaymentManagerExample.MAGNET100_PACK);
			break;
		case 14: 
			PaymentManagerExample.buyItem (PaymentManagerExample.SHIELD10_PACK);
			break;
		case 15: 
			PaymentManagerExample.buyItem (PaymentManagerExample.SHIELD75_PACK);
			break;
		#endif
		}
	}*/

	public void OnProductPurchased(int value) {
		int coins = PlayerPrefs.GetInt ("coins", 0);
		coins += value;
		PlayerPrefs.SetInt ("coins", coins);
		coinsLabel.text = coins.ToString("N0");
		if (value > 100000) {
			PlayerPrefs.SetInt("ads", 0);
		}
	}

	public void RemoveAds() {
		PlayerPrefs.SetInt("ads", 0);
		SetItemPurchased(noAdsButton, noAdsOKImage);
	}

	public void DoubleCoins() {
		PlayerPrefs.SetInt("double_coins", 1);
		SetItemPurchased(doubleCoinsButton, doubleCoinsOkImage);
	}

	public void AddStarterPack() {
		PlayerPrefs.SetInt("aidkit", PlayerPrefs.GetInt("aidkit", 0) + 3);
		PlayerPrefs.SetInt("gear", PlayerPrefs.GetInt("gear", 0) + 3);
		PlayerPrefs.SetInt("halo", PlayerPrefs.GetInt("halo", 0) + 1);
		PlayerPrefs.SetInt("multiplier", PlayerPrefs.GetInt("multiplier", 0) + 2);
		PlayerPrefs.SetInt("shield", PlayerPrefs.GetInt("shield", 0) + 2);
		PlayerPrefs.SetInt("magnet", PlayerPrefs.GetInt("magnet", 0) + 3);
		PlayerPrefs.SetInt ("starter_pack", 1);
		SetItemPurchased(starterPackButton, starterPackOkImage);
	}

	public void AddValuePack() {
		PlayerPrefs.SetInt("aidkit", PlayerPrefs.GetInt("aidkit", 0) + 17);
		PlayerPrefs.SetInt("gear", PlayerPrefs.GetInt("gear", 0) + 17);
		PlayerPrefs.SetInt("halo", PlayerPrefs.GetInt("halo", 0) + 6);
		PlayerPrefs.SetInt("multiplier", PlayerPrefs.GetInt("multiplier", 0) + 12);
		PlayerPrefs.SetInt("shield", PlayerPrefs.GetInt("shield", 0) + 12);
		PlayerPrefs.SetInt("magnet", PlayerPrefs.GetInt("magnet", 0) + 17);
		PlayerPrefs.SetInt ("value_pack", 1);
		SetItemPurchased(valuePackButton, valuePackOkImage);
	}

	public void AddAIDKit(int value) {
		PlayerPrefs.SetInt("aidkit", PlayerPrefs.GetInt("aidkit", 0) + value);
	}

	public void AddGear(int value) {
		PlayerPrefs.SetInt("gear", PlayerPrefs.GetInt("gear", 0) + value);
	}

	public void AddHalo(int value) {
		PlayerPrefs.SetInt("halo", PlayerPrefs.GetInt("halo", 0) + value);
	}

	public void AddX(int value) {
		PlayerPrefs.SetInt("multiplier", PlayerPrefs.GetInt("multiplier", 0) + value);
	}

	public void AddMagnet(int value) {
		PlayerPrefs.SetInt("magnet", PlayerPrefs.GetInt("magnet", 0) + value);
	}

	public void AddShield(int value) {
		PlayerPrefs.SetInt("shield", PlayerPrefs.GetInt("shield", 0) + value);
	}

	public void BackButtonEvent() {
		this.Hide ();
		mainMenu.Show ();
	}
	

	public void RestoreIOSPurchases() {
//		PaymentManager.Instance.RestorePurchases ();
	}

	void SetItemPurchased(GameObject button, GameObject okImage) {
		button.SetActive(false);
		okImage.SetActive(true);
	}

	public void SetNotificationVisible(bool visible) {
		if (visible) {
			notificationPanel.Show ();
		} else {
			notificationPanel.Hide ();
		}
	}

	public void PurchaseMysteryBox() {
		if (wallet.HasValue (misteryBoxPrice)) {
			wallet.Pay (misteryBoxPrice);
			mysteryBoxMenu.Show ();
		} else {
			notificationPanel.Show ();
		}
	}

	public void OpenMisteryBox() {
		mysteryBoxMenu.Hide ();
		((WonPrizeMenu) prizeWonMenu).ShowInStore (wallet);
	}

	public void ClosePrizeMenu() {
		doublePrizeButton.interactable = true;
		prizeWonMenu.Hide ();
		isRewardedVideoLoading = false;
		isRewardedVideoCancelled = false;
		isRewardedVideoShown = false;
	}

	public void DoublePrizeButtonEvent() {
		if (!isRewardedVideoLoading) {
			StartCoroutine(ShowAdWhenReady());
			doublePrizeButton.interactable = false;
			isRewardedVideoLoading = true;
		}
	}

	void OnEnable() {
		
	}

	void OnDestroy() {
	}

	private void OnRewardedVideoFailedToLoad(string s) {
		if (isRewardedVideoCancelled)
			return;
		doublePrizeButton.interactable = false;
		//IOSDialog.Create ("Can't load video", err.ToString ());
	}

	private void OnRewarded() {
		prizeWonMenu.GetComponent<WonPrizeMenu> ().DoublePrize ();
	}

	IEnumerator ShowAdWhenReady()
	{
		if (app_ads_flag.is_unity_enabled)
		{
			Debug.Log("I CALLED!! UnityAd");
			while (!Advertisement.IsReady("rewardedVideo"))
				yield return null;
			isRewardedVideoShown = true;
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhandler;

			Advertisement.Show("rewardedVideo", options);
		}

		if (app_ads_flag.is_admob_enabled)
		{
			Debug.Log("I CALLED!! Admob");
			while (!admobRewardedAd.IsLoaded())
				yield return null;
			isRewardedVideoShown = true;
			admobRewardedAd.Show();
		}
	}
	
	void AdCallbackhandler(ShowResult result)
	{
		if (result == ShowResult.Finished)
		{
			OnRewarded();
		}
		else
		{
			OnRewardedVideoFailedToLoad("Failed to load video ad");
		}
	}
	public void HandleRewardedAdLoaded(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdLoaded event received");
	}

	public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
	{
		OnRewardedVideoFailedToLoad("Failed to load video ad");
	}

	public void HandleRewardedAdOpening(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdOpening event received");
	}

	public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
	{
		MonoBehaviour.print(
			"HandleRewardedAdFailedToShow event received with message: "
							 + args.Message);
	}

	public void HandleRewardedAdClosed(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdClosed event received");
	}

	public void HandleUserEarnedReward(object sender, Reward args)
	{
		OnRewarded();
	}
}
