using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsMenu : BaseMenu {

	public BaseMenu mainMenu, notificationMenu;
	public LoadingMenu loadingMenu;
	public Button[] levelButtons;
	public List<StatsUI> levelStatsUI;
	public List<PurchaseTab> purchaseTabs;
	public int[] levelPrices;
	public string[] levelNames;
	public Text[] levelNameLabels;
	public Text coinsText;
	private Wallet wallet;
	public int[] killsToUnlock;
	//public Text killsText;
	public GameObject restorePurchasesButton;

	[System.Serializable]
	public class StatsUI
	{
		public Text killsText, coinsText, collectedText, 
					timeText, walkText, driveText;

	}

	[System.Serializable]
	public class PurchaseTab
	{
		public Text priceText;
		public GameObject purchaseButton, priceTab;
	}

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
		if (PlayerPrefs.HasKey("continue"))
			PlayerPrefs.DeleteKey ("continue");
		#if UNITY_ANDROID
		restorePurchasesButton.SetActive(false);
		#endif
	}
	
	public override void Show() {
		base.Show ();
//		wallet = Wallet.Instance;
//		wallet.Init (coinsText);
		UpdateStats (0);

		int kills = PlayerPrefs.GetInt ("total_kills", 0);
		bool secondLevelLocked = true;
		for (int i = 1; i < levelButtons.Length; i++) {
			Debug.Log (kills + " " + killsToUnlock [i]);
			bool levelLocked = PlayerPrefs.GetInt ("level" + i + "", 0) == 0 && kills < killsToUnlock[i];
			if (i == 1)
				secondLevelLocked = levelLocked;
			SetLevelLocked (i, levelLocked);
		}
		if (secondLevelLocked)
			SetPurchaseTabEnabled (2, false);
	}

	public void SelectLevel(int level) {
		level++;
		loadingMenu.SetLevel (level);
		loadingMenu.Show ();
		this.Hide ();
//		int kills = PlayerPrefs.GetInt ("kills", 0);
//		if (kills >= killsToUnlock [level - 1]) {
//			loadingMenu.SetLevel (level);
//			loadingMenu.Show ();
//			this.Hide ();
//		} else {
//			notificationText.text = "KILL " + killsToUnlock [level - 1] + " GANGSTERS";
//			SetNotificationVisible (true);
//		}
	}

	public void BackToMainMenu() {
		mainMenu.Show();
		this.Hide();
	}

	void SetLevelLocked(int levelID, bool locked) {
		if (locked) {
			levelButtons [levelID].interactable = false;
			levelNameLabels [levelID-1].text = "LOCKED";
			SetStatsEnabled (levelID, false);
			SetPurchaseTabEnabled (levelID, true);
		} else {
			levelButtons [levelID].interactable = true;
			levelNameLabels [levelID-1].text = levelNames[levelID-1];
			SetPurchaseTabEnabled (levelID, false);
			SetStatsEnabled (levelID, true);
			if (levelID == 1)
				SetPurchaseTabEnabled (2, true);
		}
	}

	void SetStatsEnabled(int levelID, bool enabled) {
		levelStatsUI [levelID].killsText.enabled = enabled;
		levelStatsUI [levelID].coinsText.enabled = enabled;
		levelStatsUI [levelID].collectedText.enabled = enabled;
		levelStatsUI [levelID].timeText.enabled = enabled;
		levelStatsUI [levelID].walkText.enabled = enabled;
		levelStatsUI [levelID].driveText.enabled = enabled;
		if (enabled) {
			UpdateStats (levelID);
		}
	}

	void SetPurchaseTabEnabled(int levelID, bool enabled) {
		purchaseTabs [levelID-1].priceTab.SetActive (enabled);
		purchaseTabs [levelID-1].purchaseButton.gameObject.SetActive (enabled);
//		if (enabled) {
//			purchaseTabs [levelID-1].priceText.text = levelPrices [levelID - 1].ToString ("N0");
//		}
	}

	const float metersToMiles = 0.000621f;

	void UpdateStats(int levelID) {
		int kills = PlayerPrefs.GetInt ("level_kills" + (levelID + 1).ToString (), 0);
		levelStatsUI [levelID].killsText.text = "Criminal Kills: " + kills.ToString();
		int coins = PlayerPrefs.GetInt ("level_coins"+(levelID+1), 0);
		levelStatsUI [levelID].coinsText.text = "Coins Earned: " + coins.ToString();
		int collected = PlayerPrefs.GetInt ("level_collected" + (levelID + 1).ToString (), 0);
		levelStatsUI [levelID].collectedText.text = "Guns Collected: " + collected.ToString();
		float time = PlayerPrefs.GetFloat ("time_seconds" + (levelID + 1).ToString (), 0);
		levelStatsUI [levelID].timeText.text = "Time Played: " + Mathf.RoundToInt(time/60f) + " min";
		float walked_distance = PlayerPrefs.GetFloat ("distance_walked" + (levelID + 1).ToString (), 0);
		levelStatsUI [levelID].walkText.text = "Miles Run: " + (walked_distance*metersToMiles).ToString("F") + " m";
		float drive_distance = PlayerPrefs.GetFloat ("distance_drived" + (levelID + 1).ToString (), 0);
		levelStatsUI [levelID].driveText.text = "Drive Distance: " + (drive_distance*metersToMiles).ToString("F") + " m";
	}

	public void SetNotificationVisible(bool visible) {
		if (visible) {
			notificationMenu.Show ();
		} else {
			notificationMenu.Hide ();
		}
	}

	public void PurchaseLevel(int levelID) {
		#if UNITY_ANDROID
		if (levelID == 1) {
//			GameBillingManagerExample.purchase (GameBillingManagerExample.SECOND_TOWN);
		} else if (levelID == 2) {
		//	GameBillingManagerExample.purchase (GameBillingManagerExample.THIRD_TOWN);
		}
		#elif UNITY_IOS
		/*if (levelID == 1) {
			PaymentManagerExample.buyItem (PaymentManagerExample.SECOND_TOWN);
		} else if (levelID == 2) {
			PaymentManagerExample.buyItem (PaymentManagerExample.THIRD_TOWN);
		}*/
		#endif
//		if (wallet.HasValue (levelPrices [levelID-1])) {
//			wallet.Pay (levelPrices[levelID-1]);
//			PlayerPrefs.SetInt ("level" + levelID + "", 1);
//			SetLevelLocked (levelID, false);
//		} else {
//			SetNotificationVisible (true);
//		}
	}

	public void OnLevelPurchased(int levelID) {
		PlayerPrefs.SetInt ("level" + levelID + "", 1);
		SetLevelLocked (levelID, false);
	}
}
