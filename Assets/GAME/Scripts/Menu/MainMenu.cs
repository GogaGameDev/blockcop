using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : BaseMenu {

	public LevelsMenu levelsMenu;
	public IAPStoreMenu shopMenu;
	public RateMenu rateMenu;
	public ShopMenu upgradesMenu;
	public AchievementsMenu achievementsMenu;
	public Button soundButton;
	public Sprite soundEnabledSprite, soundDisabledSprite;
	public AudioSource music;
	public Text highscoreText, bestKillStreakText;
	public GameObject character;

	private string KILLS_LEADERBOARD_ID_IOS = "bcc_total_kills";
	private string COINS_LEADERBOARD_ID_IOS = "bcc_total_coins";
	private string COLLECTED_LEADERBOARD_ID_IOS = "bcc_guns_collected";
	private string TIME_LEADERBOARD_ID_IOS = "bcc_time_played";
	private string HIGHSCORE_LEADERBOARD_ID_IOS = "bcc_highscore";
	private string KILLSTREAK_LEADERBOARD_ID_IOS = "bcc_killstreak";
	private string MILESRUN_LEADERBOARD_ID_IOS = "bcc_milesrun";
	private string MILESDRIVEN_LEADERBOARD_ID_IOS = "bcc_milesdriven";

	private string KILLS_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQAQ";
	private string COINS_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQAg";
	private string COLLECTED_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQAw";
	private string TIME_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQBA";
	private string HIGHSCORE_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQBQ";
	private string KILLSTREAK_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQBg";
	private string MILESRUN_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQBw";
	private string MILESDRIVEN_LEADERBOARD_ID_ANDROID = "CgkIvpjs_qAIEAIQCA";

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll();
		menuCG = GetComponent<CanvasGroup> ();
		if (PlayerPrefs.GetInt ("sound", 1) == 1) {
			soundButton.image.sprite = soundEnabledSprite;
			music.Play ();
		} else {
			soundButton.image.sprite = soundDisabledSprite;
		}
		#if UNITY_IOS

		#endif
		highscoreText.text = PlayerPrefs.GetInt ("highscore", 0).ToString("N0");
		bestKillStreakText.text = PlayerPrefs.GetInt ("killstreak", 0).ToString("N0");
	}

	public override void Show() {
		base.Show ();
		character.SetActive (true);
		character.GetComponentInChildren<SetPlayerSkin> ().UpdateSkin ();
	}

	public override void Hide() {
		base.Hide ();
		character.SetActive (false);
	}
	
	public void PlayButtonEvent() {
		levelsMenu.Show ();
		this.Hide ();
	}

	public void UpgradesButtonEvent() {
		upgradesMenu.Show ();
		this.Hide ();
	}

	public void ShopButtonEvent() {
		shopMenu.Show ();
		this.Hide ();
	}

	public void ExitButtonEvent() {
		Application.Quit();
	}

	public void SoundButtonEvent() {
		if (PlayerPrefs.GetInt ("sound", 1) == 1) {
			PlayerPrefs.SetInt ("sound", 0);
			soundButton.image.sprite = soundDisabledSprite;
			music.Pause ();
		} else {
			PlayerPrefs.SetInt ("sound", 1);
			soundButton.image.sprite = soundEnabledSprite;
			music.Play ();
		}
	}

	public void RateButtonEvent() {
		rateMenu.Show ();
		character.SetActive (false);
	}

	public void AchievementsButtonEvent() {
		achievementsMenu.Show ();
		this.Hide ();
	}

	const float metersToMiles = 0.000621f;

	public void LeaderboardsButtonEvent() {
		LeaderboardHelper.Instance.Show();
	}
}
