﻿using UnityEngine;
using System.Collections;

//base menu class
public class BaseMenu : MonoBehaviour {

	public float fadeSpeed = 0.2f;
	protected CanvasGroup menuCG;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
	}

	public virtual void Show() {
		LeanTween.alphaCanvas (menuCG, 1f, fadeSpeed).setOnComplete(() => ShowActions()).setIgnoreTimeScale(true);
	}

	public virtual void Hide() {
		menuCG.blocksRaycasts = false;
		LeanTween.alphaCanvas (menuCG, 0f, fadeSpeed).setIgnoreTimeScale(true);
	}

	protected virtual void ShowActions() {
		menuCG.blocksRaycasts = true;
	}
		


}
