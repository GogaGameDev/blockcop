﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : BaseMenu {

	public BaseMenu mainMenu;
	public Slider carSlider, humanSlider;
	public Text soundButtonText, carCounterText, humanCounterText; 
	public AudioSource music;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
		if (PlayerPrefs.GetInt ("sound", 1) == 1) {
			soundButtonText.text = "SOUND: ON";
			music.Play ();
		} else {
			soundButtonText.text = "SOUND: OFF";
		}
		carSlider.value = PlayerPrefs.GetInt ("maxcars", 10);
		humanSlider.value = PlayerPrefs.GetInt ("maxhumans", 10);
		carCounterText.text = ((int)carSlider.value).ToString();
		humanCounterText.text = ((int)humanSlider.value).ToString();
	}
	
	public void BackToMainMenu() {
		mainMenu.Show();
		this.Hide();
	}

	public void SoundButtonEvent() {
		if (PlayerPrefs.GetInt ("sound", 1) == 1) {
			PlayerPrefs.SetInt ("sound", 0);
			soundButtonText.text = "SOUND: OFF";
			music.Pause ();
		} else {
			PlayerPrefs.SetInt ("sound", 1);
			soundButtonText.text = "SOUND: ON";
			music.Play ();
		}
	}

	public void OnCarsAmountChanged() {
		carCounterText.text = ((int)carSlider.value).ToString();
		PlayerPrefs.SetInt ("maxcars", (int)carSlider.value);
	}

	public void OnHumansAmountChanged() {
		humanCounterText.text = ((int)humanSlider.value).ToString();
		PlayerPrefs.SetInt ("maxhumans", (int)humanSlider.value);
	}
}
