﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterTab : MonoBehaviour {

	public Text availableText;
	public Text costText;
	public Text descriptionText;
	public Text multiplierText;
	public Button useButton;

	// Use this for initialization
	void Start () {
		
	}


	public void Init(int available, int cost) {
		availableText.text = "Currently Available: " + available.ToString ();
		costText.text = "Cost: " + cost.ToString ();
		useButton.interactable = available >= cost;
	}

	public void InitMultiplierBooster(int available, int cost, int currentMult, int nextMult) {
		availableText.text = "Currently Available: " + available.ToString ();
		descriptionText.text = "Currently Multiplier: " + currentMult.ToString () + "x";
		if (nextMult > 0) {
			costText.text = "Cost: " + cost.ToString ();
			multiplierText.gameObject.SetActive (true);
			multiplierText.text = "Increase Multiplier to: " + nextMult.ToString () + "x";
		} else {
			multiplierText.gameObject.SetActive (false);
			costText.gameObject.SetActive (false);
			useButton.interactable = false;
			return;
		}

		useButton.interactable = available >= cost;
	}
}
