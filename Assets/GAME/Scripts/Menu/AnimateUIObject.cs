﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimateUIObject : MonoBehaviour {

	public enum AnimationType {
		ImageAlpha, OutlineAlpha
	}

	public AnimationType type = AnimationType.ImageAlpha;
	public float animationSpeed = 1f;
	public float targetValue = 0.5f;
	Outline outline;

	// Use this for initialization
	void Start () {
		if (type == AnimationType.ImageAlpha) {
			LeanTween.alpha (GetComponent<RectTransform> (), targetValue, animationSpeed).setLoopPingPong ();
		} else if (type == AnimationType.OutlineAlpha) {
			LeanTween.value (1f, targetValue, animationSpeed).setLoopPingPong ().setOnUpdate((float a) => OutlineAnimation(a));
		} 
		outline = GetComponent<Outline> ();
	}
	
	void OutlineAnimation(float a) {
		Color c = outline.effectColor;
		c.a = a;
		outline.effectColor = c;
	}

	void OnDestroy() {
		LeanTween.cancelAll ();;
	}
}
