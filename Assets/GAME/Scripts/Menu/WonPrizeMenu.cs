using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WonPrizeMenu : BaseMenu {

	public Text prizeValueLabel;
	public Image prizeIcon;
	public Sprite[] prizeSprites;

	private string currentPrizePrefsName = "";
	private int currentPrizeValue = 0;
	private Wallet wallet = null;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
//		PlayerPrefs.SetInt ("gear", 5);
//		PlayerPrefs.SetInt ("halo", 7);
	}
	
	public override void Show() {
		base.Show ();
		GetRandomPrize ();
	}

	public void ShowInStore(Wallet _wallet) {
		Show ();
		wallet = _wallet;
		wallet.UpdateValue ();
	}

	void GetRandomPrize() {
		int r = Random.Range (0, 100);
		if (r == 0) { //win 1 halo
			AddPrize("halo", 1, prizeSprites[0]);
		} else if (r == 1) { //win 1 X
			AddPrize("multiplier", 1, prizeSprites[1]);
		} else if (2 <= r && r <= 4) { //win 1 magnet
			AddPrize("magnet", 1, prizeSprites[2]);
		} else if (5 <= r && r <= 7) { //win 1 health
			AddPrize("aidkit", 1, prizeSprites[3]);
		} else if (8 <= r && r <= 10) { //win 1 gear
			AddPrize("gear", 1, prizeSprites[4]);
		} else if (11 <= r && r <= 12) { //win 1 shield
			AddPrize("shield", 1, prizeSprites[5]);
		} else if (13 <= r && r <= 57) { //win 250 coins
			AddPrize("coins", 250, prizeSprites[6]);
		} else if (58 <= r && r <= 87) { //win 500 coins
			AddPrize("coins", 500, prizeSprites[6]);
		} else if (88 <= r && r <= 97) { //win 1000 coins
			AddPrize("coins", 1000, prizeSprites[6]);
		} else if (98 <= r && r <= 99) { //win 2500 coins
			AddPrize("coins", 2500, prizeSprites[6]);
		}
	}

	void AddPrize(string playerPrefsName, int value, Sprite prizeSprite) {
		PlayerPrefs.SetInt (playerPrefsName, PlayerPrefs.GetInt (playerPrefsName, 0) + value);
		PlayerPrefs.SetInt ("session_"+playerPrefsName, PlayerPrefs.GetInt ("session_"+playerPrefsName, 0) + value);
		prizeValueLabel.text = value.ToString ();
		prizeIcon.sprite = prizeSprite;
		currentPrizePrefsName = playerPrefsName;
		currentPrizeValue = value;
	}

	public void DoublePrize() {
		prizeValueLabel.text = (currentPrizeValue*2).ToString ();
		PlayerPrefs.SetInt (currentPrizePrefsName, PlayerPrefs.GetInt (currentPrizePrefsName, 0) + currentPrizeValue);
		PlayerPrefs.SetInt ("session_"+currentPrizePrefsName, PlayerPrefs.GetInt ("session_"+currentPrizePrefsName, 0) + currentPrizeValue);
		if (wallet)
			wallet.UpdateValue ();
	}
}
