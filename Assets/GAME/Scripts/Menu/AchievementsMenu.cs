﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsMenu : BaseMenu {

	public AchievementItem[] items;
	public List<Achievement> achievements;
	public BaseMenu mainMenu;
	public Sprite[] rewardIcons;

	// Use this for initialization
	void Start () {
		menuCG = GetComponent<CanvasGroup> ();
	}

	public override void Show() {
		base.Show ();
		ShowAchievementsUI ();
	}

	void ShowAchievementsUI() {
		for (int i = 0; i < achievements.Count; i++) {
			int currentLevel = PlayerPrefs.GetInt (achievements[i].nameDescr, 0);;
			int currentValue = PlayerPrefs.GetInt (achievements [i].playerPrefsName, 0);
//			for (int k = 0; k < achievements [i].targetValues.Length; k++) {
//				if (currentValue >= achievements [i].targetValues [k]) {
//					currentLevel++;
//				}
//			}
			if (currentLevel >= achievements [i].targetValues.Length) {
				int lastLevel = achievements [i].targetValues.Length - 1;
				items[i].nameLabel.text = achievements[i].nameDescr + " (LVL " + (lastLevel+1).ToString() + ")";
				items [i].targetLabel.text = achievements [i].targetDescr [lastLevel];
				items [i].completedLabel.SetActive (true);
				items [i].rewardLabel.gameObject.SetActive (false);
				items [i].progressSlider.gameObject.SetActive (false);
			} else {
				items[i].nameLabel.text = achievements[i].nameDescr + " (LVL " + (currentLevel+1).ToString() + ")";
				items [i].targetLabel.text = achievements [i].targetDescr [currentLevel];
				items [i].completedLabel.SetActive (false);
				items [i].rewardLabel.gameObject.SetActive (true);
				items [i].progressSlider.gameObject.SetActive (true);
				items [i].rewardLabel.text = achievements [i].rewardValues [currentLevel].ToString("N0");
				items [i].rewardLabel.GetComponentInChildren<Image>().sprite = getRewardSprite(achievements [i].rewardPrefsNames[currentLevel]);
				items [i].progressSlider.value = (float)currentValue / (float)achievements [i].targetValues [currentLevel];
				items [i].progressLabel.text = currentValue + "/" + achievements [i].targetValues [currentLevel];
			}
		}
	}

	public Sprite getRewardSprite(string name) {
		switch (name) {
		case "coins":
			return rewardIcons [0];
			break;
		case "gear":
			return rewardIcons [1];
			break;
		case "shield":
			return rewardIcons [2];
			break;
		case "halo":
			return rewardIcons [3];
			break;
		}
		return rewardIcons [0];
	}

	public void BackButtonEvent() {
		this.Hide ();
		mainMenu.Show ();
	}

}
