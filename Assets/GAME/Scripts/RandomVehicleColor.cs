﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomVehicleColor : MonoBehaviour {

	public MeshRenderer[] renderers;
	public Material[] materials;

	// Use this for initialization
	void Start () {
		int matID = Random.Range (0, materials.Length);
		foreach (MeshRenderer r in renderers) {
			r.material = materials [matID];
		}
	}
	
}
