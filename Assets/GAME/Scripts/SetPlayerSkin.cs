﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerSkin : MonoBehaviour {

	public List<Skin> skins;

	private int currentSkin = 0;

	[System.Serializable]
	public class Skin
	{
		public Material baseMaterial;
		public SkinnedMeshRenderer mesh;
		public Material material;
	}

	// Use this for initialization
	void Start () {
		UpdateSkin ();
	}

	public void UpdateSkin() {
		SetSkinEnabled (currentSkin, false);
		currentSkin = PlayerPrefs.GetInt ("skin", 0);
		SetSkinEnabled (currentSkin, true);
	}

	void SetSkinEnabled(int skinID, bool enabled) {
		skins [skinID].mesh.gameObject.SetActive (enabled);
		if (enabled) {
			GetComponent<SkinnedMeshRenderer> ().material = skins [skinID].baseMaterial;
			skins [skinID].mesh.material = skins [skinID].material;
		}
	}

	public void SetSkin(int skinID) {
		SetSkinEnabled (currentSkin, false);
		currentSkin = skinID;
		SetSkinEnabled (currentSkin, true);
		Debug.Log ("yep");
	}
}
