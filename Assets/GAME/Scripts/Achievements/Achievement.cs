﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Achievement {
	public string nameDescr;
	public string[] targetDescr;
	public int[] targetValues;
	public string playerPrefsName;
	public int[] rewardValues;
	public string[] rewardPrefsNames;
}

