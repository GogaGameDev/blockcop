﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AchievementController : MonoBehaviour {

	public static AchievementController Instance;
	public Transform player;
	[HideInInspector]
	public bool measureWalking = true;
	public AchievementsMenu menu;
	public AchievementCounter firstKillsCounter, totalKillsCounter, coinsCounter, timeCounter, strokeKillsCounter, copCarKillsCounter, 
					   stolenCarKillsCounter, pistolKillsCounter, stickKillsCounter, shotgunKillsCounter,
					   assaultKillsCounter, pickupCounter, driveCounter, stepCounter, explosionCounter, deathCounter;

	private float savedTimeLevel = 0f, timePlayedSecondsAtLevel = 0f, savedTimeTotal = 0f, timePlayedSecondsTotal = 0f;
	private float distanceWalked = 0f, distanceDrived = 0f, milesDriven = 0f, milesWalked = 0f;
	[HideInInspector]
	public int earnedCoinsAtLevel, killsAtLevel, gunsCollectedAtLevel;
	private Vector3 lastPosition;
	private int levelID = 1;

	const float metersToMiles = 0.000621f;

	public class AchievementCounter
	{
		public int value = 0;
		private int level = 0;
		private Achievement achievement;
		public bool active = true;

		public AchievementCounter(Achievement achievement) {
			value = PlayerPrefs.GetInt(achievement.playerPrefsName, 0);
			this.achievement = achievement;
			level = getCurrentLevel(value);
			CheckAchievementCompleted();
		}

		public void Inc() {
			if (active) {
				value++;
				CheckTargetAchieved ();
			}
		}

		public void Inc(int i) {
			if (active) {
				value += i;
				CheckTargetAchieved ();
			}
		}

		int getCurrentLevel(int currentValue) {
			int result = 0;
//			for (int k = 0; k < achievement.targetValues.Length; k++) {
//				if (currentValue >= achievement.targetValues [k]) {
//					result++;
//				}
//			}
			result = PlayerPrefs.GetInt (achievement.nameDescr, 0);
			return result;
		}


		void CheckTargetAchieved() {
			if (value >= achievement.targetValues [level]) {
				GameController.Instance.AddAchievementReward (achievement.rewardPrefsNames [level], achievement.rewardValues [level]);
				GameUI.Instance.OnAchievementCompleted(achievement.nameDescr + " (LVL " + (level+1).ToString() + ")", 
													   achievement.rewardValues[level], achievement.rewardPrefsNames[level]);
				level++;
				PlayerPrefs.SetInt (achievement.nameDescr, level);
				CheckAchievementCompleted ();
			}
		}

		void CheckAchievementCompleted() {
			if (level >= achievement.targetValues.Length) {
				active = false;
			}
		}

		public void Save() {
			PlayerPrefs.SetInt (achievement.playerPrefsName, value);
		}

		public void Reset() {
			PlayerPrefs.SetInt(achievement.playerPrefsName, 0);
			value = 0;
		}
	}

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll ();
		firstKillsCounter = new AchievementCounter (menu.achievements [0]);
		totalKillsCounter = new AchievementCounter (menu.achievements [1]);
		coinsCounter = new AchievementCounter (menu.achievements [2]);
		timeCounter = new AchievementCounter (menu.achievements [3]);
		strokeKillsCounter = new AchievementCounter (menu.achievements [4]);
		copCarKillsCounter = new AchievementCounter (menu.achievements [5]);
		stolenCarKillsCounter = new AchievementCounter (menu.achievements [6]);
		pistolKillsCounter = new AchievementCounter (menu.achievements [7]);
		stickKillsCounter = new AchievementCounter (menu.achievements [8]);
		shotgunKillsCounter = new AchievementCounter (menu.achievements [9]);
		assaultKillsCounter = new AchievementCounter (menu.achievements [10]);
		pickupCounter = new AchievementCounter (menu.achievements [11]);
		driveCounter = new AchievementCounter (menu.achievements [12]);
		stepCounter = new AchievementCounter (menu.achievements [13]);
		explosionCounter = new AchievementCounter (menu.achievements [14]);
		deathCounter = new AchievementCounter (menu.achievements [15]);
		strokeKillsCounter.Reset ();
		//player = GameController.Instance.player.transform;
		levelID = SceneManager.GetActiveScene ().buildIndex;
		savedTimeLevel = PlayerPrefs.GetFloat ("time_seconds"+levelID.ToString(), 0);
		savedTimeTotal = PlayerPrefs.GetFloat ("total_time_seconds", 0);
		distanceWalked = PlayerPrefs.GetFloat ("distance_walked"+levelID.ToString(), 0);
		milesWalked = PlayerPrefs.GetFloat ("miles_walked_total", 0);
		distanceDrived = PlayerPrefs.GetFloat ("distance_drived"+levelID.ToString(), 0);
		milesDriven = PlayerPrefs.GetFloat ("miles_driven_total", 0);
		killsAtLevel = PlayerPrefs.GetInt ("level_kills"+levelID.ToString(), 0);
		earnedCoinsAtLevel = PlayerPrefs.GetInt ("level_coins"+levelID.ToString(), 0);
		gunsCollectedAtLevel = PlayerPrefs.GetInt ("level_collected"+levelID.ToString(), 0);
	}

	public void SaveAll() {
		firstKillsCounter.Save ();
		totalKillsCounter.Save ();
		coinsCounter.Save ();
		timeCounter.Save ();
		strokeKillsCounter.Save ();
		copCarKillsCounter.Save ();
		stolenCarKillsCounter.Save ();
		pistolKillsCounter.Save ();
		stickKillsCounter.Save ();
		shotgunKillsCounter.Save ();
		assaultKillsCounter.Save ();
		pickupCounter.Save ();
		driveCounter.Save ();
		stepCounter.Save ();
		explosionCounter.Save ();
		deathCounter.Save ();
		PlayerPrefs.SetFloat ("total_time_seconds", timePlayedSecondsTotal);
	}
	
	void OnDestroy() {
		SaveAll ();
		PlayerPrefs.SetFloat ("distance_walked"+levelID.ToString(), distanceWalked);
		PlayerPrefs.SetFloat ("distance_drived"+levelID.ToString(), distanceDrived);
		PlayerPrefs.SetFloat ("miles_walked_total", milesWalked);
		PlayerPrefs.SetFloat ("miles_driven_total", milesDriven);
		PlayerPrefs.SetFloat ("time_seconds"+levelID.ToString(), PlayerPrefs.GetFloat("time_seconds"+levelID.ToString(), 0) 
							  + timePlayedSecondsAtLevel);
		//PlayerPrefs.SetFloat ("total_time_seconds", PlayerPrefs.GetFloat("total_time_seconds", 0) + timePlayedSeconds);
		PlayerPrefs.SetInt ("level_kills"+levelID.ToString(), killsAtLevel);
		PlayerPrefs.SetInt ("level_coins"+levelID.ToString(), earnedCoinsAtLevel);
		PlayerPrefs.SetInt ("level_collected"+levelID.ToString(), gunsCollectedAtLevel);
	}

	void Update() {
		//calculate playing time
		timePlayedSecondsAtLevel = savedTimeLevel + Time.timeSinceLevelLoad;
		timePlayedSecondsTotal = savedTimeTotal + Time.timeSinceLevelLoad;
		if (timeCounter.active) {
			float timePlayedHours = timePlayedSecondsTotal / 3600f;
			if (timePlayedHours - (float)timeCounter.value >= 1f) {
				timeCounter.Inc ();
			}
		}
		//calculate travel distances
		float magnitude = (lastPosition - player.position).magnitude;
		if (measureWalking) {
			distanceWalked += magnitude;
			milesWalked += magnitude * metersToMiles;
			if (milesWalked - (float)stepCounter.value >= 1f) {
				stepCounter.Inc ();
			}
			lastPosition = player.position;
			UpdateWalkScore ();
		} else {
			distanceDrived += magnitude;
			milesDriven += magnitude * metersToMiles;
			if (milesDriven - (float)driveCounter.value >= 1f) {
				driveCounter.Inc ();
			}
			lastPosition = player.position;
			UpdateDriveScore ();
		}
	}

	float lastWalkDistance = 0f;
	void UpdateWalkScore() {
		if (distanceWalked - lastWalkDistance >= 1f) {
			lastWalkDistance = distanceWalked;
			GameController.Instance.AddScore (5);  
		}
	}

	float lastDriveMiles = 0f;
	void UpdateDriveScore() {
		float currentDriveMiles = distanceDrived * metersToMiles;
		if (currentDriveMiles - lastDriveMiles >= 0.1f) {
			lastDriveMiles = currentDriveMiles;
			GameController.Instance.AddScore (20);  
		}
	}

	public void SetMeasureWalking(bool enabled) {
		measureWalking = enabled;
		if (enabled) {
			lastWalkDistance = distanceWalked;
		} else {
			float currentDriveMiles = distanceDrived * metersToMiles;
			lastDriveMiles = currentDriveMiles;
		}
	}
}
