using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using BehaviorDesigner.Runtime;
using Opsive.ThirdPersonController;
using Opsive.ThirdPersonController.Input;

public class GameController : MonoBehaviour {

	public static GameController Instance = null;

	public GameObject player, playerCenter;

	public List<WaveLevelSettings> waveSettings;

	private int currentWave = 0;
	private int currentWaveSettings = 0;

	[System.Serializable]
	public class WaveLevelSettings
	{
		public int enemyMinCount = 2;
		public int enemyMaxCount = 2;
		public float enemyMinLife = 10f;
		public float enemyMaxLife = 20f;
		public GameObject[] enemyPrefabs;
		public int rewardBonus = 0;
	}

	public float minEnemyDistance = 120f;

	public GameObject[] pickUpWeaponsPrefabs;
	public int[] pickUpBulletsCount;
	public GameObject[] pickUpItemPrefabs;
	public float respawnTime = 300;

	public Color lockEnabledColor = Color.red;
	public Color lockDisabledColor = Color.red;
	public Button lockButton;

	public int rewardValue = 50;
	public int failValue = 50;

	public string startMessage;
	public string[] endWaveMessages;

	private int enemiesLeft;
	private Text missionText, coinsText;
//	[HideInInspector]
	//public int earnedCoins = 0;
	[HideInInspector]
	public int kills = 0;
	[HideInInspector]
	public int sessionCoins = 0;
	private int maxWaves = 10;
	private int coinsMult = 1;
	private int actualWave = 0;
	//bonuses
	[HideInInspector]
	public int halosNeeded = 1;
	private int misteryBoxes = 0;
	private bool magnetEnabled = false;
	private float magnetRadius = 200f;
	private LayerMask magnetMask;
	[HideInInspector]
	public int score = 0;
	private float timer = 0f;
	[HideInInspector]
	public bool canAddScore = true;
	[HideInInspector]
	public bool shieldEnabled = false;
	[HideInInspector]
	public bool isPlayerInCar = false;
	public List<WeaponItem> weaponItems;
	public GameObject[] vehiclePrefabs;
	public Transform vehicleSpawnPoint;

	[System.Serializable]
	public class WeaponItem {
		public PrimaryItemType weaponType;
		public ConsumableItemType bulletsType;
		public int ammo = 100;
	}

	public enum PlayerState {
		Walk, DrivePoiceCar, DriveStolenCar
	}


	public PlayerState playerState = PlayerState.Walk;

	void Awake() {
//		if (PlayerPrefs.HasKey ("continue")) {
//			currentWave = PlayerPrefs.GetInt ("continue", 0);
//			kills = PlayerPrefs.GetInt ("continue_kills", 0);
//			earnedCoins = PlayerPrefs.GetInt ("continue_coins", 0);
//		}
		InitWeaponDamage();
	}

	// Use this for initialization
	void Start () {
		QualitySettings.anisotropicFiltering = AnisotropicFiltering.ForceEnable;
		Instance = this;
		missionText = GameUI.Instance.missionText;
		coinsText = GameUI.Instance.sessionCoinsText;
		lockDisabledColor = lockButton.image.color;
		ShowMessage (startMessage, 5f);
		CreateWave ();
		CreateItems ();
		GameObject.Find ("DungeonSimpleCity").SetActive (false);
		foreach (DungeonArchitect.DungeonSceneProviderData data in FindObjectsOfType<DungeonArchitect.DungeonSceneProviderData>())
			Destroy (data.GetComponent<DungeonArchitect.DungeonSceneProviderData> ());
		CheckDailyReward ();
		if (PlayerPrefs.GetInt("double_coins", 0) == 1)
			coinsMult = 2;
		coinsText.text = sessionCoins.ToString () + getMultiplierUI();
		magnetMask = LayerMask.GetMask ("Triggers");
		timer = Time.time;
		//PlayerPrefs.SetInt ("halo", 5);
		StartCoroutine(InitAmmunitions(0.3f));
		InitVehicles ();
	}


	IEnumerator InitAmmunitions(float delay) {
		yield return new WaitForSeconds (delay);
		InitPlayerAmmunition ();
	}

	void InitWeaponDamage() {
		int weaponID = PlayerPrefs.GetInt("weapon", 0);
		if (weaponID > 0) {
			int weaponDamage = PlayerPrefs.GetInt ("weapon" + weaponID + "damage", 0);
			foreach (ShootableWeapon weapon in player.GetComponentsInChildren<ShootableWeapon>()) {
				if (weapon.ItemType == weaponItems [weaponID - 1].weaponType) {
					weapon.m_HitscanDamageAmount = weapon.m_HitscanDamageAmount + weapon.m_HitscanDamageAmount * (25f * weaponDamage) / 100f;
				}
			}
		}
		int meleeDamage = PlayerPrefs.GetInt ("weapon0damage", 0);
		foreach (MeleeWeapon weapon in player.GetComponentsInChildren<MeleeWeapon>()) {
			weapon.m_DamageAmount = weapon.m_DamageAmount + weapon.m_DamageAmount * (25f * meleeDamage)  / 100f;
		}
	}

	void InitPlayerAmmunition() {
		//player.GetComponent<CharacterHealth> ().SetShieldAmount (0);
		//init weapons
		int weaponID = PlayerPrefs.GetInt("weapon", 0);
		if (weaponID > 0) {
			int weaponDamage = PlayerPrefs.GetInt ("weapon" + weaponID + "damage", 0);
			int weaponAmmo = PlayerPrefs.GetInt ("weapon" + weaponID + "ammo", 0);

			int ammo = weaponItems [weaponID - 1].ammo + Mathf.RoundToInt (weaponItems [weaponID - 1].ammo * (25f * weaponAmmo) / 100f);
			Debug.Log ("ammo" + ammo);
			player.GetComponent<Inventory> ().PickupItem (weaponItems [weaponID - 1].weaponType.ID, 1, true, false);
			player.GetComponent<Inventory> ().PickupItem (weaponItems [weaponID - 1].bulletsType.ID, ammo, true, false);
//			foreach (ShootableWeapon weapon in player.GetComponentsInChildren<ShootableWeapon>()) {
//				if (weapon.ItemType == weaponItems [weaponID - 1].weaponType) {
//					weapon.m_HitscanDamageAmount = weapon.m_HitscanDamageAmount + weapon.m_HitscanDamageAmount * (25f * weaponDamage) / 100f;
//					Debug.Log (weaponDamage);
//				}
//			}
		}

	}

	void InitVehicles() {
		//init vehicle
		int vehicleID = PlayerPrefs.GetInt("vehicle", 0);
		GameObject copVehicle = Instantiate (vehiclePrefabs [vehicleID], vehicleSpawnPoint.position, vehicleSpawnPoint.rotation) as GameObject;
		int vehicleSpeed = PlayerPrefs.GetInt ("vehicle" + vehicleID + "speed", 0);
		int vehicleHealth = PlayerPrefs.GetInt ("vehicle" + vehicleID + "health", 0);
		int vehicleArmor = PlayerPrefs.GetInt ("vehicle" + vehicleID + "armor", 0);
		int vehicleDamage = PlayerPrefs.GetInt ("vehicle" + vehicleID + "damage", 0);
		int vehicleAmmo = PlayerPrefs.GetInt ("vehicle" + vehicleID + "ammo", 0);
		Debug.Log (vehicleHealth + "sdsd" + vehicleArmor + " " + vehicleID);
		if (copVehicle.GetComponent<VehicleControl> ()) {
			copVehicle.GetComponent<VehicleControl> ().UpgradePower (25 * vehicleSpeed);
			copVehicle.GetComponent<VehicleControl> ().UpgradeDamage (50 * vehicleDamage);
		} else {
			copVehicle.GetComponent<BikeControl> ().UpgradePower (25 * vehicleSpeed);
			copVehicle.GetComponent<BikeControl> ().UpgradeDamage (50 * vehicleDamage);
		}
		if (copVehicle.GetComponent<VehicleGun> ()) {
			if (vehicleID == 3) { //moto
				int assaultAmmoUpgrade = PlayerPrefs.GetInt ("weapon" + 3 + "ammo", 0);
				int gunAmmo = weaponItems[2].ammo + weaponItems[2].ammo * Mathf.RoundToInt((25f * assaultAmmoUpgrade)  / 100f);
				gunAmmo = gunAmmo * vehicleAmmo;
				copVehicle.GetComponent<VehicleGun> ().SetAmmo (gunAmmo);
			} else if (vehicleID == 4) { //tank
				int rocketAmmoUpgrade = PlayerPrefs.GetInt ("weapon" + 5 + "ammo", 0);
				int gunAmmo = weaponItems[4].ammo + weaponItems[4].ammo * Mathf.RoundToInt((25f * rocketAmmoUpgrade)  / 100f);
				gunAmmo = gunAmmo * vehicleAmmo;
				copVehicle.GetComponent<VehicleGun> ().SetAmmo (gunAmmo);
			}

		}
		Health health = copVehicle.GetComponent<Health> ();
		health.MaxHealth = health.MaxHealth + health.MaxHealth * (50f * vehicleHealth) / 100f;
		health.MaxShield = health.MaxHealth * (50f * vehicleArmor) / 100f;
		health.SetHealthAmount (health.MaxHealth);
		health.SetShieldAmount (health.MaxShield);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.I)) {
			//GameObject.FindWithTag ("Enemy").GetComponent<Health> ().TakeDown (0f, GameObject.FindWithTag ("Enemy").transform.forward * -1000);
			//GameObject.FindWithTag ("Enemy").GetComponent<CharacterHealth> ().InstantDeath();
			//player.GetComponent<Inventory>().GetCurrentItem(typeof(PrimaryItemType)).gameObject.SetActive(false);
			//FindObjectOfType<GameUI>().RestartGame();
			//player.GetComponent<AnimatorMonitor> ().OnRespawn ();
			//player.GetComponent<Health> ().SetHealthAmount (player.GetComponent<Health> ().MaxHealth);
			player.GetComponent<CharacterHealth> ().InstantDeath();
			SetStickKinematic (true);
		}
		if (magnetEnabled && !isPlayerInCar) {
			Collider[] pickUpColliders = Physics.OverlapSphere (player.transform.position, magnetRadius, magnetMask);
			for (int i = 0; i < pickUpColliders.Length; i++) {
				pickUpColliders [i].GetComponent<MagnetItem> ().Move (player.transform);
			}
		}
		AddScoreEachSecond ();
	}

	Vector3 firstPosition;

	void CreateWave() {
		if (currentWave != 0 && currentWave % 2 == 0 && currentWaveSettings < waveSettings.Count-1)
			currentWaveSettings++;
		WaveLevelSettings waveHolder = waveSettings [currentWaveSettings];
		enemiesLeft = 0;
		for (int i = 0; i < waveHolder.enemyMinCount; i++) {
			Vector3 pos = Vector3.zero;
			if (i == 0) {
				pos = GetSpawnPoint (false);
				firstPosition = pos;
			} else {
				pos = getNearPosition (firstPosition, 10f);
			}
			int r = Random.Range (0, waveHolder.enemyPrefabs.Length);
			GameObject enemy = Instantiate (waveHolder.enemyPrefabs [r]) as GameObject;
			float enemyLife = Random.Range (waveHolder.enemyMinLife, waveHolder.enemyMaxLife);
			enemy.GetComponent<CharacterHealth>().MaxHealth = enemyLife;
			enemy.GetComponent<CharacterHealth>().SetHealthAmount(enemyLife);
			enemy.GetComponent<BehaviorTree> ().GetVariable ("Doug").SetValue (player);
			enemy.GetComponent<BehaviorTree> ().GetVariable ("Doug Center").SetValue (playerCenter);
			pos.y = waveHolder.enemyPrefabs [r].transform.position.y;
			//enemy.transform.position = pos;
			enemy.GetComponent<NavMeshAgent> ().Warp (pos);
			enemiesLeft++;
		}
	}

	void ShowMessage(string text, float time) {
		if (LeanTween.isTweening (missionText.gameObject)) {
			LeanTween.cancel (missionText.gameObject);
		}
		Color c = missionText.color;
		c.a = 0;
		missionText.color = c;
		missionText.text = text;
		LeanTween.alphaText(missionText.GetComponent<RectTransform>(), 1f, 1f);
		LeanTween.alphaText (missionText.GetComponent<RectTransform> (), 0f, 1f).setDelay (time);
	}

	void CreateItems() {
		for (int i = 0; i < pickUpWeaponsPrefabs.Length; i++) {
			Vector3 pos = GetSpawnPoint (true);
			GameObject weapon = Instantiate (pickUpWeaponsPrefabs[i]) as GameObject;
			pos.y = pickUpWeaponsPrefabs[i].transform.position.y;
			weapon.transform.position = pos;
			weapon.AddComponent<Respawner> ().SetTime (respawnTime);
			foreach (Inventory.ItemAmount item in weapon.GetComponent<ItemPickup>().ItemList) {
				if (item.Amount > 1) {
					item.Initialize (item.ItemType, pickUpBulletsCount[i]);
					break;
				}
			}
		}
		for (int i = 0; i < pickUpItemPrefabs.Length; i++) {
			Vector3 pos = GetSpawnPoint (false);
			GameObject item = Instantiate (pickUpItemPrefabs[i]) as GameObject;
			pos.y = pickUpItemPrefabs[i].transform.position.y;
			item.transform.position = pos;
			item.AddComponent<Respawner> ().SetTime (respawnTime);
		}
	}
	



	/*void SpawnEnemies() {
		int enemyCount = Random.Range (minEnemyCount, maxEnemyCount + 1);
		Vector3 pos = GetEnemyPoint ();
		for (int i = 0; i < enemyCount; i++) {
			GameObject enemy = Instantiate (enemyPrefabs [0]) as GameObject;
			enemy.transform.position = getNearPosition (pos, 4f);
			//enemy.GetComponent<NavMeshAgent> ().SetDestination (pos);
			enemy.GetComponent<BehaviorTree> ().GetVariable ("Doug").SetValue (player);
			enemy.GetComponent<BehaviorTree> ().GetVariable ("Doug Center").SetValue (playerCenter);
		}
	}*/

	private Vector3 minBoundsPoint;
	private Vector3 maxBoundsPoint;
	private float boundsSize = float.NegativeInfinity;

	private Vector3 GetSpawnPoint(bool nearPlayer)
	{

		if (boundsSize < 0)
		{
			minBoundsPoint = Vector3.one * float.PositiveInfinity;
			maxBoundsPoint = -minBoundsPoint;
			var vertices = UnityEngine.AI.NavMesh.CalculateTriangulation().vertices;
			foreach (var point in vertices)
			{
				if (minBoundsPoint.x > point.x)
					minBoundsPoint = new Vector3(point.x, minBoundsPoint.y, minBoundsPoint.z);
				if (minBoundsPoint.y > point.y)
					minBoundsPoint = new Vector3(minBoundsPoint.x, point.y, minBoundsPoint.z);
				if (minBoundsPoint.z > point.z)
					minBoundsPoint = new Vector3(minBoundsPoint.x, minBoundsPoint.y, point.z);
				if (maxBoundsPoint.x < point.x)
					maxBoundsPoint = new Vector3(point.x, maxBoundsPoint.y, maxBoundsPoint.z);
				if (maxBoundsPoint.y < point.y)
					maxBoundsPoint = new Vector3(maxBoundsPoint.x, point.y, maxBoundsPoint.z);
				if (maxBoundsPoint.z < point.z)
					maxBoundsPoint = new Vector3(maxBoundsPoint.x, maxBoundsPoint.y, point.z);
			}
			boundsSize = Vector3.Distance(minBoundsPoint, maxBoundsPoint);
		}
		Vector3 randomPoint = new Vector3(
			Random.Range(minBoundsPoint.x, maxBoundsPoint.x),
			0f,
			Random.Range(minBoundsPoint.z, maxBoundsPoint.z)
		);

		if (nearPlayer) {
			while (Vector3.Distance (player.transform.position, randomPoint) > minEnemyDistance) {
				randomPoint.x = Random.Range (minBoundsPoint.x, maxBoundsPoint.x);
				randomPoint.z = Random.Range (minBoundsPoint.z, maxBoundsPoint.z);
//				Debug.Log (randomPoint);
			}
		} else {
			while (Vector3.Distance (player.transform.position, randomPoint) < minEnemyDistance) {
				randomPoint.x = Random.Range (minBoundsPoint.x, maxBoundsPoint.x);
				randomPoint.z = Random.Range (minBoundsPoint.z, maxBoundsPoint.z);
//				Debug.Log (randomPoint);
			}
		}
//		Debug.Log (randomPoint);
		UnityEngine.AI.NavMeshHit hit;
		UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out hit, 50f, 1);
//		Debug.Log (hit.position);
		//  posr = hit.position;
		return hit.position;
	}

	Vector3 getNearPosition(Vector3 point, float radius) {
		Vector3 pos = point;
		pos += Random.onUnitSphere * radius;
		pos.y = point.y;
		UnityEngine.AI.NavMeshHit hit;
		UnityEngine.AI.NavMesh.SamplePosition(pos, out hit, 50.0f, 1);
		return hit.position;
	}

	public void LockTargetButtonEvent() {
		RigidbodyCharacterController controller = player.GetComponent<RigidbodyCharacterController> ();
		controller.Aim = !controller.Aiming;
//		if (controller.AlwaysAim) {
//			lockButton.image.color = lockDisabledColor;
//			controller.AlwaysAim = false;
//		} else {
//			lockButton.image.color = lockEnabledColor;
//			controller.AlwaysAim = true;
//		}
	}

	public void DisableLockTarget() {
		RigidbodyCharacterController controller = player.GetComponent<RigidbodyCharacterController> ();
		controller.Aim = false;
//		if (controller.AlwaysAim) {
//			lockButton.image.color = lockDisabledColor;
//			controller.AlwaysAim = false;
//		}
	}

	public void DecreaseEnemies() {
		enemiesLeft--;
		AddKillReward ();
		kills++;
		GameUI.Instance.sessionKillsText.text = kills.ToString ();
		//increase achievements
		AchievementController.Instance.strokeKillsCounter.Inc ();
		AchievementController.Instance.firstKillsCounter.Inc ();
		AchievementController.Instance.totalKillsCounter.Inc ();
		AchievementController.Instance.killsAtLevel++;
		if (playerState == PlayerState.Walk) {
			string weaponName = "";
			Inventory inv = player.GetComponent<Inventory> ();
			if (inv &&  inv.GetCurrentItem (typeof(PrimaryItemType)))
				weaponName = inv.GetCurrentItem (typeof(PrimaryItemType)).ItemName;
			if (weaponName == "Axe") {
				AchievementController.Instance.stickKillsCounter.Inc ();
			} else if (weaponName == "Pistol") {
				AchievementController.Instance.pistolKillsCounter.Inc ();
			} else if (weaponName == "Shotgun") {
				AchievementController.Instance.shotgunKillsCounter.Inc ();
			} else if (weaponName == "Assault Rifle") {
				AchievementController.Instance.assaultKillsCounter.Inc ();
			}

		} else if (playerState == PlayerState.DrivePoiceCar) {
			AchievementController.Instance.copCarKillsCounter.Inc ();
		} else if (playerState == PlayerState.DriveStolenCar) {
			AchievementController.Instance.stolenCarKillsCounter.Inc ();
		}

		//PlayerPrefs.SetInt ("kills", PlayerPrefs.GetInt ("kills", 0) + 1);
		if (enemiesLeft == 0) {
			StartCoroutine (OnMissionEnd ());
		}
//		if (kills % 6 == 0) {
//			GameUI.Instance.ShowAd ();
//		}
	}


	IEnumerator OnMissionEnd() {
		ShowMessage (endWaveMessages [Random.Range (0, endWaveMessages.Length)], 5f);
		currentWave++;
		currentWave = Mathf.Min (currentWave, maxWaves);
		actualWave++;
		if (actualWave % 3 == 0) {
			GameUI.Instance.ShowAd ();
		}
		yield return new WaitForSeconds (2f);
		CreateWave ();
	}

	void SaveProgress() {
		int currentLevel = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex;
		if (PlayerPrefs.GetInt ("progress", 0) < currentLevel) {
			PlayerPrefs.SetInt ("progress", currentLevel);
		}
	}

	void OnDestroy() {
		//Destroy (FindObjectOfType<EventHandler> ());
	}

	public void OnContinueGame() {
		//PlayerPrefs.SetInt ("continue", currentWave);
		//PlayerPrefs.SetInt ("continue_kills", kills);
		//PlayerPrefs.SetInt ("continue_coins", earnedCoins);
	}

	public void AddKillReward() {
		int totalReward = (rewardValue + 10 * currentWave) * coinsMult;
		sessionCoins += totalReward;
		//earnedCoins += totalReward;
		coinsText.text = sessionCoins.ToString () + getMultiplierUI();
		ShowMessage ("+" + totalReward.ToString() + " coins", 3f);
		GameUI.Instance.IncreaseTotalCoins (totalReward);
		AchievementController.Instance.coinsCounter.Inc (totalReward);
		AchievementController.Instance.earnedCoinsAtLevel += totalReward;
		AddScore (5 * totalReward);
	}

	public void AddScore(int value) {
		if (canAddScore) {
			score += value;
			GameUI.Instance.UpdateScoreLabel (score);
		}
	}
		
	void AddScoreEachSecond() {
		if (Time.time - timer >= 1f) {
			timer = Time.time;
			AddScore (10);
		}
	}

	string getMultiplierUI() {
		if (coinsMult > 1)
			return " (" + coinsMult + "x)";
		else
			return "";
	}

	public void DecreaseScore() {
		sessionCoins -= failValue;
		if (sessionCoins < 0) {
			int diff = failValue + sessionCoins;
			GameUI.Instance.DecreaseTotalCoins (diff);
			sessionCoins = 0;
		} else {
			GameUI.Instance.DecreaseTotalCoins (failValue);
		}
		coinsText.text = sessionCoins.ToString ();
		ShowMessage ("Don't kill  peaceful people!" + " -" + failValue.ToString() + " coins", 3f);

	}

	public void SaveScore() {
	//	PlayerPrefs.SetInt ("coins", PlayerPrefs.GetInt ("coins", 0) + score);
		//PlayerPrefs.SetInt ("kills", PlayerPrefs.GetInt ("kills", 0) + kills);
	}

	/*public void SetWeaponEnabled(bool enabled) {
		player.GetComponent<Inventory>().GetCurrentItem(typeof(PrimaryItemType)).gameObject.SetActive(enabled);
	}*/

	void CheckDailyReward() {
		if (PlayerPrefs.HasKey ("last_bonus")) {
			string oldDateString = PlayerPrefs.GetString("last_bonus");
			System.DateTime oldDate = System.Convert.ToDateTime(oldDateString);
			System.DateTime currentDate = System.DateTime.Now;
			System.TimeSpan difference = currentDate.Subtract(oldDate);
			if (difference.Days >= 1) {
				AddDailyReward (1000);
				PlayerPrefs.SetString ("last_bonus", System.Convert.ToString(currentDate));
			}
		} else {
			PlayerPrefs.SetString ("last_bonus", System.Convert.ToString(System.DateTime.Now));
			AddDailyReward (1000);
		}
	}

	void AddDailyReward(int value) {
		GameUI.Instance.IncreaseTotalCoins (value);
		GameUI.Instance.SetDailyRewardMenuVisible (true);
	}

	public void AddAchievementReward(string name, int value) {
		//GameUI.Instance.IncreaseTotalCoins (value);
		PlayerPrefs.SetInt (name, PlayerPrefs.GetInt(name, 0) + value);
	}

	public bool hasEnoughHalos() {
		return PlayerPrefs.GetInt ("halo", 0) >= halosNeeded;
	}

	public void Revive() {
		PlayerPrefs.SetInt ("halo", PlayerPrefs.GetInt ("halo", 0) - halosNeeded);
		halosNeeded = halosNeeded * 2;
		player.GetComponent<AnimatorMonitor> ().OnRespawn ();
		player.GetComponent<Health> ().SetHealthAmount (player.GetComponent<Health> ().MaxHealth);
		SetStickKinematic (false);
	}


	bool isStickDelayed = false;
	IEnumerator SetStickKinematicDelay(float sec, bool isKinematic) {
		if (!isStickDelayed) {
			isStickDelayed = true;
			yield return new WaitForSeconds (sec);
			MeleeWeapon stick = player.GetComponentInChildren<MeleeWeapon> ();
			if (stick) {
				stick.GetComponent<Rigidbody> ().isKinematic = isKinematic;
			}
			isStickDelayed = false;
		}
	}
		
	public void SetStickKinematic(bool isKinematic) {
		if (isKinematic) {
			MeleeWeapon stick = player.GetComponentInChildren<MeleeWeapon> ();
			if (stick) {
				stick.GetComponent<Rigidbody> ().isKinematic = isKinematic;
			}
		} else {
			StartCoroutine(SetStickKinematicDelay (3f, false));
		}
	}
		
	public void AddBonus(string prefsName, int value) {
		PlayerPrefs.SetInt (prefsName, PlayerPrefs.GetInt (prefsName, 0) + value);
	}



	public void SetShieldBoosterEnabled(bool enabled) {
		player.GetComponent<CharacterHealth> ().Invincible = enabled;
		player.GetComponent<DriveVehicle> ().SetShieldEnabled (enabled);
		shieldEnabled = enabled;
	}

	public void SetMagnetBoosterEnabled(bool enabled) {
		magnetEnabled = enabled;
		if (!enabled) {
			foreach (MagnetItem item in FindObjectsOfType<MagnetItem>())
				item.Reset ();
		}
	}

	private int xUsed = 0;
	public void UpdateXBooster() {
		int iapMult = PlayerPrefs.GetInt ("double_coins", 0) == 1 ? 2 : 1;
		xUsed++;
		if (xUsed == 1) {
			coinsMult = iapMult * 2;
			PlayerPrefs.SetInt ("cur_mult", 2);
			PlayerPrefs.SetInt ("next_mult", 3);
			PlayerPrefs.SetInt ("next_mult_cost", 2);
		} else if (xUsed == 2) {
			coinsMult = iapMult * 3;
			PlayerPrefs.SetInt ("cur_mult", 3);
			PlayerPrefs.SetInt ("next_mult", 4);
			PlayerPrefs.SetInt ("next_mult_cost", 3);
		} else if (xUsed == 3) {
			coinsMult = iapMult * 4;
			PlayerPrefs.SetInt ("cur_mult", 4);
			PlayerPrefs.SetInt ("next_mult", -1);
			PlayerPrefs.SetInt ("next_mult_cost", -1);
		}
		coinsText.text = sessionCoins.ToString () + getMultiplierUI();
	}

	public void SetVehicleFireEnabled(bool enabled) {
		VehicleGun gun = player.GetComponent<DriveVehicle> ().getVehicleGun();
		if (gun)
			gun.SetFireEnabled(enabled);
	}

	public void SetPlayerInCar(bool inCar) {
		isPlayerInCar = inCar;
		if (inCar && magnetEnabled) {
			foreach (MagnetItem item in FindObjectsOfType<MagnetItem>())
				item.Reset ();
		}
	}
}
