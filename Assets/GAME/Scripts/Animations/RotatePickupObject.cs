﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePickupObject : MonoBehaviour {

	public float time = 1f;

	// Use this for initialization
	void Start () {
		LeanTween.rotateAround (gameObject, Vector3.up, 360, time).setLoopClamp ();
	}

	void OnDestroy () {
		LeanTween.cancel (gameObject);
	}
}
