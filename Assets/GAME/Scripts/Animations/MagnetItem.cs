﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetItem : MonoBehaviour {

	private bool isMoveStarted = false; //is animation started
	private int moveID = 0; //id of animation
	private Transform player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isMoveStarted) {
			LTDescr d = LeanTween.descr(moveID);
			if (d != null) {
				Vector3 target = player.position;
				target.y = transform.position.y;
				LeanTween.descr(moveID).setTo(target);
			} else {
				isMoveStarted = false;
			}
		}
	}

	public void Move(Transform _player) {
		if (isMoveStarted)
			return;
		isMoveStarted = true;
		player = _player;
		moveID = LeanTween.move(this.gameObject, _player.position, 0.5f).id;
		gameObject.layer = LayerMask.NameToLayer ("Default");
	}

	public void Reset() {
		LeanTween.cancel (gameObject);
		isMoveStarted = false;
		gameObject.layer = LayerMask.NameToLayer ("Triggers");
	}
}
